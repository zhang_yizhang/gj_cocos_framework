import { _decorator, Component, Node, LabelComponent } from 'cc';
import AdBase from './AdBase';
const { ccclass, property, executeInEditMode } = _decorator;

@ccclass('ContactEmailComp')
@executeInEditMode(true)
export class ContactEmailComp extends AdBase {
    protected onLoad() {
        const label = this.getComponent(LabelComponent) || this.addComponent(LabelComponent)!;
        label.string = '联系客服：\nwurenxian@xplaymobile.com';
    }
}
