import config, {Platform} from "../../../config/config";
import * as cc from "cc";

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    btnNode: cc.Node = null!;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        if(config.platform != Platform.bytedance){
            this.node.destroy();
        }

        return
    }

    onEnable() {
        if (config.platform != Platform.bytedance) {
            return
        }
        this.scheduleOnce(() => {
            let size = cc.view.getVisibleSize()
            if (this.enabledInHierarchy && !this.btnNode) {
                this.btnNode = this.CreateMoreGamesButton(this.node, size.width, size.height)
            }
        }, 0.2)
    }

    onDisable() {
        if (config.platform != Platform.bytedance) {
            return
        }
        this.CleanMoreGamesButton(this.btnNode);
        this.btnNode = null!;
    }

    CreateMoreGamesButton(morgamePosNode: cc.Node, screenWidth: number, screenHight: number) {
      

        var positon = morgamePosNode.getComponent(cc.UITransform)!.convertToWorldSpaceAR(cc.v3(0,0, 0));
          // @ts-ignore
        let rate=tt.getSystemInfoSync().screenWidth/screenWidth;
     
        let Xbili = (positon.x ) / screenWidth
        let Ybili = (screenHight - positon.y ) / screenHight;
        let comp = this.node.getComponent(cc.UITransform)!;
        let width = comp.width*rate
        let height =  comp.height*rate
        console.log("Ybili   " + Ybili + " screenHight "+screenHight + "  positon  "+ positon+ "  height  "+height)
        // @ts-ignore
        let x = Xbili * tt.getSystemInfoSync().screenWidth
        // @ts-ignore
        let y = Ybili * tt.getSystemInfoSync().screenHeight
        console.log('x  ,, y ',x,y)
        console.log(width,height)
        // @ts-ignore
        let btn = tt.createMoreGamesButton({
            type: "image",
            image: "image/moregame.png",
            actionType: "box",
            style: {
                left:x,
                top: y,
                width:width,
                height: height,
                lineHeight: 40,
                backgroundColor: "",
                textColor: "",
                textAlign: "center",
                fontSize: 16,
                borderRadius: 4,
                borderWidth: 0,
                borderColor: ""
            },
            appLaunchOptions: [{
                appId: "ttXXXXXX",
                query: "foo=bar&baz=qux",
                extraData: {}
            }
                // {...}
            ],
            onNavigateToMiniGame(res : any) {
                console.log("跳转其他小游戏", res);
            }
        });
        btn.onTap(() => {
            console.log("点击更多游戏");
        });
        console.log(btn)
        return btn;
    }
    CleanMoreGamesButton(btn : any) {
        if (btn) {
            // console.log(btn)
            console.log('更多按钮关闭');
            btn.destroy();
            btn = null;
        }
    }


    // update (dt) {}
}
