/**
@class NavigateIconAd 
@author YI ZHANG
@date 2020/9/9
@desc
**/

import AdBase from "./AdBase";
import ASCAd from "../../AdSDK/ASCAd";
import * as cc from "cc";

const {ccclass, property} = cc._decorator;
@ccclass
export default class NavigateIconAd extends AdBase {
    show(){
        let comp = this.node.getComponent(cc.UITransform)!;
        let pos = comp.convertToWorldSpaceAR(cc.v3(0,0,0));
        ASCAd.getInstance().getNavigateIconFlag() && ASCAd.getInstance().showNavigateIcon(comp.width, comp.height, pos.x , pos.y);
    }

    hide() {
        ASCAd.getInstance().hideNavigateIcon();
    }

}
