/** 打开隐私协议
@class UserButton
@author YI ZHANG
@date 2021/10/20
@desc
**/
import { _decorator, Component, Node } from 'cc';
import ASCAd from "../../AdSDK/ASCAd";
import * as cc from "cc";
const { ccclass, property } = _decorator;

@ccclass('UserButton')
export class UserButton extends Component {

    protected start(): void {
        let button = this.addComponent(cc.Button)!;
        let event = new cc.EventHandler();
        event.handler = "click";
        event.target = this.node;
        event.component = "UserButton";
        button.clickEvents.push(event);
    }

    click(){
        ASCAd.getInstance().openServiceAgreement();
    }
}
