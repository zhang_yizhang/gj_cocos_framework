/** 打开隐私协议
@class PrivacyButton
@author YI ZHANG
@date 2021/10/20
@desc
**/
import { _decorator, Component, Node } from 'cc';
import ASCAd from "../../AdSDK/ASCAd";
import * as cc from "cc";
const { ccclass, property } = _decorator;

@ccclass('PrivacyButton')
export class PrivacyButton extends Component {

    protected start(): void {
        let button = this.addComponent(cc.Button)!;
        let event = new cc.EventHandler();
        event.handler = "click";
        event.target = this.node;
        event.component = "PrivacyButton";
        button.clickEvents.push(event);
    }

    click(){
        ASCAd.getInstance().openProtocol()
    }
}
