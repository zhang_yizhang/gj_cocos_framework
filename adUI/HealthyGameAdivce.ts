import { _decorator, Component, Node, tween, WidgetComponent } from 'cc';
import { EDITOR } from 'cc/env';
import AdBase from './AdBase';
const { ccclass, property, executeInEditMode } = _decorator;

@ccclass('HealthyGameAdivce')
@executeInEditMode(true)
export class HealthyGameAdivce extends AdBase {
    protected onLoad() {
        const widget = this.getComponent(WidgetComponent) || this.addComponent(WidgetComponent)!;
        widget.isAlignLeft = widget.isAlignRight = widget.isAlignTop = widget.isAlignBottom = true;
        widget.left = widget.right = widget.top = widget.bottom = 0;
        if (!EDITOR) {
            tween(this.node)
                .delay(2)
                .call(() => { this.node && this.node.destroy() })
                .start();
        }
    }
}
