import { _decorator, Component, Node, ButtonComponent, EventHandler, LabelComponent } from 'cc';
import { DEBUG } from 'cc/env';
import config, { Platform } from '../../../config/config';
import { ReportData } from '../../../Script/data/ReportData';
import { PlatformManager, ProductInfo } from '../../../Script/Manager/PlatformManager';
import { GameManager } from '../../../Script/MVVM/GameManager';
import ModelManager from '../../../Script/MVVM/Model/ModelManager';
import ReportComp from '../adUI/ReportComp';
const { ccclass, property } = _decorator;

@ccclass('GoogleNoADBtn')
export class GoogleNoADBtn extends Component {
    @property(LabelComponent)
    priceLab!: LabelComponent;

    _money: number = 0;
    _price: string = '';
    _proId: string = 'iap.no_popup_ad';
    _proName: string = '';

    protected start(): void {
        if (config.platform !== Platform.google || DEBUG) {
            this.node.destroy();
            return;
        }
        if (ModelManager.UserModel.payForNoAD) {
            this.node.destroy();
            return;
        }

        this.priceLab.node.active = false;
        PlatformManager.getInstance().reqProductInfo((info: ProductInfo[]) => {
            console.log('payForNoAdsBtn-start', JSON.stringify(info))
            let no_popup_ad_inof = info.find((data) => data.id == 'iap.no_popup_ad')!
            this._proId = no_popup_ad_inof.id
            this._price = no_popup_ad_inof.price
            this._proName = no_popup_ad_inof.name
            this.priceLab.string = this._price
            this.priceLab.node.active = true;
        })

        let button = this.addComponent(ButtonComponent)!;
        let event = new EventHandler();
        event.handler = "click";
        event.target = this.node;
        event.component = "GoogleNoADBtn";
        button.clickEvents.push(event);
    }

    click() {
        PlatformManager.getInstance().buyProps(0, this._proId, this._proName, (success: boolean) => {
            console.log('payForNoAdsBtn-buyProps', success)
            if (success) {
                ModelManager.UserModel.payForNoAD = true;
                PlatformManager.getInstance().showBannerAD(false);
                ReportComp.setUser({
                    buyProps: this._proName
                })
                GameManager.showTip('The purchase is successful,\n and the advertisement is permanently removed.');
                this.node.active = false;
                ReportComp.customReport('in_app_product', {
                    proName: this._proName,
                    proId: this._proId,
                    price: 0.99
                })
            } else {
                GameManager.showTip('Failed to get product.');
            }

        })
    }
}
