/**
 @class BannerAd
 @author YI ZHANG
 @date 2020/9/9
 @desc
 **/
import AdBase, {AdType, } from "./AdBase";
import config, {Platform} from "../../../config/config";
import NativeIconAd from "./NativeIconAd";
import NativeImageAd from "./NativeImageAd";
import NavigateSettleAd from "./NavigateSettleAd";
import BannerAd from "./BannerAd";
import NavigateIconAd from "./NavigateIconAd";
import BuildingBlocksAd from "./BuildingBlocksAd";
import * as cc from "cc";
import {EDITOR} from "cc/env";
import {PlatformEnum} from "../cocosCreator3d/platform/PlatformManagerBase";

const {ccclass, property} = cc._decorator;
@ccclass
export default class BlockAdComp extends cc.Component {
    @property({type : cc.Enum(AdType)})
    adType = AdType.Banner;
    @property({type : [PlatformEnum]})
    platformEnumList : Platform[] = [];
    private Class!: {prototype:AdBase};
    private _curSchedule!: number;
    private _curSchedule2!: number;
    private block!: boolean;

    protected onLoad(): void {
        if(EDITOR){
            return;
        }
        if(this.platformEnumList.length > 0 && this.platformEnumList.indexOf(config.platform) == -1){
            this.node.active = false;
            if(this.node){
                this.node.destroy();
            }           
            return;
        }
        switch (this.adType) {
            case AdType.NativeIcon:
                this.Class = NativeIconAd;
                break;
            case AdType.NativeImage:
                this.Class = NativeImageAd;
                break;
            case AdType.NavigateSettleAd:
                this.Class = NavigateSettleAd;
                break;
            case AdType.Banner:
                this.Class = BannerAd;
                break;
            case AdType.NavigateIcon:
                this.Class = NavigateIconAd;
                break;
            case AdType.BuildingBlocksAd:
                this.Class = BuildingBlocksAd;
                break;
        }
    }

    protected onEnable(): void {
        if(EDITOR){
            return;
        }
        clearTimeout(this._curSchedule);
        this._curSchedule = setTimeout(()=>{
            if(!cc.isValid(this) || !this.enabledInHierarchy){
                return;
            }
            if(this.Class && !this.block){
                // @ts-ignore
                if(this.Class == NativeImageAd){
                    // debugger;
                }
                this.Class.changeBlockNum(1);
                this.block = true;
            }
        });

    }

    protected onDisable(): void {
        if(EDITOR){
            return;
        }
        clearTimeout(this._curSchedule2);
        this._curSchedule2 = setTimeout(()=>{
            if(this.Class && this.block){
                this.block = false;
                // @ts-ignore
                this.Class.changeBlockNum(-1);
            }
        });

    }

}
