/**
 title 结算页互推
 author NavigateSettleAd
 date 2020/6/19
 desc
 **/

import AdBase, {AdType} from "./AdBase";
import config, {Platform} from "../../../config/config";
import BlockAdComp from "./BlockAdComp";
import ASCAd from "../../AdSDK/ASCAd";
import * as cc from "cc";

const {ccclass, property} = cc._decorator;
enum Type
{
    LINE = 2,
    MINI_GAME = 1,
}
let TypeEnum =  cc.Enum(Type);
@ccclass
export default class NavigateSettleAd extends AdBase {
    @property({type : TypeEnum})
    _type : Type = Type.LINE;
    @property({type : TypeEnum})
    set type(type){
        this._type = type;
    }
    get type(){
        return this._type;
    }
    @property(cc.CCBoolean)
    isBlockBannerAd : boolean = true;

    show(){
        //节点类型
        let comp = this.node.getComponent(cc.UITransform)!;
        let pos = comp.convertToWorldSpaceAR(cc.v3(0,0,0));
        if (!ASCAd.getInstance().getNavigateSettleFlag()) {
            return;
        }
        ASCAd.getInstance().showNavigateSettle(this._type,this._type == Type.MINI_GAME ? 0 : pos.x,this._type == Type.MINI_GAME ? 0 : pos.y);
    }

    hide() {
        ASCAd.getInstance().hideNavigateSettle();
    }

    protected onLoad(): void {
        super.onLoad();
        if(cc.isValid(this.node)){
            let node = new cc.Node();
            let block = node.addComponent(BlockAdComp);
            if(this.isBlockBannerAd){
                block.adType = AdType.Banner;
                block.platformEnumList = [];
                block = node.addComponent(BlockAdComp);
            }
            if(config.platform !== Platform.wechat){
                block.adType = AdType.NativeIcon;
                block.platformEnumList = [];
                block = node.addComponent(BlockAdComp);
                block.adType = AdType.NavigateIcon;
                block.platformEnumList = [];
            }
            // block = node.addComponent(BlockAdComp);
            // block.adType = AdType.NativeImage;
            // block.platformEnumList = [];
            this.node.addChild(node);
        }
    }

}
