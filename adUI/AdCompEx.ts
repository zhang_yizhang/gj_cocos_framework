/** 广告组件
@class AdCompEx
@author YI ZHANG
@date 2020/9/9
@desc
**/
import * as cc from "cc";
import AdBase, {AdType} from "./AdBase";
import {Platform} from "../../../config/config";
import NativeIconAd from "./NativeIconAd";
import NativeImageAd from "./NativeImageAd";
import NavigateSettleAd from "./NavigateSettleAd";
import BannerAd from "./BannerAd";
import NavigateIconAd from "./NavigateIconAd";
import BuildingBlocksAd from "./BuildingBlocksAd";
import NavigateBoxBanner from "./NavigateBoxBanner";
import {EDITOR} from "cc/env";
import {AndroidEnum} from "../cocosCreator3d/platform/PlatformManagerBase";
import { PrivacyAgreementNew } from "./PrivacyAgreementNew";
import { ContactEmailComp } from "./ContactEmailComp";
import { HealthyGameAdivce } from "./HealthyGameAdivce";


const {ccclass, property} = cc._decorator;
@ccclass
export default class AdCompEx extends cc.Component{
    @property
    private _type: AdType = AdType.BuildingBlocksAd;
    @property({type : cc.Enum(AdType),tooltip : "Banner = 1,\n" +
            "    NativeIcon = 原生icon,\n" +
            "    NavigateIcon = 互推icon,\n" +
            "    NavigateSettleAd = 结算页互推,\n" +
            "    NativeImage = 原生大图,\n" +
            "    BuildingBlocksAd = 积木广告,\n" +
            "    PrivacyAgreement = 隐私协议,\n" +
            "    ContactEmail = 联系客服,\n" +
            "    HealthyGameAdivce = 健康游戏公告,\n" +
            "    NavigateBoxBannerAD = 互推盒子,"})
    set adType(type){
        this._type = type;
        this.resetComp();
    }
    get adType(){
        return this._type;
    }
    @property({type : cc.Enum(Platform)})
    platformEnumList : Platform[] = [];
    @property({type : cc.Enum(AndroidEnum)})
    androidEnumList : AndroidEnum[] = [];

    protected onLoad(): void {
        if(!EDITOR){
            this.getComponent(AdBase)!.platformEnumList = this.platformEnumList;
            this.getComponent(AdBase)!.androidEnumList = this.androidEnumList;
        }
        console.log("AdCompEx-onload")
    }

    /**
     * 重置组件
     */
    resetComp(){
        if(!EDITOR){
            return;
        }
        this.node.getComponent(AdBase) && this.node.getComponent(AdBase)!.destroy();
        setTimeout(()=>{
            let comp;
            switch (this.adType) {
                case AdType.NativeIcon:
                    comp = this.getComponent(NativeIconAd) || this.addComponent(NativeIconAd);
                    break;
                case AdType.NativeImage:                   
                    comp = this.getComponent(NativeImageAd) || this.addComponent(NativeImageAd);
                    break;
                case AdType.NavigateSettleAd:
                    comp = this.getComponent(NavigateSettleAd) || this.addComponent(NavigateSettleAd);
                    break;
                case AdType.Banner:
                    comp = this.getComponent(BannerAd) || this.addComponent(BannerAd);
                    break;
                case AdType.NavigateIcon:
                    comp = this.getComponent(NavigateIconAd) || this.addComponent(NavigateIconAd);
                    break;
                case AdType.BuildingBlocksAd:
                    comp = this.getComponent(BuildingBlocksAd) || this.addComponent(BuildingBlocksAd);
                    break;
                case AdType.NavigateBoxBannerAD:
                    comp = this.getComponent(NavigateBoxBanner) || this.addComponent(NavigateBoxBanner);
                    break;
                case AdType.PrivacyAgreement:
                    comp=this.getComponent(PrivacyAgreementNew)||this.addComponent(PrivacyAgreementNew);
                    break;
                case AdType.ContactEmail:
                    comp=this.getComponent(ContactEmailComp)||this.addComponent(ContactEmailComp);
                    break;
                case AdType.HealthyGameAdivce:
                    comp=this.getComponent(HealthyGameAdivce)||this.addComponent(HealthyGameAdivce);
                    break;
            }
        });

    }

}
