import config, {Platform} from "../../../../config/config";
import * as cc from "cc";
import ASCAd from "../../../AdSDK/ASCAd";


const {ccclass, property} = cc._decorator;

@ccclass
export default class appBox extends cc.Component {
    protected onLoad(): void {

    }

    protected start(): void {
        switch (config.platform) {
            case Platform.androids:
                this.node.active = config.platform == Platform.androids && ASCAd.getInstance().getOPPOShowMoreGameFlag() && PlatformManagerBase.getAndroidChannel() == AndroidEnum.OPPO;
                break;
            case Platform.qq:
                this.node.active = config.platform == Platform.qq && ASCAd.getInstance().getBoxFlag();
                break;
            // case Platform.oppo:
            //     this.node.active = config.platform == Platform.oppo;
            //     break;
            default:
                this.node.active = false;
                break;
        }
        this.node.on(cc.Node.EventType.TOUCH_END,this.openBox,this);
    }

    openBox(){
        switch (config.platform) {
            case Platform.qq:
                if( ASCAd.getInstance().getBoxFlag()){
                    ASCAd.getInstance().showAppBox()
                }
                break;
            case Platform.oppo:
                if( ASCAd.getInstance().getNavigateBoxPortalFlag()){
                    ASCAd.getInstance().showNavigateBoxPortal()
                }
                break;
            case Platform.androids:
                if(ASCAd.getInstance().getOPPOShowMoreGameFlag()){
                    // 展示更多游戏按钮
                    ASCAd.getInstance().showOPPOMoreGame();
                }
                break;

        }
    }

    // update (dt) {}
}
import PlatformManagerBase, {AndroidEnum} from "../../cocosCreator3d/platform/PlatformManagerBase";
