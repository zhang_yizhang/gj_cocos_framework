/**
@class BannerAd
@author YI ZHANG
@date 2020/9/9
@desc
**/
import AdBase from "./AdBase";
import * as cc from "cc";
import ASCAd from "../../AdSDK/ASCAd";
import config, { Platform } from "../../../config/config";
import CCUiStack from "../cocosCreator3d/mvvm/ui/CCUiStack";
import ModelManager from "../../../Script/MVVM/Model/ModelManager";

const { ccclass, property } = cc._decorator;
CCUiStack.setUpdateTopCb(() => {
    if (BannerAd.isShow) {
        BannerAd.show();
    }
});
@ccclass
export default class BannerAd extends AdBase {
    static isShow: boolean;
    static lastShowTime = 0;
    private delay: number = 0;

    protected onLoad(): void {
        super.onLoad();
    }

    show() {
        if (config.platform == Platform.webTest) {
            console.log('showBannerAD:'+true)
            return;
        }
        if (config.platform === Platform.google) {
            if (ModelManager.UserModel.payForNoAD) {
                return;
            }
        }
        ASCAd.getInstance().showBanner(1);
        BannerAd.isShow = true;
        BannerAd.lastShowTime = new Date().getTime();
    }

    hide() {
        if (config.platform == Platform.webTest) {
            console.log('showBannerAD:'+false)
            return;
        }
        ASCAd.getInstance().hideBanner();
        BannerAd.isShow = false;
    }

    protected update(dt: number): void {
        // if(!BannerAd.isShow){
        //     return;
        // }
        // if(BannerAd.isShow && this.delay + dt > 3 &&  new Date().getTime() - BannerAd.lastShowTime > 20000){
        //     this.delay = 0;
        //     BannerAd.lastShowTime = new Date().getTime();
        //     return;
        // }
        // this.delay += dt;
    }

}
