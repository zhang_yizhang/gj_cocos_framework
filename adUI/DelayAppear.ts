// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import config, {Platform} from "../../../config/config";
import * as cc from "cc";
import {CCFloat} from "cc";
import {NodeTool} from "../cocosCreator3d/utils/NodeTool";

const {ccclass, property} = cc._decorator;

@ccclass("DelayAppear")
export default class DelayAppear extends cc.Component {

    @property(CCFloat)
    delayTime : number = 1;
    tween : cc.Tween<any> = null!;
    static isDelay : boolean = false;
    private oldOpacity!: number;

    protected onEnable(): void {
        if(this.delayTime <= 0 || !DelayAppear.isDelay){
            return;
        }
        this.oldOpacity =  this.oldOpacity || NodeTool.getOpacityComp(this.node).opacity;
        NodeTool.getOpacityComp(this.node).opacity = 1;
        if(this.tween){
            this.tween.stop();
        }
        this.tween = cc.tween(NodeTool.getOpacityComp(this.node)).delay(this.delayTime).to(0,{opacity : this.oldOpacity}).start();
    }
}
