/**
@class NativeImageAd
@author YI ZHANG
@date 2020/9/9
@desc
**/
import AdBase, {AdType} from "./AdBase";
import config, {Platform} from "../../../config/config";
import BlockAdComp from "./BlockAdComp";
import ASCAd from "../../AdSDK/ASCAd";
import * as cc from "cc";
import GetConfig from "../../AdSDK/utils/GetConfig";
import BannerAd from "./BannerAd";
import {NodeTool} from "../cocosCreator3d/utils/NodeTool";
import AdCenter from "../../AdSDK/ad/AdCenter";

const {ccclass, property,requireComponent} = cc._decorator;
@ccclass("NativeImageAd")
@requireComponent(cc.WidgetComponent)
export default class NativeImageAd extends AdBase {
    static isShowViewNode = true;
    @property(cc.Node)
    viewNode : cc.Node = null!;
    @property(cc.CCBoolean)
    changeParent : boolean = true;
    delayTime : number = 0;
    private blockNode!: cc.Node;
    private viewPos!: cc.Vec3;
    protected onLoad(): void {
        console.log("getNativeImageFlag-onLoad")
        super.onLoad();
        (this.node.getComponent(cc.UIOpacity) || this.node.addComponent(cc.UIOpacity))!.opacity = 255;
        this.viewNode && (this.viewNode.active = false);
        if(config.platform == Platform.androids){
            this.viewNode = undefined!;
        }
        if(cc.isValid(this.node)){
            let node = new cc.Node();
            let block = node.addComponent(BlockAdComp);
            block.adType = AdType.Banner;
            block.platformEnumList = [];
            block = node.addComponent(BlockAdComp);
            block.adType = AdType.NativeIcon;
            block.platformEnumList = [];
            block = node.addComponent(BlockAdComp);
            block.adType = AdType.NavigateIcon;
            block.platformEnumList = [];
            block = node.addComponent(BlockAdComp);
            block.adType = AdType.NavigateSettleAd;
            block.platformEnumList = [];
            block = node.addComponent(BlockAdComp);
            block.adType = AdType.NavigateIcon;
            block.platformEnumList = [];
            this.blockNode = node;
            this.node.addChild(node);
        }

    }

    protected onEnable(): void {
        super.onEnable();
        this.blockNode.active = true;
        let banner = this.node.getComponent(BannerAd);
        if(banner){
            banner.destroy();
        }
        this.viewNode && (this.viewNode.active = false);
        // this.show()
        // console.log("getNativeImageFlag")
    }

    show(){
        if(config.platform == Platform.webTest){
            return;
        }
        this.blockNode.active = true;
        let banner = this.node.getComponent(BannerAd);
        if(banner){
            banner.destroy();
        }
        let node = this.node;
        if(node.getComponent(cc.Widget)){
            node.getComponent(cc.Widget)!.updateAlignment();
        }
        let comp = this.node.getComponent(cc.UITransform)!;
        let pos = comp.convertToWorldSpaceAR(cc.v3(0,0,0));
        let size = cc.view.getVisibleSize();
       
        if(ASCAd.getInstance().getNativeImageFlag()){
            ASCAd.getInstance().showNativeImage(comp.width, comp.height, pos.x, pos.y,1);
        }else {
            this.blockNode.active = false;
            // this.node.addComponent(BannerAd);
            return;
        }
        if(!NativeImageAd.isShowViewNode){
            return;
        }
        if(this.viewNode && !this.viewPos){
            this.viewPos = this.viewNode.getWorldPosition(cc.v3());
        }
        this.viewNode && this.scheduleOnce(()=>{
            console.log("test5");
            this.viewNode.targetOff(this.viewNode);
            let image : boolean | cc.Node = false;
            if(GetConfig.getChannelName() === "android"){
                image = true;
            }else {
                // @ts-ignore
                let node = AdCenter.getInstance().adFactory.sdkCanvas;
                if (node){
                    image = (<cc.Node><any>node.getChildByName("node_nativeImage"))
                        || (<cc.Node><any>node.getChildByName("node_testNativeImage")) || false;
                    console.log("test1");
                }else {
                    console.log("test4");
                }
            }
            if(image){
                let self = this;
                console.log("test2");
                if(image instanceof cc.Node){
                    let comp = image.getComponent(cc.UITransform) || (image.addComponent(cc.UITransform));
                    let old = comp.onDisable;
                    comp.onDisable = function () {
                        old.call(this);
                        cc.isValid(self.viewNode) && (self.viewNode.active = false);
                    };
                    //this.viewNode.active = true;
                    if(this.changeParent){
                        this.viewNode.setParent(image);
                        NodeTool.setLayer(this.viewNode,image.layer);
                        this.viewNode.getComponent(cc.UITransform)!.priority = 3000;
                        this.viewNode.setWorldPosition(this.viewPos);
                    }
                }
                let info = ASCAd.getInstance().getNativeAdInfo(1) || ASCAd.getInstance().getNativeAdInfo(0);
                let id = info && info.adId;
                if(id){
                    this.viewNode.active = true;
                    this.viewNode.on(cc.Node.EventType.TOUCH_END,()=>{
                        ASCAd.getInstance().reportNativeAdClick(id);
                    },this.viewNode)
                }else {
                    this.viewNode.active = false;
                }

            }
        },GetConfig.getChannelName() == "test" ? 0.8 : 0.1);
    }

    protected update(dt: number): void {
        if(this != this.Class.curInstance){
            return;
        }
        if(this.delayTime > 5){
            let banner = this.node.getComponent(BannerAd);
            if(banner && ASCAd.getInstance().getNativeImageFlag()){
                this.show();
            }
            this.delayTime = 0;
        }
        this.delayTime += dt;
    }

    hide() {
        ASCAd.getInstance().hideNativeImage();
        this.viewNode && (this.viewNode.active = false);
    }

}
