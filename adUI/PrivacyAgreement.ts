/**
@class PrivacyAgreement
@author YI ZHANG
@date 2020/12/25
@desc
**/
import config, {Platform} from "../../../config/config";
import * as cc from "cc";
import {DEBUG} from "cc/env";
import ASCAd from "../../AdSDK/ASCAd";
import {UiManager} from "../mvvm/UiManager/UiManager";
import PlatformManagerBase from "../cocosCreator3d/platform/PlatformManagerBase";

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {
    static show(cb ?: ()=>void,type = ""){
        PlatformManagerBase.getInstance().showPrivacyAgreement(cb,type);

    }
}
