/** 华为视频按钮处理
@class HuaweiAdBtn
@author YI ZHANG
@date 2021/8/13
@desc
**/
import * as cc from 'cc';
import {_decorator, Component} from 'cc';
import PlatformManagerBase, {AndroidEnum} from "../cocosCreator3d/platform/PlatformManagerBase";
import config, {Platform} from "../../../config/config";
import ASCAd from "../../AdSDK/ASCAd";

const { ccclass, property } = _decorator;

@ccclass('HuaweiAdBtn')
export class HuaweiAdBtn extends Component {
    btn ?: cc.Button;
    oldOpacity : number = -1;
    protected onEnable(): void {
        if(config.platform == Platform.androids && PlatformManagerBase.getAndroidChannel() == AndroidEnum.HUA_WEI || config.platform == Platform.huawei){
            this.unscheduleAllCallbacks();
            let btn : null | cc.Button = null;
            let node = this.node;
            while (!btn && node){
                btn = node.getComponent(cc.Button);
                node = node.parent!;
            }
            if(!btn){
                return;
            }
            this.btn = btn;
            let comp = btn.getComponent(cc.UIOpacity) || btn.addComponent(cc.UIOpacity)!;
            if(this.oldOpacity == - 1){
                this.oldOpacity = comp.opacity;
            }

            let flag = ASCAd.getInstance().getVideoFlag();
            comp.opacity = flag ? this.oldOpacity : 1;
            btn.interactable = !!flag;
            this.schedule(()=>{
                if(cc.isValid(comp) && cc.isValid(btn)){
                    let flag = ASCAd.getInstance().getVideoFlag();
                    comp.opacity = flag ? this.oldOpacity : 1;
                    btn!.interactable = !!flag;
                }
            },1);
        }
    }

    protected onDisable(): void {
        if((config.platform == Platform.androids && PlatformManagerBase.getAndroidChannel() == AndroidEnum.HUA_WEI || config.platform == Platform.huawei) && cc.isValid(this.btn)) {
            (this.btn!.getComponent(cc.UIOpacity) || this.btn!.addComponent(cc.UIOpacity)!).opacity = this.oldOpacity;
            this.btn!.interactable = true;
        }
    }
}
