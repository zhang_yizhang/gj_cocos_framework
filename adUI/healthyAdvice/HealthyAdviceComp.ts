//Tianhe
import { _decorator, Component, Node, SpriteComponent, SpriteFrame, WidgetComponent, LabelComponent, Color, CCString, UITransformComponent, ccenum, Enum, game, tween } from 'cc';
import { EDITOR } from 'cc/env';
const { ccclass, property, executeInEditMode } = _decorator;

enum AgeLevel { '不显示', '8+', '12+', '16+' }
ccenum(AgeLevel)

@ccclass('HealthyAdviceComp')
@executeInEditMode(true)
export class HealthyAdviceComp extends Component {
    @property({ displayName: '著作权人（公司）' })
    set copyrightOwnerText(value: string) {
        this._copyrightOwnerText = value;
        this.node.getChildByName('copyrightOwner').getComponent(LabelComponent).string = '著作权人：' + value;
    }
    get copyrightOwnerText(): string { return this._copyrightOwnerText }
    @property
    _copyrightOwnerText: string = '著作权人：';

    @property({ displayName: '著作权号' })
    set copyrightNumText(value: string) {
        this._copyrightNumText = value;
        this.node.getChildByName('copyrightNum').getComponent(LabelComponent).string = '著作权号：' + value;
    }
    get copyrightNumText(): string { return this._copyrightNumText }
    @property
    _copyrightNumText: string = '著作权号：';

    @property({ type: AgeLevel, displayName: '年龄分级' })
    set ageLevel(value: AgeLevel) {
        this._ageLevel = value;
        this.refreshAgeLevel();
    }
    get ageLevel(): AgeLevel { return this._ageLevel }
    @property
    _ageLevel: AgeLevel = AgeLevel.不显示;

    @property({ type: [SpriteFrame], displayName: '年龄分级图片' })
    ageLevelSpriteList: SpriteFrame[] = [];

    onLoad() {
        this.refreshWidget();
        if (!this.node.getChildByName('advice')) {
            this.newLabelNode('advice', '抵制不良游戏，拒绝盗版游戏。\n注意自我保护，谨防受骗上当。\n适度游戏益脑，沉迷游戏伤身。\n合理安排时间，享受健康生活。', true, 50, 120, 0.15);
        }
        if (!this.node.getChildByName('copyrightOwner')) {
            this.newLabelNode('copyrightOwner', '著作权人：', false, 40, 40, -0.2);
        } else {
            this.node.getChildByName('copyrightOwner').getComponent(LabelComponent).string = '著作权人：' + this._copyrightOwnerText;
        }
        if (!this.node.getChildByName('copyrightNum')) {
            this.newLabelNode('copyrightNum', '著作权号：', false, 40, 40, -0.25);
        } else {
            this.node.getChildByName('copyrightNum').getComponent(LabelComponent).string = '著作权号：' + this._copyrightNumText;
        }
        this.refreshAgeLevel();
        if (!EDITOR) {
            this.scheduleOnce(() => { this.node.destroy() }, 2);
        }
    }

    refreshWidget() {
        const widget = this.getComponent(WidgetComponent) || this.addComponent(WidgetComponent);
        widget.isAlignTop = widget.isAlignBottom = widget.isAlignLeft = widget.isAlignRight = true;
        widget.top = widget.bottom = widget.left = widget.right = 0;
    }

    newLabelNode(nodeName: string, text: string, isBold: boolean, fontSize: number, lineHeight: number, verticalCenter: number) {
        const node = new Node(nodeName);
        node.parent = this.node;
        const label = node.addComponent(LabelComponent);
        label.string = text;
        label.color = Color.BLACK;
        label.isBold = isBold;
        label.fontSize = fontSize;
        label.lineHeight = lineHeight;
        const widget = node.addComponent(WidgetComponent);
        widget.isAlignVerticalCenter = true;
        widget.isAbsoluteVerticalCenter = false;
        widget.verticalCenter = verticalCenter;
    }

    refreshAgeLevel() {
        const ageLevelNode = this.node.getChildByName('ageLevel')!;
        if (ageLevelNode) {
            const sprite = ageLevelNode.getComponent(SpriteComponent)!;
            ageLevelNode.active = this._ageLevel !== 0
            if (!!this.ageLevelSpriteList[this._ageLevel - 1])
                sprite.spriteFrame = this.ageLevelSpriteList[this._ageLevel - 1];
        }
    }
}
