/**
 title 结算页互推
 author NavigateBoxBanner
 date 2020/6/19
 desc
 **/

import AdBase, {AdType} from "./AdBase";
import config, {Platform} from "../../../config/config";
import BlockAdComp from "./BlockAdComp";
import ASCAd from "../../AdSDK/ASCAd";
import * as cc from "cc";

const {ccclass, property} = cc._decorator;
@ccclass
export default class NavigateBoxBanner extends AdBase {
    show(){
        ASCAd.getInstance().getNavigateBoxBannerFlag() && ASCAd.getInstance().showNavigateBoxBanner();
    }

    hide() {
        ASCAd.getInstance().hideNavigateBoxBanner()
    }

    protected onLoad(): void {
        super.onLoad();
        if(cc.isValid(this.node) && config.platform == Platform.oppo){
            let node = new cc.Node();
            let block = node.addComponent(BlockAdComp);
            block.adType = AdType.Banner;
            block.platformEnumList = [Platform.oppo];
            this.node.addChild(node);
        }
    }

}
