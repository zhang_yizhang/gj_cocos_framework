
import { _decorator, Component, Node, Widget, TERRAIN_SOUTH_INDEX, view } from 'cc';
import config, { Platform } from '../../../config/config';
import { PlatformManager } from '../../../Script/Manager/PlatformManager';
import { AndroidEnum } from '../cocosCreator3d/platform/PlatformManagerBase';
const { ccclass, property, requireComponent } = _decorator;

/**
 * Predefined variables
 * Name = bannerSafeAreaWidget
 * DateTime = Thu Mar 24 2022 12:02:43 GMT+0800 (中国标准时间)
 * Author = 234538431
 * FileBasename = bannerSafeAreaWidget.ts
 * FileBasenameNoExtension = bannerSafeAreaWidget
 * URL = db://assets/a_SubModule/lib/adUI/bannerSafeAreaWidget.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */

@ccclass('bannerSafeAreaWidget')
@requireComponent(Widget)
export class bannerSafeAreaWidget extends Component {
    @property
    noBannerBytedance: boolean = true;//抖音
    orginBottom: number = 0;
    widget!:Widget;
    __preload() {
        this.widget = this.node.getComponent(Widget)!
        this.orginBottom =  this.widget.bottom
    }

    // onLoad(){}

    onEnable() {
        let size = view.getVisibleSize();
        let bottom = 200;
        switch (config.platform) {
            case Platform.vivo:{
                
                break
            }
            case Platform.webTest:{
                bottom = 0;
                break
            }
            case Platform.oppo:{
                bottom = 230;
                break
            }
            case Platform.bytedance: {
                bottom = this.noBannerBytedance?0:180;
                break;
            }
            case Platform.androids:
                if (PlatformManager.getAndroidChannel() == AndroidEnum.XIAO_MI || 1005) {
                    bottom = 250;
                } else {
                    bottom = 180;
                }

                break;
            case Platform.wechat:
                bottom = 350;
                break;
            case Platform.ios:
                bottom = size.height / size.width > 2 ? 95 : 150;
                break;
        }

        this.widget.bottom=this.orginBottom+bottom;
        this.widget.updateAlignment()

    }

    onDisable(){
    
    }

    // onEnable(){

    // }

    start() {
        // [3]
    }

    // update (deltaTime: number) {}
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/ccclass.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
