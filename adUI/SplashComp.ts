/**
@class SplashComp
@author YI ZHANG
@date 2021/11/22
@desc
**/
import * as cc from 'cc';
import {_decorator, Component, gfx, sp} from 'cc';
import {EDITOR} from "cc/env";
import config, {Platform} from "../../../config/config";

const { ccclass, property,executeInEditMode} = _decorator;
 
@ccclass('SplashComp')
@executeInEditMode
export class SplashComp extends Component {
    @property(cc.CCString)
    code = "";
    @property(cc.CCString)
    gameName = "";
    @property(cc.CCString)
    authorName = "深圳市小元元科技有限公司";
    @property(cc.CCString)
    issuer = "深圳市益欣网络科技有限公司";
    @property(cc.SpriteFrame)
    whiteBg = null;
    @property(cc.CCInteger)
    space = 180;
    start () {
        if(!this.node.isValid){
            return;
        }
        this.updateView();
        if(!EDITOR){
            this.scheduleOnce(()=>{
                if(this.node.isValid){
                    this.node.destroy();
                }               
            },2)
        }
    }

    updateView(){
        if(EDITOR){
            return;
        }
        let sprite = this.node.getComponent(cc.Sprite)!;
        if(sprite){
            return
        }
        sprite = this.node.addComponent(cc.Sprite);
        if(!this.whiteBg){
            let texture = new cc.Texture2D();
            // @ts-ignore
            texture.reset({width : 2,height :2,format : cc.gfx.Format.RGBA8 });
            // @ts-ignore
            texture.setFilters(cc.gfx.Filter.LINEAR, cc.gfx.Filter.LINEAR);
            let data = new Uint8Array(new ArrayBuffer(16));
            data.fill(255);
            texture.uploadData(data);
            let spriteFrame = new cc.SpriteFrame();
            spriteFrame.texture = texture;
            sprite.spriteFrame = spriteFrame;
            spriteFrame.rotated = true;
            this.getComponent(cc.UITransform)!.setContentSize(cc.view.getVisibleSize());
        }else {
            sprite.spriteFrame = this.whiteBg;
            let size = cc.view.getVisibleSize();
            this.getComponent(cc.UITransform)!.setContentSize(size.width * 3,size.height * 3);
        }


        this.createLab("抵制不良游戏，拒绝盗版游戏。\n注意自我保护，谨防受骗上当。\n适度游戏益脑，沉迷游戏伤身。\n合理安排时间，享受健康生活。",
            58,cc.v3(0,this.space * 2.5,0),120);
        this.createLab(`著作权人: ${this.authorName}`,50,cc.v3(0,-1.5 * this.space));
        let y = -2.5;
        if(this.issuer){
            this.createLab(`运营单位: ${this.issuer}`,50,cc.v3(0,y * this.space));
            y -= 1;
        }
        this.createLab(`著作权号: ${this.code}`,50,cc.v3(0,y * this.space));
        y -= 1;
        if(this.gameName){
            this.createLab(`软著名称: ${this.gameName}`,50,cc.v3(0,y * this.space));
            y -= 1;
        }




    }

    createLab(string : string,size : number,pos : cc.Vec3,lineSize = size){
        let node = new cc.Node();
        this.node.addChild(node);
        let lab = node.addComponent(cc.Label)!;
        lab.string = string;
        lab.fontSize = size;
        lab.color = cc.Color.BLACK;
        lab.lineHeight = 120;
        lab.isBold = true;
        node.setPosition(pos);
        return lab;
    }
}

