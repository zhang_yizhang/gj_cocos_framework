import { _decorator, Component, Node, LabelComponent, LabelOutlineComponent } from 'cc';
import PlatformManagerBase from '../cocosCreator3d/platform/PlatformManagerBase';
import AdBase from './AdBase';
const { ccclass, property, executeInEditMode } = _decorator;

@ccclass('PrivacyAgreementNew')
@executeInEditMode()
export class PrivacyAgreementNew extends AdBase {
    protected onLoad(): void {
        const label = this.getComponent(LabelComponent) || this.addComponent(LabelComponent)!;
        label.string = '隐私协议';
        const labelOutLine = this.getComponent(LabelOutlineComponent) || this.addComponent(LabelOutlineComponent)!;
        labelOutLine.width = 3;
        this.node.on(Node.EventType.TOUCH_END, () => { PlatformManagerBase.getInstance().openProtocol() }, this);
    }
}
