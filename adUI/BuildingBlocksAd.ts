/**
 title 积木广告
 author zhangyizhang
 date 2020/6/19
 desc
 **/
import * as cc from "cc";
import {CCBoolean, CCInteger} from "cc";
import ASCAd from "../../AdSDK/ASCAd";
import AdBase, {AdType} from "./AdBase";
import {EDITOR} from "cc/env";
import BlockAdComp from "./BlockAdComp";
import config, {Platform} from "../../../config/config";


const {ccclass, property} = cc._decorator;
enum Dir
{
    HORIZONTAL="landscape",
    VERTICAL="",
}
let DirEnum =  cc.Enum(Dir);
@ccclass
export default class BuildingBlocksAd extends AdBase {
    @property({type : cc.Enum(Platform)})
    noBlockAdPlatformEnumList : Platform[] = [];
    private blockNode?: cc.Node;
    static isOpen : boolean = true;

    protected onLoad(): void {
        super.onLoad();
        this.redraw();
        if(cc.isValid(this.node) && this.noBlockAdPlatformEnumList.indexOf(config.platform) == -1){
            let node = new cc.Node();
            let block = node.addComponent(BlockAdComp);
            block.adType = AdType.Banner;
            block.platformEnumList = [];
            this.blockNode = node;
            this.blockNode.active = false;
            this.node.addChild(node);
        }
    }
    @property(CCInteger)
    _num : number = 1;
    @property({type : CCInteger})
    set num(num){
        if(num > 0 && num <= 5){
            this._num = num;
            this.redraw();
        }
    }
    get num(){
        return this._num;
    }



    redraw(){
        if(!EDITOR){
            (this.node.getComponent(cc.UIOpacity) || this.node.addComponent(cc.UIOpacity))!.opacity = 1;
            return;
        }
        let space = 10;
        let size = 140;
        let width = size * this.num + space * (this.num - 1);
        let height = size * this.num + space * (this.num - 1) ;
        this.node.getComponent(cc.UITransform)!.setContentSize(cc.size(width,height));
    }

    protected onEnable(): void {
        super.onEnable();
        if(this.blockNode){
            this.blockNode.active = !ASCAd.getInstance().getBlockFlag();
        }

    }

    protected onDisable(): void {
        super.onDisable();
        if(this.blockNode){
            this.blockNode.active = false;
        }

    }

    hide() {
        ASCAd.getInstance().hideBlock();
    }

    show() {
        let comp = this.node.getComponent(cc.UITransform)!;
        let pos = comp.convertToWorldSpaceAR(cc.v3(0,0,0));
        let viewSize = cc.view.getVisibleSize();
        if(ASCAd.getInstance().getBlockFlag()){
            if(this.blockNode) {
                this.blockNode.active = true;
            }
            if(config.platform==Platform.qq){
                ASCAd.getInstance().showBlock("landscape",pos.x / viewSize.width,
                    (viewSize.height - pos.y) / viewSize.height,5);
            }else {
                ASCAd.getInstance().showBlock(0,0,0,0);
            }
        }else {
            if(this.blockNode) {
                this.blockNode.active = false;
            }
        }
    }

    // update (dt) {}
}