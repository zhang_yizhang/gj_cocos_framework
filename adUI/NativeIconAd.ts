/**
@class NativeIconAd
@author YI ZHANG
@date 2020/9/9
@desc
**/
import AdBase from "./AdBase";
import ASCAd from "../../AdSDK/ASCAd";
import * as cc from "cc";
import config, {Platform} from "../../../config/config";
import PlatformManagerBase, { AndroidEnum } from "../cocosCreator3d/platform/PlatformManagerBase";

const {ccclass, property} = cc._decorator;
@ccclass
export default class NativeIconAd extends AdBase {
    @property
    alwaysEnable:boolean=false;

    protected onLoad(): void {
        super.onLoad();
    }

    show(){
        if(config.platform == Platform.webTest){
            return;
        }
        let comp = this.node.getComponent(cc.UITransform)!;
        let pos = comp.convertToWorldSpaceAR(cc.v3(0,0,0));
        let size = cc.view.getVisibleSize();
        if(config.platform == Platform.wechat){
            pos.x -= comp.width / 2;
            pos.y += comp.height / 2;
        }
        if(PlatformManagerBase.getAndroidChannel()==AndroidEnum.VIVO){
            const fun=(num:number)=>{return Math.round(num*10)/10}
            pos.set(fun(this.node.worldPosition.x/size.x) , fun(1-this.node.worldPosition.y/size.y))
        }
        ASCAd.getInstance().getNativeIconFlag() && ASCAd.getInstance().showNativeIcon(comp.width, comp.height, pos.x , pos.y);
    }

    hide() {
        if(this.alwaysEnable)return;
        ASCAd.getInstance().hideNativeIcon();
    }

}
