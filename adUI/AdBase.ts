/**
 title 广告基类
 author NavigateSettleAd
 date 2020/6/19
 desc
 **/

import * as cc from "cc";
import config, {Platform} from "../../../config/config";
import {DEBUG, EDITOR} from "cc/env";
import ASCAd from "../../AdSDK/ASCAd";

export enum AdType {
    Banner = 1,
    NativeIcon = 2,
    NavigateIcon = 3,
    NavigateSettleAd = 4,
    NativeImage = 5,
    BuildingBlocksAd = 6,
    NavigateBoxBannerAD = 7,
    PrivacyAgreement=8,
    ContactEmail=9,
    HealthyGameAdivce=10,
}

const {ccclass, property} = cc._decorator;
let isShowAd = true;
let adClassList : any[] = [];
@ccclass
export default  class AdBase extends cc.Component {
    platformEnumList : Platform[] = [];
    androidEnumList : AndroidEnum[] = [];
    static instanceAdList : AdBase[] = [];
    protected Class !: typeof AdBase;
    private _curSchedule: number = -1;
    static blockNum: number;
    static curInstance ?: AdBase;
    static _instanceAdList = false;

    protected onLoad(): void {
        if(EDITOR){
            return;
        }
        if(this.platformEnumList.length > 0 && this.platformEnumList.indexOf(config.platform) == -1){
            this.node.active = false;
            if(this.node){
                this.node.destroy();
            }
            return;
        }

        let id = PlatformManagerBase.getAndroidChannel();
        if(this.androidEnumList.length > 0 && config.platform == Platform.androids && this.androidEnumList.indexOf(id) == -1){
            this.node.active = false;
            if(this.node){
                this.node.destroy();
            }
            
            return;
        }
        // @ts-ignore
       this.Class = this.__proto__.constructor;
        if(!this.Class._instanceAdList){
            adClassList.push(this.Class);
            this.Class.instanceAdList = [];
            this.Class._instanceAdList = true;
            if(!isShowAd){
                this.Class.changeBlockNum(1);
            }
        }

    }



    show(){

    }

    hide(){

    }

    protected onEnable(): void {
        if(EDITOR){
            return;
        }
        this.unschedule(this._show);
        this.scheduleOnce(this._show,0.4);

    }

    _show(){
        if(!cc.isValid(this) || !this.enabledInHierarchy){
            return;
        }
        this.Class && this.Class.push(this);
    }

    static push(instance : AdBase){
        this.instanceAdList.push(instance);
        if(this.instanceAdList.length > 1){
            return;
        }
        if(!this.blockNum){
            instance.Class.curInstance = instance;
            instance.show();
        }

    }

    static remove(instance : AdBase){
        for(let i = 0;i < this.instanceAdList.length;++i){
            if(this.instanceAdList[i] == instance){
                let item = this.instanceAdList.splice(i,1)[0];
                if(i == 0 && !this.blockNum){
                    item.hide();
                    item.Class.curInstance = undefined;
                    if(this.instanceAdList.length){
                        this.instanceAdList[0].Class.curInstance = this.instanceAdList[0];
                        this.instanceAdList[0].show();
                    }
                }
                break;
            }
        }
    }

    protected onDisable(): void {
        if(EDITOR){
            return;
        }
        this.Class && this.Class.remove(this);
    }

    static hide(){
        if(this.instanceAdList.length > 0){
            this.instanceAdList[0].Class.curInstance = undefined;
            this.instanceAdList[0].hide();
        }
    }

    static changeBlockNum(num : number){
        // @ts-ignore
        if(!this._initBlockNum){
            this.blockNum = 0;
            // @ts-ignore
            this._initBlockNum = true;
        }
        let lastNum = this.blockNum;
        this.blockNum  += num;
        if(lastNum <= 0 && this.blockNum > 0){
            this.hide();
        }
        else if(lastNum > 0 && this.blockNum <= 0){
            this.show();
        }
        this.blockNum = Math.max(0,this.blockNum);
    }

    static setIsShowAd(isShow : boolean){
        if(isShow == isShowAd){
            isShowAd = isShow;
            for(let i = 0;i < adClassList.length;++i){
                adClassList[i].changeBlockNum(isShow ? -1 : 1);
            }
        }
    }

    static show(){
        if(this.instanceAdList.length > 0){
            this.instanceAdList[0].Class.curInstance = this.instanceAdList[0];
            this.instanceAdList[0].show();
        }
    }

    // update (dt) {}
}
import PlatformManagerBase, {AndroidEnum} from "../cocosCreator3d/platform/PlatformManagerBase";
