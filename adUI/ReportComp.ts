
import * as cc from "cc";
import {DEBUG, PREVIEW} from "cc/env";
import config, { LangType, Platform } from "../../../config/config";
import { PlatformManager } from "../../../Script/Manager/PlatformManager";
import ASCAd from "../../AdSDK/ASCAd";
import GetConfig from "../../AdSDK/utils/GetConfig";

/**
 title 打点
 author zhangyizhang
 date 2020/6/18
 desc
 **/

const {ccclass, property} = cc._decorator;

@ccclass
export default class ReportComp  {
    static ReportData : {[key : string] : string};
    static EventMap : {[key : string] : string[]};
    static setReportData(ReportData : {[key : string] : string}){
        let EventMap : {[key : string] : string[]} = {

        };
        let keys = Object.keys(ReportData);
        for(let i = 0;i < keys.length;++i){
            if(!ReportData[keys[i]]){
                continue;
            }
            let event = keys[i].split("_")[0];
            EventMap[event] = EventMap[event] || [];
            EventMap[event].push(keys[i]);
        }
        ReportComp.ReportData = ReportData;
        ReportComp.EventMap = EventMap;
    }

    /**
     *
     * @param eventType 事件类型
     * @param isAllReport 是否全部上报  douyin需要全部上报才有效
     * @param type
     */
    static report(eventType : string,type = 1){
        let event = ReportComp.ReportData[eventType].split("_")[0];
        let eventData = ReportComp.EventMap[event];
        let data : any = {};
        // @ts-ignore
        let isAllReport = !!(globalThis.tt);
        if(!isAllReport && type == 1){
            data[event] = eventType;
        }else {
            for(let i = 0;i < eventData.length;++i){
                data[eventData[i]] = eventData[i] == ReportComp.ReportData[eventType] ? type : 0;
            }
        }
        let keys = Object.keys(data);
        for (let i = 0; i < keys.length; ++i) {
            if (typeof data[keys[i]] === "boolean") {
                data[keys[i]] = data[keys[i]] ? "1" : "0";
            }
            data[keys[i]] = "" + data[keys[i]];
        }
        this.customReport(event,data);
    }

    static umaUtil : any = null;
    static getUmaUtil() : any{
        let reportParent = null;
        switch (GetConfig.getChannelName()) {
            case "oppo":
            case "vivo":
            case "huawei":
            case "xiaomi":
                reportParent = globalThis;
                break;
            case "tiktok":
                // @ts-ignore
                reportParent = globalThis.tt;
                break;
            case "weixin":
                // @ts-ignore
                reportParent = globalThis.wx;
                break;
            case "tencentqq":
                // @ts-ignore
                reportParent = globalThis.qq;
                break;
            case "kwai":
                // @ts-ignore
                reportParent = globalThis.kwaigame;
                break;
        }
        this.umaUtil = reportParent && reportParent.uma;
        this.getUmaUtil = ()=>{
            return this.umaUtil;
        };
        return this.getUmaUtil();
    }

    static reportByKey(key : string,type = 1){
        this.report(ReportComp.ReportData[key],type);
    }

    static getBeforeReportFlag(){
        if(!this.preEvent){
            return false;
        }
        return this.preEvent.event!='EventHide'
    }
    static getBeforeReportData(ignoreEvent = ""){
        if(!this.preEvent || this.preEvent.event == ignoreEvent){
            return "";
        }
        return '{"'+this.preEvent.event+'":'+JSON.stringify(this.preEvent.data)+'}';
    }

    static preEvent ?: {event : string,data : any};
    static customReport(event : string,data : any ,reportLv:number=10){
        this.preEvent = {event : event,data : data};
        let uma = this.getUmaUtil();
        if(uma){
            uma.trackEvent(event, data); 
            return;
        }
        if(PREVIEW){
            cc.log("reportAnalytics:", event, data);
        }
        PlatformManager.getInstance().customReport(event,data,reportLv)
        // if(config.platform===Platform.google){
        //     ASCAd.getInstance().customEvent(reportLv,event,data);
        // }else{
        //     ASCAd.getInstance().reportAnalytics(event,data);
        // }
    }

    /**
     * 用户属性上报
     * @param data {}
     */
    static setUser(data:any){
        if(config.langType==LangType.cn)return
        console.log('setUser:',JSON.stringify(data))
        ASCAd.getInstance().setUser(data);
    }

    static customReport2(event : string,data : any){
        this.preEvent = {event : event,data : data};
        let uma = this.getUmaUtil();
        if(uma){
            uma.trackEvent(event, data); 
            return;
        }
        if(PREVIEW){
            cc.log("reportAnalytics:", event, data);
        }
        
    }
}
