/**
@class Annotation
@author YI ZHANG
@date 2020/8/31
@desc
**/
import {Type} from "../mvvm/MvvmUtils/modelUnit";
export namespace Annotation{
    export const bindSet = function(func : (oldValue : any,newValue : any)=>any){
        return function (proto : any,name : string) {
            let key = "_*bindSet_key_" + name;
            Object.defineProperty(proto, name, {
                enumerable: true,
                configurable: true,
                get(): any {
                    return this[key]
                },
                set(value : any){
                    let oldValue = this[key];
                    this[key] = value;
                    this[key] = func.call(this,value,oldValue) || value
                }
            })
        }
    };
}
