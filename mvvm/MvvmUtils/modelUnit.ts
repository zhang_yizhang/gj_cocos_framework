/** 数据工具类
 @class modelUnit
 @author YI ZHANG
 @date 2020/7/8
 @desc
 **/
import * as cc from "cc";

export let saveToLocal = function(key : string,value : any){
    cc.sys.localStorage.setItem(key,JSON.stringify(value));
};
export let loadFromLocal = function (key: string) {
    return cc.sys.localStorage.getItem(key) || null;
};
let deleteLocalByKey = function (key: string) {
    cc.sys.localStorage.removeItem(key);
};
export interface ModelBase {
    setSaveKey(key : string):void;
    save(key : string):void;
    init<T extends {}>(this: (new () => T)): T
}
export enum Type {
    ATTR,
    OBJECT,//粒度细分暂未实现  暂不可用
}
let dirtyModel = new Map<number,{_$dirtyMap : Map<string,boolean>}>();
let modelId = 1;
export default class ModelUnit{
    static serializationAttr(type : Type,defaultValue ?: any,Class ?: any) {
        return function (proto : any,name : string) {
            if(!proto["_$keys"]){
                Object.defineProperty(proto,  "_$keys", {
                    enumerable: false,
                    configurable: false,
                    value : {}
                })
            }
            proto["_$keys"][name] = {name : name,type : type,Class : Class,defaultValue : defaultValue};
        }
    }

    static serializeClass()  {
        return function<T extends {new(...args:any[]):{}}>(constructor:T){
            let proto = constructor.prototype;
            // @ts-ignore
            let Class = class extends constructor {
                _$_canSave = false;
                _$dirtyMap  = new Map<string,boolean>();
                _$modelId !: number;
                setIsSave(isSave : boolean){
                    this._$_canSave = isSave;
                }
                processObject(value : any,key : string){
                    let self = this;
                    if(!value){
                        debugger;
                    }
                    let keys = Object.keys(value);
                    for(let i = 0;i < keys.length;++i){
                        // @ts-ignore
                        let item = value[keys[i]];
                        if(typeof item == "object"){
                            value[keys[i]] = this.processObject(item,key);
                        }
                    }
                    return  new Proxy(value, {
                        set(target: any, p: string, value: any, receiver: any): boolean {
                            if(typeof value == "object"){
                                value = self.processObject(value,key);
                            }
                            self.setDirtyKey(key);
                            return Reflect.set(target,p,value,receiver);
                        },
                    })
                }
                setDirtyKey(key : string){
                    if(this._$_canSave){
                        this._$dirtyMap.set(key,true);
                        dirtyModel.set(this._$modelId,this);
                    }
                }
                setSaveKey(key : string){
                    this._$idKey = key;
                }
                constructor() {
                    super();

                }
                init(){
                    this._$modelId = modelId++;
                    let dataList = proto["_$keys"];
                    let keys = Object.keys(dataList || {});
                    this._$_load();
                    this.setIsSave(true);
                    let self = this;
                    let set = new Set<string>();
                    for(let i = 0;i < keys.length;++i){
                        let key = keys[i]
                        set.add(key);
                        if(typeof (<any>this)[key] == "object"){
                            (<any>this)[key] = self.processObject((<any>this)[key],key);
                        }
                    }
                    let target = new Proxy(this, {
                        set(target: any, p: string, value: any, receiver: any): boolean {
                            if(!set.has(p)){
                                target[p] = value;
                                return true;
                            }
                            if(typeof value == "object"){
                                value = self.processObject(value,p);
                            }
                            self.setDirtyKey(p);
                            return Reflect.set(target,p,value,receiver);
                        },
                    });
                    return target;
                }
                save(key : string){
                    this._$_canSave && this._$_saveAttr(key,(<any>this)[key]);
                }
                _$_save(){
                    // @ts-ignore
                    let keys : {name : string,type : Type}[] = this.__proto__["_$keys"]
                    for(let i = 0;i < keys.length;i++){
                        let data = keys[i]
                        this._$_saveAttr(data.name,(<any>this)[data.name]);
                    }
                }

                _$_load(){
                    let dataList = proto["_$keys"];
                    let keys = Object.keys(dataList);
                    for(let i = 0;i < keys.length;i++){
                        let data = dataList[keys[i]];
                        let key = data.name;
                        this._$_loadAttr(key)
                    }
                }

                _$_saveAttr(key : string,value : any){
                    let data :  {name : string,type : Type} = proto["_$keys"][key];
                    switch (data.type) {
                        case Type.ATTR:
                            saveToLocal(this._$idKey + "/" + key,value);
                            break;
                    }
                }

                _$_loadAttr(key : string){
                    let data :  {name : string,type : Type,Class : any,defaultValue ?: any} = proto["_$keys"][key];
                    let localStr = loadFromLocal(this._$idKey + "/" + key);
                    switch (data.type) {
                        case Type.ATTR:
                            if(localStr === null){
                                return;
                            }
                            let value = JSON.parse(localStr);
                            (<any>this)[key] = data.Class ? data.Class(value) : value;
                            break;
                    }
                }


                _$_deleteItem(key : string){
                    let data :  {name : string,type : Type} = proto["_$keys"][key];
                    let value = (<any>this)[key];
                    switch (data.type) {
                        case Type.ATTR:
                            deleteLocalByKey( this._$idKey + "/" + key);
                            break;
                    }
                }

                _$_destroy(){
                    let dataList = proto["_$keys"];
                    for(let key in dataList){
                        this._$_deleteItem(key);
                    }
                }

                _$_initObj(key : string,value : any){
                    let oldValue = (<any>this)["_$KEY_" + key];
                    if(oldValue){
                        for(let key in value){
                            oldValue[key] = value[key];
                        }
                        return oldValue;
                    }
                    return value;
                }
            };
            return Class;

        }

    }
}
setInterval(()=>{//数据持久化处理
    dirtyModel.forEach((value : any)  => {
        value._$dirtyMap.forEach(
            (key : any,value1 : any) => {
                value._$_saveAttr(value1,value[value1]);
            });
        value._$dirtyMap.clear();
    });
    dirtyModel.clear();
},100);