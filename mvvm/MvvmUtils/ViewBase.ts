/** 视图基类
 @class ViewBase
 @author YI ZHANG
 @date 2020/7/8
 @desc
 **/

import Watcher from "../ccc-vue/Watcher";
import Vue from "../ccc-vue/Vue";
export default class ViewBase{

    private _watcherList : Watcher[] = [];
    private _isDisableWatch : boolean = false;

    /**
     * 暂停监听
     */
    disableWatch(){
        if(this._isDisableWatch == true){
            return;
        }
        this._isDisableWatch = true;
        this.updateWatchActive();
    }

    /**
     * 开启监听
     */
    enableWatch(){
        if(this._isDisableWatch == false){
            return;
        }
        this._isDisableWatch = false;
        this.updateWatchActive();
    }

    /**
     * 添加观察者
     * @param watcher 观察者
     * @param value 该值只是为了触发绑定
     * @param isNoTrigger
     */
    addWatcher(watcher : Watcher,value : any,isNoTrigger ?: boolean) : Watcher{
        this._watcherList = this._watcherList || [];
        watcher.active = !this._isDisableWatch;
        Vue.bind(watcher,value,isNoTrigger);
        this._watcherList.push(watcher)
        return watcher;
    }

    /**
     * 移除指定观察者
     * @param watcher
     */
    removeWatcher(watcher : Watcher){
        this._watcherList = this._watcherList || [];
        for(let i = 0;i < this._watcherList.length;++i){
            if(this._watcherList[i] === watcher){
                this._removeWatcher(this._watcherList[i])
                this._watcherList.splice(i,1);
                return;
            }
        }
    }

    /**
     * 根据vm移除观察者
     * @param vm
     */
    removeWatcherByVm(vm : Vue){
        this._watcherList = this._watcherList || [];
        for(let i = 0;i < this._watcherList.length;++i){
            if(this._watcherList[i].vm === vm){
                this._removeWatcher(this._watcherList[i])
                this._watcherList.splice(i--,1);
            }
        }
    }

    /**
     * 移除所有监听
     */
    removeAllWatch(){
        this._watcherList = this._watcherList || [];
        for(let i = 0;i < this._watcherList.length;++i){
            this._removeWatcher(this._watcherList[i])
            this._watcherList.splice(i--,1);
        }
    }

    private _removeWatcher(watcher : Watcher){
        watcher.teardown();
    }

    private updateWatchActive(){
        this._watcherList = this._watcherList || [];
        for(let i = 0;i < this._watcherList.length;++i){
            this._watcherList[i].active = !this._isDisableWatch;
            if(!this._isDisableWatch){
                this._watcherList[i].trigger(true);
            }
        }
    }


}
