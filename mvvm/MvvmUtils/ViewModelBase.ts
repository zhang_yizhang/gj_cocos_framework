/** 视图模型基类
@class ViewModelBase
@author YI ZHANG
@date 2020/7/17
@desc
**/

export abstract class ViewModelBase {
    static viewModelUpdateList : {priority : number,list : ViewModelBase[]}[] = [];
    protected constructor(viewModelPriority : number) {
        ViewModelBase.push(viewModelPriority,this);
    }
    abstract init():ViewModelBase
    static push(priority : number,view : ViewModelBase){
        let i = 0;
        for(;i < this.viewModelUpdateList.length;++i){
            if(this.viewModelUpdateList[i].priority == priority){
                this.viewModelUpdateList[i].list.push(view);
                return;
            }
            if(this.viewModelUpdateList[i].priority > priority){
                break;
            }
        }
        this.viewModelUpdateList.push({priority : priority,list : [view]});
    }
    abstract addWatch() : void;
    abstract update(dt : number) : void;
}