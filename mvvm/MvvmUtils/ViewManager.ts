/** 视图管理器
@class ViewManager
@author YI ZHANG
@date 2020/7/13
@desc
**/
import * as cc from "cc";
import CCUiBase from "../../../lib/cocosCreator3d/mvvm/ui/CCUiBase";
import CCPanelBase from "../../../lib/cocosCreator3d/mvvm/ui/PanelBase";
import { UiLoadConfig } from "../UiManager/UIBase";
import CCLoadItem from "../../cocosCreator3d/mvvm/ui/CCLoadItem";
import { LoadPriority } from "../LoadManager/LoadManager";
import { UiManager, UiShowType, UiType } from "../UiManager/UiManager";
import { LoadDelegate, LoadState } from "../LoadManager/LoadItem";

export interface ViewLoadData {//视图加载数据
    loadItem: {
        type: new () => any
        resUrl: string
    },
    priority: number
}
export interface ViewData {
    dependLoadItem: ViewLoadData//依赖项
    preLoadItemList?: ViewLoadData[]//预加载依赖项
    isNoShowBlock?: boolean;//是否不显示加载屏蔽层  屏蔽一直有效
    preLoadUI?: ({ viewType: number, loadPriority: LoadPriority })[];//预加载ui列表
    preUiList?: ({ viewType: number, loadPriority?: LoadPriority })[],//该UI之前的UI列表，将会在这些列表中加依赖
    UiBase?: CCUiBase//该视图挂载的UiBase
    uiType: UiType//视图类型
    maskData?: {//遮罩数据
        isMask: boolean,//是否显示遮罩 默认 true
        isBlock: boolean,//是否屏蔽下层 默认 true
        maskOpacity: number,//遮罩透明度 默认 180
        color?: cc.Color,//遮罩颜色 默认黑色
    },
}
export interface ViewPanelInterface { [key: number]: ViewData }

export default class ViewManager {//视图管理器
    private static blockSprite: cc.SpriteFrame = null!;
    public static ViewPanelMap: ViewPanelInterface;
    static init(ViewPanel: ViewPanelInterface, blockSpriteFrame: cc.SpriteFrame) {
        this.blockSprite = blockSpriteFrame;
        this.ViewPanelMap = ViewPanel;
        for (let key in ViewPanel) {
            let value = ViewPanel[key];
            if (!value.preUiList) {
                continue;
            }
            for (let i = 0; i < value.preUiList.length; ++i) {
                let preData = value.preUiList[i];
                let item = ViewPanel[preData.viewType];
                if (!item) {
                    continue;
                }
                item.preLoadUI = item.preLoadUI || [];
                item.preLoadUI.push({ viewType: Number(key), loadPriority: preData.loadPriority || LoadPriority.PreLoadUI });
            }
        }
    }

    /**
     * 缓存ui
     * @param viewType
     * @param complete
     * @param progress
     */
    static async cacheUi(viewType: number, complete?: (state?: LoadState) => void, progress?: (progress: number) => void) {
        let viewData = this.ViewPanelMap[viewType];
        let UiBase = viewData.UiBase!;
        if (!UiBase) {
            this.ViewPanelMap[viewType].UiBase = new CCUiBase(viewData, viewType, viewData.uiType);
            UiBase = viewData.UiBase!
        }
        await UiBase.loadUiRes();
        // UiBase.preInit()
    }

    static openUi(viewType: number, extData: any, { showType = UiShowType.PUSH,
        cb, dependLoadItems = [], loadDelegateList = [] }:
        {
            showType?: UiShowType, cb?: (isSuccess: boolean) => void,
            dependLoadItems?: UiLoadConfig<CCLoadItem<cc.Asset>>[], loadDelegateList?: LoadDelegate[]
        }) {
        let viewData = this.ViewPanelMap[viewType];
        let UiBase = viewData.UiBase!;
        if (!UiBase) {
            this.ViewPanelMap[viewType].UiBase = new CCUiBase(viewData, viewType, viewData.uiType);
            UiBase = viewData.UiBase!
        }
        UiBase.tempDependLoadItemsEx = dependLoadItems;
        UiBase.tempLoadDelegateList = loadDelegateList;
        UiManager.getInstance().openUI(UiBase, showType, extData, (isSuccess) => {
            cb && cb(isSuccess);
        });
    }

    static closeUi(viewType: number, extData?: any) {
        let viewData = this.ViewPanelMap[viewType];
        let UiBase = viewData.UiBase;
        if (UiBase) {
            UiManager.getInstance().closeUI(UiBase, extData);
        }
    }

    static closeUiType(type: UiType) {
        for (const key in this.ViewPanelMap) {
            if (Object.prototype.hasOwnProperty.call(this.ViewPanelMap, key)) {
                if (this.ViewPanelMap[key].uiType == type) {
                    let UiBase = this.ViewPanelMap[key].UiBase;
                    if (UiBase) {
                        UiManager.getInstance().closeUI(UiBase,null);
                    }
                }
            }
        }

    }

    static getUi(viewType: number): CCPanelBase | null {
        let viewData = this.ViewPanelMap[viewType];
        let UiBase = viewData.UiBase;
        return UiBase && cc.isValid(UiBase.getPanel()) && UiBase.getPanel()!.getComponent(CCPanelBase) || null;
    }

    static isShow(viewType: number) {
        let ui = this.getUi(viewType);
        return ui && ui.enabledInHierarchy;
    }

    /**
     * 设置纯白色块图
     * @param spriteFrame
     */
    static setBlockSprite(spriteFrame: cc.SpriteFrame) {
        this.blockSprite = spriteFrame;
    }

    /**
     * 获取纯白色块图
     */
    static getBlockSprite() {
        return this.blockSprite;
    }
}