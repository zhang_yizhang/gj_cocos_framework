/** 单例基类
@class SingletonPatternBase
@author YI ZHANG
@date 2020/7/9
@desc
**/
import * as cc from "cc";

export default class SingletonPatternBase
{
    protected static instance: SingletonPatternBase;
    static getInstance<T extends {}>(this: (new () => T)): T {
        if(!(<any>this).instance){
            (<any>this).instance = new this();
        }
        return (<any>this).instance;
    }
}

export  class CCCSingletonPatternBase extends cc.Component
{
    protected onLoad(): void {
        // @ts-ignore
        this.__proto__.constructor.instance = this;
    }

    protected static instance: SingletonPatternBase;
    static getInstance<T extends {}>(this: (new () => T)): T {
        if(!(<any>this).instance){
            (<any>this).instance = new this();
        }
        return (<any>this).instance;
    }
}
