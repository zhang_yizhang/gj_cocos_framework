export enum ChangeType {//变更类型
    ARRAY_ADD,
    ARRAY_UPDATE,
    ARRAY_DEL,
    ARRAY_MOVE,
    ARRAY_REFRESH,
}