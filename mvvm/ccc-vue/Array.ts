import * as _u from './VueUtils';
import {ChangeType} from "./VueNotifyType";

const arrayProto = Array.prototype
export const arrayMethods = Object.create(arrayProto)

const methodsToPatch = [
    'push',
    'pop',
    'shift',
    'unshift',
    'splice',
    'sort',
    'reverse'
]
function createMoveList(i,moveNum,length){
    let move = [];
    for(;i < length;++i){
        move.push([i,i + moveNum]);
    }
    return move;
}
/**
 * Intercept mutating methods and emit events
 */
methodsToPatch.forEach(function (method) {
    // cache original method
    const original = arrayProto[method]
    _u.def(arrayMethods, method, function mutator(...args) {
        let oldLength = this.length;
        const result = original.apply(this, args)
        const ob = this.__ob__
        let inserted
        let length = this.length
        switch (method) {
            case 'push':
                ob.dep.notify(ChangeType.ARRAY_ADD,[this.length - 1]);
                inserted = args
                break
            case 'unshift':
                let move = createMoveList(0,1,oldLength);
                ob.dep.notify(ChangeType.ARRAY_MOVE,move);
                ob.dep.notify(ChangeType.ARRAY_ADD,[0]);
                inserted = args
                break
            case 'splice':
                inserted = args.slice(2);
                let num = args[1];
                let moveNum = -num + inserted.length;
                if(num > 0){
                    let deleteList = [];
                    for(let i = 0;i < num;++i){
                        deleteList.push(args[0] + i)
                    }
                    ob.dep.notify(ChangeType.ARRAY_DEL,deleteList);
                }

                if(moveNum != 0){
                    let move1 = createMoveList(args[0] + num,moveNum,oldLength - args[0] - num);
                    ob.dep.notify(ChangeType.ARRAY_MOVE,move1);
                }
                if(inserted.length > 0){
                    let addList = [];
                    for(let i = 0;i < inserted.length;++i){
                        addList.push(args[0] + i);
                    }
                    ob.dep.notify(ChangeType.ARRAY_ADD,addList);
                }
                break
            case 'pop':
                ob.dep.notify(ChangeType.ARRAY_DEL,[length]);
                break;
            case 'shift':
                ob.dep.notify(ChangeType.ARRAY_DEL,[0]);
                if(length > 0){
                    let move3 = createMoveList(1,-1,oldLength - 1);
                    ob.dep.notify(ChangeType.ARRAY_MOVE,move3);
                }
                break;
            case 'sort':
                ob.dep.notify(ChangeType.ARRAY_REFRESH,[0]);
                break;
            case 'reverse':
                let move4 = [];
                for(let i = 0;i < length;++i){
                    move4.push([i,length - i - 1])
                }
                ob.dep.notify(ChangeType.ARRAY_MOVE,move4);
                break;

        }
        if (inserted) ob.observeArray(inserted)
        // notify change
        ob.dep.notify()
        return result
    })

    arrayMethods.set = function (index,value) {
        if(index < 0 || index > this.length){
            throw new Error("set arrayMethods index error");
        }
        const ob = this.__ob__
        this[index] = value;
        if(index != this.length){
            ob.dep.notify(ChangeType.ARRAY_UPDATE,[index]);
        }else {
            this.push(value);
        }
        ob.observeArray(value)
    }
})