/*
 * @Author: kunsi 
 * @Date: 2019-04-20 13:21:51 
 * @Last Modified by: kunsi
 * @Last Modified finishTime: 2019-04-20 13:23:53
 */

import {observe} from './VueObserver';
import Watcher from "./Watcher";
import {ChangeType} from "./VueNotifyType";
export default class Vue {
    static getVm<T>(data : T) : T {
        observe(data);
        return data;
    }

    /**
     * 订阅观察，当观察的值发生变化时触发 watcher的回调函数
     *
     * @param   {Watcher}  watcher  
     * @param   {any}      value    要观察的值
     *
     * @param isNoTrigger
     * @return  {[type]}            Watcher
     */
    static bind(watcher: Watcher, value ?: any,isNoTrigger ?: boolean) {
        watcher.init(isNoTrigger);
        return watcher;
    }

    /**
     *Creates an instance of Watcher.
     * @param {Vue} vm
     * @param {(value, oldValue) => void} cb 值改变会自动执行该回调函数
     * @param {*} 执行回调函数的target，防止this被吞噬
     * @memberof Watcher
     */
    static createWatch( vm: Vue, cb ?:  (value : any,oldValue : any,target : any,changeType : ChangeType,changeData : any) => void, target ?: any){
        // @ts-ignore
        let watch = new Watcher(vm,cb,target);
        return(value:any,isNoTrigger ?: boolean)=>{
            Vue.bind(watch,isNoTrigger)
        }
    }
}