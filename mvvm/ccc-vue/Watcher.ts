import * as _u from './VueUtils';
import Set from './Set';

import Dep, {
    pushTarget,
    popTarget
} from './Dep'
import Vue from "./Vue";
import {ChangeType} from "./VueNotifyType";
import * as cc from "cc";

let uid = 0;

/**
 * A watcher parses an expression, collects dependencies,
 * and fires callback when the expression value changes.
 * This is used for both the $watch() api and directives.
 */
export default class Watcher {
    express;
    vm: Vue;
    id: number;

    deps: Array < Dep > ;
    newDeps: Array < Dep > ;
    depIds: Set;
    newDepIds: Set;
    deep: boolean;
    active: boolean;
    before?:  Function;
    getter: Function;
    target: any;
    expression: string | Function;
    oldArrayLength: number;
    cb: (value : any,oldValue : any,target : any,changeType : ChangeType,changeData : any) => void;
    value: any;
    isCCObject : boolean = false;
    private isChange: any;
    private oldValue: any;
    /**
     *Creates an instance of Watcher.
     * @param {Vue} vm
     * @param {(value, oldValue) => void} cb 值改变会自动执行该回调函数
     * @param {*} 执行回调函数的target，防止this被吞噬
     * @memberof Watcher
     */
    private constructor(
        vm: Vue,
        cb ?:  (value : any,oldValue : any,target : any,changeType : ChangeType,changeData : any) => void,
        target ?: any,
    ) {
        this.express = [];
        this.vm = vm;
        this.id = ++uid

        this.deps = []
        this.newDeps = []
        this.depIds = new Set()
        this.newDepIds = new Set()
        this.oldArrayLength = 0;
        this.isChange = false;
        pushTarget(this)
        this.active = true;
        cb && this.setCallback(cb,target);
        // this.value = this.get();
    }

    static createWatch( vm: Vue,
                        cb :  ((value : any,oldValue : any,target : any,changeType : ChangeType,changeData : any) => void) | undefined,
                        target : any,){
        return new Watcher(vm,cb,target);
    }

    setCallback(cb : (value : any,oldValue : any,target : any,changeType : ChangeType,changeData : any)=>void,target){
        this.target = target;
        this.isCCObject = (this.target instanceof cc.Component) || (this.target instanceof cc.Node);
        this.cb = (value : any,oldValue : any,target : any,changeType : ChangeType,changeData : any)=>{
            if(!this.active){
                this.value = value;
                this.oldValue = oldValue;
                this.isChange = true;
            }
            if(this.isCCObject){
                if(!cc.isValid(this.target)){
                    this.teardown();
                    return;
                }
            }
            this.active && cb.call(this.target,value,oldValue,target,changeType,changeData);
        }

    }

    trigger(isRefresh ?: boolean){
        if(this.isChange || isRefresh){
            this.isChange = false;
            this.cb.call(this.target, this.value,this.oldValue,this.vm);
        }

    }

    init(isNoTrigger ?: boolean) {
        this.value = this.get();
        this.oldValue = this.value;
        !isNoTrigger && this.cb.call(this.target, this.value,this.value,this.vm);
    }

    checkTargetValid(){

    }

    /**
     * Evaluate the getter, and re-collect dependencies.
     */
    get() {
        // pushTarget(this)
        popTarget();
        let value;
        const vm = this.vm;
        try {
            if (!this.value) {
                if (!this.getter) {
                    this.getter = _u.parsePathFromArray(this.express);
                }
                value = this.getter.call(vm, vm);
                if (Array.isArray(value)) {
                    this.oldArrayLength = value.length;
                }
                this.value = value;
            }
        } catch (e) {

        } finally {
            // "touch" every property so they are all tracked as
            // dependencies for deep watching
            if (this.deep) {
                _u.traverse(value)
            }
            this.cleanupDeps()
        }
        
        return value
    }

    set(value) {
        this.value = value;
    }

    popTarget() {
        popTarget();
    }

    /**
     * Add a dependency to this directive.
     */
    addDep(dep: Dep) {
        const id = dep.id
        // console.warn("addDep", dep.id);
        if (!this.newDepIds.has(id)) {
            this.newDepIds.add(id)
            this.newDeps.push(dep)
            if (!this.depIds.has(id)) {
                dep.addSub(this)
            }
        }
    }

    /**
     * Clean up for dependency collection.
     */
    cleanupDeps() {

        let i = this.deps.length
        while (i--) {
            const dep = this.deps[i]
            if (!this.newDepIds.has(dep.id)) {
                dep.removeSub(this)
            }
        }
        let tmp: any = this.depIds;
        this.depIds = this.newDepIds
        this.newDepIds = tmp
        this.newDepIds.clear()
        tmp = this.deps;
        this.deps = this.newDeps
        this.newDeps = tmp
        this.newDeps.length = 0

    }

    /**
     * Subscriber interface.
     * Will be called when a dependency changes.
     */
    update(type,extData) {
        this.run(type,extData)
    }

    /**
     * Scheduler job interface.
     * Will be called by the scheduler.
     */
    run(type,extData) {
        const oldValue = this.value;
        let arrayUpdate = false;
        const value = this.getter.call(this.vm, this.vm);
        if (Array.isArray(value)) {
            if (value.length !== this.oldArrayLength) {
                arrayUpdate = true;
                this.oldArrayLength = value.length;
            }
        }
        if ((arrayUpdate || value !== oldValue) && this.target) {

            try {
                this.value = value;
                this.cb.call(this.target, value, oldValue,this.vm);
            } catch (e) {
            }

        }
    }

    /**
     * Evaluate the value of the watcher.
     * This only gets called for lazy watchers.
     */
    // evaluate() {
    //     this.value = this.get()
    //     this.dirty = false
    // }

    /**
     * Depend on all deps collected by this watcher.
     */
    depend() {
        let i = this.deps.length
        while (i--) {
            this.deps[i].depend()
        }
    }

    /**
     * Remove self from all dependencies' subscriber list.
     */
    teardown() {
        if (this.active) {
            // remove self from vm's watcher list
            // this is a somewhat expensive operation so we skip it
            // if the vm is being destroyed.

            let i = this.deps.length
            while (i--) {
                this.deps[i].removeSub(this)
            }
        }
    }

    bind(value){
        return this;
    }
}