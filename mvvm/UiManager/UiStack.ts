import {UIBase} from "./UIBase";
export default abstract class UiStack {
    abstract pop(extData ?: any) : void;
    abstract push(ui : UIBase<any>,extData ?: any,callback ?: (isSuccess : boolean)=>void): void;
    abstract remove(ui : UIBase<any>,extData ?: any): void;
    abstract replace(ui : UIBase<any>,extData ?: any,callback ?: (isSuccess : boolean)=>void): void;
    abstract getTop(): void;
}