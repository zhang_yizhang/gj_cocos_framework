import {UiManager, UiType} from "./UiManager";
import LoadItem, {LoadDelegate, LoadDelegateTool, LoadState} from "../LoadManager/LoadItem";
import {LoadManager} from "../LoadManager/LoadManager";
import SingletonPatternBase from "../SingletonPatternBase";
import * as cc from "cc";
import config, {Platform} from "../../../../config/config";

export interface UIInterface {
    open(extData ?: any) : void,
    close() : void,
}
export class UiLoadConfig<T extends LoadItem> {//加载配置
    loadItem : T
    priority : number
    loadId ?: number
    constructor(loadItem : T,priority : number) {
        this.loadItem = loadItem;
        this.priority = priority;
    }
}
export abstract class UIBase<T extends LoadItem> extends SingletonPatternBase implements UIInterface{
    public uiType : UiType = UiType.FIRST_LEVEL_UI //ui类型
    dependLoadItem ?: UiLoadConfig<T>//依赖加载项
    dependLoadItemsEx : UiLoadConfig<T>[] = []//依赖加载项扩展
    tempDependLoadItemsEx : UiLoadConfig<T>[] = []//临时依赖加载项扩展
    preLoadItemList ?: UiLoadConfig<T>[]//预加载列表项
    loadDelegateList : LoadDelegate[] = [];//加载委托列表
    tempLoadDelegateList : LoadDelegate[] = [];//临时加载委托列表
    static LOAD_TIME_OUT = 15;//加载超时
    protected isInShow : boolean = false//是否显示中
    protected loadCbId : number = -1//加载回调id
    protected blockId : number = -1//屏蔽层id
    protected extData : any//打开数据
    protected isShowBlockUi : boolean = true;
    protected _inited : boolean = false;

    /**
     * 加载资源
     */
    async loadUiRes(extData ?: any){
        UiManager.getInstance().closeBlock(this.blockId);
        this.extData = extData;
        if(!this.dependLoadItem){
            await this.onLoadStateProcess(LoadState.SUCCESS);
            return true;
        }
        return await new Promise(resolve => {
            this.blockId = UiManager.getInstance().showBlock({isNoShowBlock : !this.isShowBlockUi,timeOut : UIBase.LOAD_TIME_OUT});//打开屏蔽层
            let list = [this.dependLoadItem];
            list.push(...this.dependLoadItemsEx);
            list.push(...this.tempDependLoadItemsEx);
            this.tempDependLoadItemsEx = [];
            let num = list.length;
            let cb = async (state : LoadState)=>{//添加加载完成回调
                if(num == - 1){
                    return;
                }
                if(state !== LoadState.SUCCESS){
                    num = -1;
                    cc.error("资源加载错误:",this.dependLoadItem!.loadItem);
                    await this.onLoadStateProcess(LoadState.FAIL);
                    resolve(false);
                    return;
                }
                if(--num > 0){
                    return;
                }
                UiManager.getInstance().closeBlock(this.blockId);
                await this.onLoadStateProcess(LoadState.SUCCESS);
                resolve(true);
            };
            let sumProgress = 0;
            let sumLoad = num;
            list.forEach(value => {
                let curProgress = 0;
                value!.loadItem.addLoadDelegate(LoadDelegateTool.getInstance((lostState)=>{
                    cb(lostState);
                },(progress)=>{
                    let pre = sumProgress;
                    sumProgress += (progress - curProgress) / sumLoad;
                    curProgress = progress;
                    this.onLoadProgress(sumProgress,pre);
                }))//添加到加载队列
                LoadManager.getInstance().push_front(value!.loadItem,value!.priority);
            })
        });
    }

    /**
     * 添加加载委托
     * @param loadDelegate
     */
    addLoadDelegate(loadDelegate : LoadDelegate){
        this.loadDelegateList.push(loadDelegate);
    }

    /**
     * 清除加载委托
     */
    clearLoadDelegate(){
        this.loadDelegateList.length = 0;
    }


    /**
     * 加载进度
     * @param progress
     * @param preProgress
     */
    protected onLoadProgress(progress : number,preProgress : number){
        let list = [...this.loadDelegateList,...this.tempLoadDelegateList];
        for(let i = 0;i < list.length;++i){
            list[i].setProgress(progress,preProgress);
        }
    }

    /**
     * 加载状态处理
     * @param loadState
     */
    async onLoadStateProcess(loadState : LoadState){
        let list = [...this.loadDelegateList,...this.tempLoadDelegateList];
        for(let i = 0;i < list.length;++i){
            await new Promise(resolve => {
                list[i].process!(this,()=>{
                    resolve();
                })
            });
        }
        for(let i = 0;i < list.length;++i){
            list[i].setState(loadState);
        }
    }


    /**
     * 开始预加载资源
     */
    protected startPreLoadItem(){
        if(!this.preLoadItemList || config.platform == Platform.androids || config.platform == Platform.ios){
            this.preLoadItemList = undefined;
            return;
        }
        for(let i = 0;i < this.preLoadItemList.length;++i){//预加载资源
            let item = this.preLoadItemList[i];
            item.loadId = LoadManager.getInstance().push_front(item.loadItem,item.priority);
        }
    }

    /**
     * 打开界面
     * @param extData
     */
    async open(extData : any){
        if(!this._inited){
            this._inited = true;
            this.init()
        }
        let list = [...this.loadDelegateList,...this.tempLoadDelegateList];
        this.tempLoadDelegateList = [];
        this.isInShow = true;
        await this.show(extData);
        for(let i = 0;i < list.length;++i){
            list[i].complete();
        }
        this.onShow();
        this.startPreLoadItem();
    }

    /**
     * 初始化
     */
    init(){

    }

    /**
     * 显示
     * @param extData
     */
    protected async show(extData : any){
    }

    /**
     * 显示之后触发
     */
    protected onShow(){

    }

    /**
     * 栈顶显示
     */
    onStackShow(){

    }

    /**
     * 非栈顶隐藏
     */
    onStackHide(){

    }

    close(){
        if(!this.isInShow){
            return;
        }
        this.isInShow = false;
        this.hide();
        this.onHide();
    }

    /**
     * 隐藏
     */
    protected hide(){
        UiManager.getInstance().closeBlock(this.blockId);//移除屏蔽层
        if(this.dependLoadItem){
            this.dependLoadItem.loadItem.removeLoadDelegate(this.loadCbId)//移除加载回调
            LoadManager.getInstance().remove(this.dependLoadItem.loadItem,this.dependLoadItem.loadId);//移除加载
        }
        if(this.preLoadItemList){
            for(let i = 0;i < this.preLoadItemList.length;++i){//移除预加载
                let item = this.preLoadItemList[i];
                LoadManager.getInstance().remove(item.loadItem,item.loadId);
            }
        }
    }
    /**
     * 隐藏之后触发
     */
    protected onHide(){

    }

    isShow(){
        return this.isInShow;
    }
}