/** UI管理单例
 @class UiManager
 @author YI ZHANG
 @date 2020/7/9
 @desc
 **/
import {UIBase} from "./UIBase";
import SingletonPatternBase from "../SingletonPatternBase";
import UiStack from "./UiStack";
export enum UiShowType {
    REPLACE,
    PUSH,
}
export enum UiType {//ui类型
    FIRST_LEVEL_UI,
    SECOND_LEVEL_UI,
    TOP_UI,
    FIRST_POP_UPS,
    SECOND_POP_UPS,
    EFFECT_UI,
    GUIDE_UI,
    TOP_POP_UPS,//置顶弹窗
    TIP_UI,
    BLOCK_UI,
}
export class UiManager extends SingletonPatternBase{
    private uiStackMap : {[key : number] : UiStack} = {}
    _blockUI !: {open:(data : {isNoShowBlock ?: boolean,msg ?: string})=>void,close:()=>void,isShow : boolean} ;
    private blockId : number = 1;
    private blockMap : {[key : number] : {time : number,isShow : boolean}} = {};
    private checkBlockTime = 0.5;
    private curBlockTime = 0;
    private blockTimeOut = 6;
    private showBlockNum: number = 0;

    getTopUI(){

    }

    /**
     * 设置uiStack
     * @param uiType
     * @param uiStack
     */
    setUiStack(uiType : UiType,uiStack : UiStack){
        this.uiStackMap[uiType] = uiStack;
    }

    /**
     * 获取ui栈
     * @param uiType
     */
    getUiStack(uiType : UiType){
        return this.uiStackMap[uiType];
    }

    /**
     * 打开ui
     * @param ui
     * @param showType
     * @param extData
     * @param callback
     */
    openUI(ui : UIBase<any>,showType : UiShowType,extData ?: any,callback ?: (isSuccess : boolean)=>void){
        let uiStack = this.uiStackMap[ui.uiType];
        if(!uiStack){
            return;
        }
        switch (showType) {
            case UiShowType.PUSH:
                uiStack.push(ui,extData,callback);
                break;
            case UiShowType.REPLACE:
                uiStack.replace(ui,extData,callback);
                break
        }
    }


    /**
     * 关闭ui
     * @param ui
     * @param extData
     */
    closeUI(ui : UIBase<any>,extData : any){
        let uiStack = this.uiStackMap[ui.uiType];
        if(!uiStack){
            return;
        }
        uiStack.remove(ui,extData);
    }

    /**
     * 显示屏蔽层
     */
    showBlock(extData : {isNoShowBlock ?: boolean,msg ?: string,timeOut ?: number} = {}){
        let keys = Object.keys(this.blockMap);
        if(keys.length == 0){
            this._blockUI.open(extData);
        }
        if(!extData.isNoShowBlock){
            this.showBlockNum ++;
        }
        this.blockMap[this.blockId] = {time : extData.timeOut || this.blockTimeOut,isShow : !extData.isNoShowBlock};
        return this.blockId++;
    }

    /**
     * 隐藏屏蔽层
     * @param id 屏蔽id
     */
    closeBlock(id : number){
        if(!id){
            return;
        }
        let data = this.blockMap[id];
        delete this.blockMap[id];
        if(!this._blockUI.isShow){
            return;
        }
        let keys = Object.keys(this.blockMap);
        if(keys.length == 0){
            this.showBlockNum = 0;
            this._blockUI.close();
        }else {
            if(data && data.isShow){
                this.showBlockNum--;
                if(this.showBlockNum <= 0) {
                    this._blockUI.open({isNoShowBlock: true})
                }

            }
        }
    }

    /**
     * 设置屏蔽层
     * @param uiBase
     */
    setBlock(uiBase : {open:(data : {isNoShowBlock ?: boolean,msg ?: string})=>void,close:()=>void,isShow : boolean}){
        this._blockUI = uiBase;
    }

    /**
     * 关闭所有屏蔽层
     */
    closeAllBlock(){
        this.blockMap = {};
        this._blockUI.close();
    }

    /**
     * 检测屏蔽层超时的关闭
     * @param dt
     */
    checkBlock(dt : number){
        this.curBlockTime += dt;
        if(this.curBlockTime < this.checkBlockTime){
            return;
        }
        this.curBlockTime = 0;
        let keys = Object.keys(this.blockMap);
        for(let i = 0;i < keys.length;++i){
            let key : number = <any>keys[i];
            this.blockMap[key].time -= this.checkBlockTime;
            if(this.blockMap[key].time <= 0){
                this.closeBlock(key)
            }
        }
    }

    update(dt : number){
        this.checkBlock(dt);
    }
}