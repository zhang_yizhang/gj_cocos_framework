/** 加载管理单例
@class LoadManager
@author YI ZHANG
@date 2020/7/9
@desc
**/

import SingletonPatternBase from "../SingletonPatternBase";
import LoadItem, {LoadDelegateTool, LoadState} from "./LoadItem";
let loadId = 1;
export enum LoadPriority {
    UI = 1,//UI
    DependLoadUI = 10,//强依赖加载UI
    PreLoadUI = 20,//预加载UI
    preLoadRes1 = 30,//预加载资源
    preLoadRes2 = 40,//预加载资源
    preLoadRes3 = 50,//预加载资源
    preLoadAudio4 = 60,//预加载音频
}

export class LoadManager extends SingletonPatternBase{
    protected loadDequeList : LoadItem[][] = [];//加载队列列表
    protected loadDequeMap : {[key : number] : LoadItem[]} = {};//加载队列map
    protected inLoadDequeList : LoadItem[] = [];//加载中列表
    protected allLoadItemMap : {[key : number] : {loadItem : LoadItem,priority : number,ids : number[]}} = {};//加载ItemMap
    downCount : number = 5;//下载线程数
    private waitDownCount = 0;//等待下载数

    /**
     * 添加一个下载到队列头
     * @param loadItem
     * @param priority
     */
    push_front(loadItem : LoadItem,priority : number) : number{
        return this.push(loadItem,false,priority)
    }
    /**
     * 添加一个下载到队列尾
     * @param loadItem
     * @param priority
     */
    push_back(loadItem : LoadItem,priority : number) : number{
        return this.push(loadItem,true,priority)
    }

    /**
     * 移除下载项
     * @param loadItem
     * @param id
     */
    remove(loadItem : LoadItem,id ?: number){
        let data = this.allLoadItemMap[loadItem.id]
        if(!data){
            return;
        }
        if(id && data.ids.length > 0){
            let index = data.ids.indexOf(id);
            index != -1 && data.ids.splice(index,1);
            if(data.ids.length > 0){
                return;
            }
        }
        let deque = this.inLoadDequeList;
        for(let i = 0;i < deque.length;++i){
            if(loadItem === deque[i]){
                delete this.allLoadItemMap[loadItem.id];
                deque.splice(i,1);
                this.waitDownCount--;
                return;
            }
        }
        deque = this.getLoadDequeByPriority(data.priority);
        for(let i = 0;i < deque.length;++i){
            if(loadItem === deque[i]){
                delete this.allLoadItemMap[loadItem.id];
                deque.splice(i,1);
                this.waitDownCount--;
                return;
            }
        }
    }

    /**
     * 添加一个下载
     * @param loadItem
     * @param isBack
     * @param priority
     */
    private push(loadItem : LoadItem,isBack : boolean,priority : number) : number{
        if(this.allLoadItemMap[loadItem.id]){
            this.allLoadItemMap[loadItem.id].ids.push(loadId);
            if(priority < this.allLoadItemMap[loadItem.id].priority){//优先级更高更改优先级
                let deque = this.getLoadDequeByPriority(priority);
                for(let i = 0;i < deque.length;++i){
                    if(this.allLoadItemMap[loadItem.id].loadItem === deque[i]){
                        deque.splice(i,1);
                        break
                    }
                }
            }else {
                return loadId++;
            }
        }else {
            this.allLoadItemMap[loadItem.id] = {loadItem : loadItem,priority : priority,ids : [loadId]};
            this.waitDownCount++;
        }
        let deque = this.getLoadDequeByPriority(priority);
        isBack && deque.push(loadItem) || deque.unshift(loadItem);
        this.checkDown();
        if(priority === LoadPriority.UI && !isBack){
            this.loadItem();
        }
        return loadId++;
    }

    /**
     * 获取相关优先级加载队列
     * @param priority 优先级
     */
    getLoadDequeByPriority (priority : number){
        let deque = this.loadDequeMap[priority];
        if(deque){
            return deque;
        }
        let prePriorityDeque;
        let maxPriority = -1;
        let keys = Object.keys(this.loadDequeMap);
        for(let i = 0;i < keys.length;++i){
            let temp = Number(keys[i])
            if(temp > maxPriority && temp < priority){
                maxPriority = temp;
                prePriorityDeque = this.loadDequeMap[temp];
            }
        }
        deque = [];
        this.loadDequeMap[priority] = deque;
        if(!prePriorityDeque){
            this.loadDequeList.push(deque)
            return deque;
        }
        for(let i = 0;i < this.loadDequeList.length;++i){
            if(this.loadDequeList[i] == prePriorityDeque){
                this.loadDequeList.splice(i + 1,0,deque);
                break;
            }
        }
        return deque;
    }



    /**
     * 检查是否可以开始下载
     */
    checkDown(){
        if(this.waitDownCount === 0 || this.downCount <= this.inLoadDequeList.length){
            return;
        }
        this.loadItem();
    }

    /**
     * 加载一个资源
     */
    loadItem(){
        for(let i = 0;i < this.loadDequeList.length;++i){
            let list = this.loadDequeList[i];
            if(list.length > 0){
                this.startDownload(list.shift()!);
                break;
            }
        }
    }

    /**
     * 开始下载
     * @param loadItem
     */
    startDownload(loadItem : LoadItem){
        this.inLoadDequeList.push(loadItem);
        loadItem.addLoadDelegate(LoadDelegateTool.getInstance((state : LoadState)=>{
            if(!this.allLoadItemMap[loadItem.id]){
                return;
            }
            this.remove(loadItem);
            let index = this.inLoadDequeList.indexOf(loadItem);
            if(index != -1){
                this.inLoadDequeList.splice(index,1);
            }
        }));
        loadItem.load()
    }

    update(dt : number){
        this.checkDown();
    }

}
