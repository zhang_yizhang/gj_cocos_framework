/** 加载项
@class LoadItem
@author YI ZHANG
@date 2020/7/9
@desc
**/
export enum LoadState {
    SUCCESS,
    FAIL
}
let loadId = 0;
let loadCbId = 0;

export interface LoadDelegate {//进度委托
    setProgress : (progress : number,preProgress : number)=>void,
    setState : (lostState : LoadState)=>void
    process : (asset : any,finish : ()=>void)=>void;
    complete : ()=>void;
    fail : ()=>void;
}
interface LoadDelegate2 {//进度委托
    setProgress ?: (progress : number,preProgress : number)=>void,
    setState ?: (lostState : LoadState)=>void
    process ?: (asset : any,finish : ()=>void)=>void;
    complete ?: ()=>void;
    fail ?: ()=>void;
}
let nullCb = ()=>{};
let defaultProcessCb = (asset : any,finish : ()=>void)=>{finish()};
export class LoadDelegateTool{
    static getInstance(setState ?: (lostState : LoadState)=>void,setProgress ?: (progress : number,preProgress : number)=>void, process ?: (asset : any,finish : ()=>void)=>void):LoadDelegate{
        return  {
            setState : setState || nullCb,
            setProgress : setProgress || nullCb,
            process : process || defaultProcessCb,
            complete : nullCb,
            fail : nullCb,
        }
    }

    static getInstanceByData(data : LoadDelegate2){
        return  {
            setState : data.setState || nullCb,
            setProgress : data.setProgress || nullCb,
            process : data.process || defaultProcessCb,
            complete : data.complete || nullCb,
            fail : data.fail || nullCb,
        }
    }

}
export default class LoadItem{
    readonly id : number//唯一标识
    private loadDelegate : Map<number,LoadDelegate> = new Map<number, LoadDelegate>();
    private curProgress = 0;

    //加载委托map
    protected constructor() {
        this.id = loadId++;
    }

    /**
     * 添加加载委托
     * @param loadDelegate 加载委托
     */
    addLoadDelegate(loadDelegate : LoadDelegate){
        this.loadDelegate.set(loadCbId,loadDelegate);
        return loadCbId++;
    }

    /**
     * 删除加载回调
     * @param id 回调id
     */
    removeLoadDelegate(id : number){
        this.loadDelegate.delete(id);
    }

    /**
     * 加载状态处理
     * @param loadState
     */
    async loadStateProcess(loadState : LoadState){
        let list: LoadDelegate[] = []
        this.loadDelegate.forEach((value,key)=>{
           list.push(value)
            // value.setProgress(progress,preProgress)
        })
        for(let i=0 ;i<list.length;i++){
            await new Promise(resolve => {
                list[i].process!(this,()=>{
                    resolve(null);
                })
            });
        }
        for(let i = 0;i < list.length;++i){
            list[i].setState(loadState);
            list[i].complete();
        }
        this.loadDelegate.clear();
    }

    /**
     * 加载进度处理
     * @param progress
     */
    progressProcess(progress : number){
        let preProgress = this.curProgress;
        this.curProgress = Math.max(progress,this.curProgress);
        this.loadDelegate.forEach((value,key)=>{
            value.setProgress(progress,preProgress)
        })
    }

    load() {
        this.curProgress = 0;
    }
}