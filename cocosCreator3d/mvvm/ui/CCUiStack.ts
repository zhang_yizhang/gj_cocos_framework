/** creator ui栈实现
@class CCUiStack
@author YI ZHANG
@date 2020/7/10
@desc
**/

import UiStack from "../../../mvvm/UiManager/UiStack";
import CCUiBase from "./CCUiBase";
import {UiManager, UiType} from "../../../mvvm/UiManager/UiManager";
import * as cc from "cc";
import {CCBoolean} from "cc";

const {ccclass, property} = cc._decorator;
let uiType = cc.Enum(UiType);
@ccclass
export default class CCUiStack extends cc.Component implements UiStack{
    private stack : CCUiBase[] = [];
    @property({type : uiType})
    uiType : UiType = uiType.FIRST_LEVEL_UI;
    @property(CCBoolean)
    isAutoHide : boolean = true;//是否自动隐藏低层级
    @property(CCBoolean)
    isTopUpdateNotice : boolean = true;//顶部ui更新是否通知
    static updateTopList : CCUiStack[] = [];
    static topData ?: {ui : CCUiBase,stack : CCUiStack};
    private static updateTopCb: ((ui: CCUiBase,oldUi ?: CCUiBase) => void)[] = [];


    protected onLoad() {
        UiManager.getInstance().setUiStack(this.uiType,this);
        if(this.isTopUpdateNotice){
            CCUiStack.updateTopList.push(this);
            CCUiStack.updateTopList.sort((a,b)=>{
                return b.uiType - a.uiType;
            })
        }
    }

    pop(extData?: any) {
        let topUi = this.stack[this.stack.length - 1];
        if(!topUi){
            return;
        }
        this.remove(topUi);
    }

    async push(ui: CCUiBase, extData?: any,callback ?:(isSuccess : boolean)=>void) {
        let isSuccess = await ui.loadUiRes(extData);
        if(!isSuccess){
            callback && callback(false);
        }
        this._remove(ui);
        this._push(ui,extData,callback);
    }

    _push(ui: CCUiBase,extData?: any,callback ?:(isSuccess : boolean)=>void){
        this._remove(ui);
        if(this.stack.length && this.isAutoHide && this.stack[this.stack.length - 1].isShow()){
            this.stack[this.stack.length - 1].onStackHide();
        }
        this.stack.push(ui);
        if(this.isTopUpdateNotice){
            CCUiStack.updateTopUI();
        }
        this.node.addChild(ui.root);
        ui.open(extData);
        callback && callback(true);
    }

    async replace(ui:CCUiBase,extData?:any,callback ?:(isSuccess : boolean)=>void){
        let isSuccess = await ui.loadUiRes(extData);
        if(!isSuccess){
            callback && callback(false);
            return ;
        }
        let topUi = this.stack[this.stack.length - 1];
        if(topUi){
            this._remove(topUi);
        }
        this._push(ui,extData,callback);
    }

    _remove(ui: CCUiBase, extData?: any) {
        for(let i = 0;i < this.stack.length;++i){
            if(this.stack[i] === ui){
                this.stack.splice(i,1);
                if(cc.isValid(ui.root)){
                    ui.root.removeFromParent();
                }
                ui.close();
                if(this.stack.length && this.isAutoHide && this.stack[this.stack.length - 1].isShow()){
                    this.stack[this.stack.length - 1].onStackShow();
                }
                return true;
            }
        }
        return false;
    }

    remove(ui: CCUiBase, extData?: any) {
        if(this._remove(ui)){
            CCUiStack.updateTopUI();
        }
    }

    getTop() {
        return this.stack[this.stack.length - 1];
    }

    static updateTopUI(){
        for(let i = 0;i < this.updateTopList.length;++i){
            let stack1 = this.updateTopList[i];
            let stack = stack1.stack;
            let top = this.getTopUiByStack(stack);
            if(!this.topData){
                this.topData = {stack : stack1,ui : top};
            }
            if(top && top !== this.topData.ui){
                let oldUi = this.topData && this.topData.ui;
                this.topData = {stack : stack1,ui : top};
                for(let j = 0;j < this.updateTopCb.length;++j){
                    this.updateTopCb[j](top,oldUi);
                }
                return;
            }
        }
    }

    static getTopUiByStack(stack : CCUiBase[]){
        return stack[stack.length - 1] && (stack[stack.length - 1]);
    }


    static setUpdateTopCb(cb : (ui : CCUiBase,oldUi ?: CCUiBase)=>void){
        this.updateTopCb.push(cb);
    }

    // update (dt) {}
}
