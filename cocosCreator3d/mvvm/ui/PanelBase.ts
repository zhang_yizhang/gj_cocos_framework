/** 视图父类
@class PanelBase
@author YI ZHANG
@date 2020/7/10
@desc
**/

import ViewBase from "../../../mvvm/MvvmUtils/ViewBase";
import CCUiBase from "./CCUiBase";
import * as cc from "cc";
import {_decorator} from "cc";
import ViewManager from "../../../mvvm/MvvmUtils/ViewManager";
import Vue from "../../../mvvm/ccc-vue/Vue";
import {ChangeType} from "../../../mvvm/ccc-vue/VueNotifyType";
import Watcher from "../../../mvvm/ccc-vue/Watcher";
let nullCb = (value : any)=>{};
@_decorator.ccclass
export default class CCPanelBase extends cc.Component{
    public viewType : number = -1;
    public uiBase ?: CCUiBase;
    watchManager: ViewBase;
    isFirst : boolean = true;
    protected isBlockTopUI = false;
    static blockTopUiCount = 0;
    constructor(...args : any){
        super(...args);
        this.watchManager = new ViewBase();
    }

    async initPanelBase(data : any){

    }

    protected onEnable(): void {
        this.watchManager.enableWatch();
    }


    protected onDisable(): void {
        this.watchManager.disableWatch();
    }

    protected onDestroy(): void {
        this.watchManager.removeAllWatch();
    }

    /**
     * 添加监听
     * @param vm
     * @param cb
     * @param target
     */
    watch(vm: Vue){
        let watch = Watcher.createWatch(vm,undefined,this);
        return <T>(value : T,cb :  (value : T,oldValue : T,target : any,changeType : ChangeType,changeData : any) => void,isNoTrigger ?: boolean)=>{
            watch.setCallback(cb,this);
            this.watchManager.addWatcher(watch,value,isNoTrigger);
        }
    }

    /**
     * 显示
     * @param extData
     */
    async show(extData : any){

    }
    /**
     * 显示之后触发
     */
    onShow(){

    }


    /**
     * 隐藏
     */
    hide(){
        this.watchManager.removeAllWatch();
    }

    /**
     * 隐藏之后触发
     */
    onHide(){

    }

    /**
     * 栈顶显示
     */
    onStackShow(){

    }

    /**
     * 非栈顶隐藏
     */
    onStackHide(){

    }


    close(){
        ViewManager.closeUi(this.viewType);
    }

    btnClose(){
        this.close();
    }

}