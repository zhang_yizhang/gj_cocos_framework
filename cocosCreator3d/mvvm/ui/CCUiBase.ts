/** creator Ui基类
@class CCUiBase
@author YI ZHANG
@date 2020/7/10
@desc
**/
import * as cc from "cc";
import {UIBase} from "../../../mvvm/UiManager/UIBase";
import CCLoadItem from "./CCLoadItem";
import {UiType} from "../../../mvvm/UiManager/UiManager";
import CCPanelBase from "./PanelBase";
import ViewManager, {ViewData, ViewLoadData} from "../../../mvvm/MvvmUtils/ViewManager";
import GlobalWidget from "../../utils/GlobalWidget";
export default class CCUiBase extends UIBase<CCLoadItem<cc.Asset>>{

    protected panel ?: cc.Node//展示节点
    readonly root : cc.Node;//展示父节点 主要为了方便UI栈管理
    public viewType : number//视图名称
    private _maskNode ?: cc.Node;
    maskOpacity: number = 180;

    //是否处于打开动画
    public getPanel(){//获取展示节点
        return this.panel
    }

    constructor(viewData : ViewData,viewType : number,uiType ?: UiType,preLoadList ?: ViewLoadData[]) {
        super();
        this.uiType = uiType || viewData.uiType;
        this.viewType = viewType;
        this.isShowBlockUi = !viewData.isNoShowBlock;
        this.dependLoadItem = {//初始化依赖加载预制体
            loadItem: new CCLoadItem<cc.Prefab>(viewData.dependLoadItem.loadItem),
            priority: viewData.dependLoadItem.priority
        };
        this.preLoadItemList = this.preLoadItemList || [];
        if(viewData.preLoadUI){//预下载ui
            for(let i = 0;i < viewData.preLoadUI.length;++i){
                let data = ViewManager.ViewPanelMap[viewData.preLoadUI[i].viewType];
                this.preLoadItemList.push({
                    loadItem: new CCLoadItem<cc.Prefab>(data.dependLoadItem.loadItem,true),
                    priority: viewData.preLoadUI[i].loadPriority,
                })
            }
        }
        if(viewData.preLoadItemList){//预下载ui
            for(let i = 0;i < viewData.preLoadItemList.length;++i){
                let data = viewData.preLoadItemList[i];
                this.preLoadItemList.push({
                    loadItem: new CCLoadItem<cc.Prefab>(data.loadItem,true),
                    priority: data.priority,
                })
            }
        }


        this.root = this._createFullNode();//创建父节点

        if((!viewData.maskData || viewData.maskData.isBlock)){

        }
        if((!viewData.maskData || viewData.maskData.isMask)){
            this.maskOpacity = viewData.maskData && viewData.maskData.maskOpacity || 180;
            let node = this._createFullNode();
            let widget : cc.Widget = node.getComponent(cc.Widget)!;
            node.addComponent(GlobalWidget);
            if((!viewData.maskData || viewData.maskData.isBlock)){
                node.addComponent(cc.BlockInputEvents);
            }
            widget.bottom = -1000;
            widget.top = -1000;
            widget.left = -1000;
            widget.right = -1000;
            node.getComponent(cc.UIOpacity)!.opacity = this.maskOpacity;
            let sprite = node.addComponent(cc.Sprite);
            sprite.spriteFrame = ViewManager.getBlockSprite();
            sprite.color = viewData.maskData && viewData.maskData.color || cc.Color.BLACK;
            this.root.addChild(node);
            this._maskNode = node;
        }
        if(preLoadList){
            this.preLoadItemList = [];
            for(let i = 0;i < preLoadList.length;++i){//初始化预加载资源
                let data = preLoadList[i];
                this.preLoadItemList.push({
                    loadItem: new CCLoadItem<cc.Prefab>(data.loadItem),
                    priority: data.priority
                })
            }
        }
    }



    /**
     * 创建填充满父节点的节点
     * @private
     */
    private _createFullNode(layer ?: number){
        let node = new cc.Node();//创建父节点
        let widget : cc.Widget = node.addComponent(cc.Widget);
        widget.alignMode = cc.Widget.AlignMode.ON_WINDOW_RESIZE;
        widget.isAlignBottom = true;
        widget.bottom = 0;
        widget.isAlignTop = true;
        widget.top = 0;
        widget.isAlignLeft = true;
        widget.left = 0;
        widget.isAlignRight = true;
        widget.right = 0;
        node.addComponent(cc.UIOpacity);
        return node
    }

    init() {
        super.init();
        if(!this.dependLoadItem){
            return;
        }
        
        let node = cc.instantiate(this.dependLoadItem.loadItem.asset as cc.Prefab);
        this.panel = node;
        this.root.addChild(node);
        let panelBase = this.getPanelBase();
        if(panelBase){
            panelBase.uiBase = this;
            panelBase.viewType = this.viewType;
        }
    }

    preInit(){
        if(!cc.isValid(this.panel)){

            console.log(this.dependLoadItem.loadItem,'preInit')
            this.init();
        }
    }

    /**
     * 显示
     * @param extData
     */
    async show(extData?: any) {
        await super.show(extData);
        this.root.active = true;
        if(!cc.isValid(this.panel)){
            this.init();
        }
        let panelBase = this.getPanelBase();
        if(panelBase){
            await panelBase.show(extData);
        }
    }

    protected onShow() {
        super.onShow();
        let panelBase = this.getPanelBase();
        if(panelBase){
            panelBase.onShow();
        }
    }

    /**
     * 隐藏
     * @param extData
     */
    hide(extData?: any) {
        super.hide();
        let panelBase = this.getPanelBase();
        if(panelBase){
            panelBase.hide();
        }
    }

    /**
     * 获取ui类
     */
    getPanelBase(){
        if(!cc.isValid(this.panel)){
            return;
        }
        return this.panel!.getComponent(CCPanelBase);
    }




    protected onHide() {
        super.onHide();
        let panelBase = this.getPanelBase();
        if(panelBase){
            panelBase.onHide();
        }
    }

    onStackShow() {
        super.onStackShow();
        this.root.active = true;
        let panelBase = this.getPanelBase();
        if(panelBase){
            panelBase.onStackShow();
        }
        this.onShow();
    }

    onStackHide() {
        super.onStackHide();
        this.root.active = false;
        let panelBase = this.getPanelBase();
        if(panelBase){
            panelBase.onStackHide();
        }
        this.onHide();
    }


    /**
     * 切换屏蔽层开关
     * @param isBlock
     */
    toggleBlock(isBlock : boolean){
        if(this._maskNode){
            this._maskNode.active = isBlock;
        }else {
            let comp = this.root.getComponent(cc.BlockInputEvents);
            if(comp){
                comp.enabled = isBlock;
            }
        }
    }


    /**
     * 获取遮罩
     */
    getMaskNode(){
        return this._maskNode;
    }
}
