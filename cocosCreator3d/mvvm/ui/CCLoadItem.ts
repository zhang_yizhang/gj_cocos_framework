/** creator加载item
@class CCLoadItem
@author YI ZHANG
@date 2020/7/10
@desc
**/
import LoadItem, { LoadDelegateTool, LoadState} from "../../../mvvm/LoadManager/LoadItem";
import {LoadManager, LoadPriority} from "../../../mvvm/LoadManager/LoadManager";
import * as cc from "cc";
import EventManager from "../../../Event/EventManager";
import {EDITOR} from "cc/env";
let resMap : {[key : string] : cc.Asset | cc.Asset[]} = {};//资源缓存  todo 张一章 添加资源释放
let downLoadResMap : {[key : string] : boolean} = {};//已下载map
let bundleMap : {[key : string] : cc.AssetManager.Bundle} = {};//bundle缓存
export class DirAsset extends cc.Asset{

}
export let LoadItemEvent = {
    LOAD_FAIL : "LOAD_FAIL",
};
cc.assetManager.downloader.maxRequestsPerFrame = 3;
cc.assetManager.downloader.maxRetryCount = 10;
cc.assetManager.downloader.retryInterval = 700;
export default class CCLoadItem<T extends cc.Asset> extends LoadItem{
    readonly type : new()=>T//加载类型
    readonly resUrl : string//资源路径
    readonly extName ?: string//资源路径
    readonly bundleName !: string//bundle
    static bundleList: string[] = [];
    asset !: T | T[];//资源
    private readonly isDownLoad : boolean = false;//是否只是下载
    private readonly _loadKey : string = "";//加载唯一key

    static bundleHashMap : {[key : string] : string} = {};//bundleHash远程
    static bundlePathMap : {[key : string] : string} = {};
    private prePath ?: string;

    //bundleHash远程
    private isDir: boolean | undefined;
    constructor(data : {type : new()=>T,resUrl : string,extName ?: string,prePath ?: string},isDownLoad ?:boolean,isDir ?: boolean) {
        super();
        let index = data.resUrl.indexOf(":");
        let resUrl = data.resUrl.substr(index + 1,data.resUrl.length);
        let bundleName = index == -1 ? "resources" : data.resUrl.substr(0,index);
        this.type = data.type;
        this.resUrl = resUrl;
        this.prePath = data.prePath;
        this.bundleName = bundleName;
        this.extName = data.extName;
        this.isDir = isDir;
        this._loadKey = CCLoadItem.getKey(this.bundleName,this.resUrl,this.type.prototype.__classname__);
        this.isDownLoad = isDownLoad!;
    }

    static setBundleHash(bundle : string,hash : string,path: string){
        this.bundleHashMap[bundle] = hash;
        this.bundlePathMap[bundle] = path;
        
    }

    static async load<T extends cc.Asset>(resUrl : string,type : new()=> T , priority ?: LoadPriority,isDownLoad ?:boolean,prePath ?: string,extName ?: string){
        return await new Promise((resolve : (asset : T | undefined)=>void, reject) => {
            let item = new CCLoadItem({resUrl : resUrl,type : type,extName : extName,prePath : prePath},isDownLoad);
            item.addLoadDelegate(LoadDelegateTool.getInstance((loadState: LoadState)=>{
                resolve(item.asset as T);
            }));
            LoadManager.getInstance().push_front(item,priority || LoadPriority.UI)
        });
    }

    static async loadDir<T extends cc.Asset>(resUrl : string,type : new()=> T , priority ?: LoadPriority,isDownLoad ?:boolean,prePath ?: string,extName ?: string){
        return await new Promise((resolve : (asset : T[] | undefined)=>void, reject) => {
            let item = new CCLoadItem({resUrl : resUrl,type : type,extName : extName,prePath : prePath},isDownLoad,true);
            item.addLoadDelegate(LoadDelegateTool.getInstance((loadState: LoadState)=>{
                resolve(item.asset as T[]);
            }));
            LoadManager.getInstance().push_front(item,priority || LoadPriority.UI)
        });
    }

    static releaseDir<T extends cc.Asset>(resUrl : string,type : new()=> T){
        let index = resUrl.indexOf(":");
        let resUrl1 = resUrl.substr(index + 1,resUrl.length);
        let bundleName = index == -1 ? "resources" : resUrl.substr(0,index);
        let loadKey = CCLoadItem.getKey(bundleName,resUrl1,type.prototype.__classname__);
        let asset = resMap[loadKey] as T[];
        if(!asset){
            return;
        }
        for(let i = 0;i < asset.length;++i){
            if(cc.isValid(asset[i])){
                cc.assetManager.releaseAsset(asset[i]);
                asset[i].destroy();
            }
        }
        delete resMap[loadKey];
    }

    static getRes<T extends cc.Asset>(resUrl : string,type : new()=> T ) : T | null{
        let index = resUrl.indexOf(":");
        let resPath = resUrl.substr(index + 1,resUrl.length);
        let bundleName = index == -1 ? "resources" : resUrl.substr(0,index);
        let key = CCLoadItem.getKey(bundleName,resPath,type.prototype.__classname__);
        let asset =  resMap[key] as T;
        if(cc.isValid(asset)){
            return asset;
        }
        return null;
    }

    static getResByDir<T extends cc.Asset>(resUrl : string,type : new()=> T ) : T[]{
        let index = resUrl.indexOf(":");
        let resPath = resUrl.substr(index + 1,resUrl.length);
        let bundleName = index == -1 ? "resources" : resUrl.substr(0,index);
        let key = CCLoadItem.getKey(bundleName,resPath,type.prototype.__classname__);
        let asset =  resMap[key] as T[];
        if(cc.isValid(asset)){
            return asset;
        }
        return [];
    }

    static cacheRes(resUrl : string,loadCb : (err : any,asset : any)=>void,
                    progressCb?: (progress: number) => void,priority ?: LoadPriority){
        let item = new CCLoadItem({resUrl : resUrl,type : DirAsset});
        item.addLoadDelegate(LoadDelegateTool.getInstance((loadState: LoadState)=>{
            loadCb && loadCb(loadState == LoadState.FAIL ? "loadFail" : undefined,item.asset);
        },progressCb));
        LoadManager.getInstance().push_front(item,priority || LoadPriority.UI)
    }

    static cacheResDir(bundleName :string,resUrl : string,loadCb ?: (err : any,asset : any)=>void,
                    progressCb?: (progress: number) => void,priority ?: LoadPriority,isDownload  = false){
        let item = new CCLoadItem({resUrl : resUrl,type : DirAsset},isDownload);
        item.addLoadDelegate(LoadDelegateTool.getInstance((loadState: LoadState)=>{
            loadCb && loadCb(loadState == LoadState.FAIL ? "loadFail" : undefined,item.asset);
        },progressCb));
        LoadManager.getInstance().push_front(item,priority || LoadPriority.UI)
    }

    /**
     * 加载图片
     * @param resUrl
     */
    static async loadSpriteFrame(resUrl : string){
        return await this.load(resUrl,cc.SpriteFrame);
    }


    /**
     * 加载材质
     * @param resUrl
     */
    static async loadMaterial(resUrl : string){
        return await this.load(resUrl,cc.Material);
    }


    load() {
        super.load();
        if(EDITOR){
            if(!this.extName){
                this.loadStateProcess(LoadState.FAIL);
                console.error("加载编辑器资源要指定扩展名");
                return;
            }
            // @ts-ignore
            let info = EditorExtends.Asset.getAssetInfoFromUrl("db://assets/" + this.prePath + "/" + this.bundleName + "/" + this.resUrl + "." + this.extName);
            // @ts-ignore
            if(!info){
                // @ts-ignore
                info = EditorExtends.Asset.getAssetInfoFromUrl("db://assets/" + this.prePath + "/" + this.bundleName + "/" + this.resUrl.substr(0,this.resUrl.lastIndexOf("/")) + "." + this.extName);
            }
            if(!info){
                this.loadStateProcess(LoadState.FAIL);
                return;
            }
            let uuid = "";
            if(info.type.indexOf(this.type.prototype.__classname__) != -1){
                uuid = info.uuid;
            }else {
                let keys = Object.keys(info.subAssets);
                for(let i = 0;i < keys.length;++i){
                    let item = info.subAssets[keys[i]];
                    if(item.type.indexOf(this.type.prototype.__classname__) != -1){
                        uuid = item.uuid;
                        break;
                    }
                }
            }
            if(!uuid){
                this.loadStateProcess(LoadState.FAIL);
                return;
            }
            cc.AssetLibrary.loadAsset(uuid,(error,asset)=>{
                this.asset = asset;
                if(error){
                    this.loadStateProcess(LoadState.FAIL);
                }else {
                    this.loadStateProcess(LoadState.SUCCESS);
                }
            });
            return;;
        }
        if(this.isDownLoad && downLoadResMap[this._loadKey]){
            this.loadStateProcess(LoadState.SUCCESS);
            return;
        }
        let asset = resMap[this._loadKey];
        if(!this.isDownLoad && cc.isValid(asset)){
            this.asset = <T><any>asset;
            this.loadStateProcess(LoadState.SUCCESS);
            return;
        }
        if(cc.DebugMode){
            if(this.isDownLoad){
                cc.log("预下载:",this._loadKey);
            }else {
                cc.log("下载:",this._loadKey,downLoadResMap[this._loadKey] ? "   命中缓存" : "无缓存");
            }
        }
        let bundle = bundleMap[this.bundleName];
        if(bundle){
            this._loadItem(bundle);
            return;
        }
        (async ()=>{
            let index = CCLoadItem.bundleList.indexOf(this.bundleName);
            if(index !== -1){
                for(let i = 0;i < index;++i){
                    let bundle = bundleMap[CCLoadItem.bundleList[i]];
                    if(!bundle){
                        await new Promise(resolve => {
                            let bundleName = CCLoadItem.bundleList[i];
                            console.log("threee",CCLoadItem.bundlePathMap[bundleName],CCLoadItem.bundleHashMap[bundleName],bundleName,CCLoadItem.bundlePathMap,CCLoadItem.bundleHashMap);
                            cc.assetManager.loadBundle(CCLoadItem.bundlePathMap[bundleName] || bundleName ,{version: CCLoadItem.bundleHashMap[bundleName]},((err, bundle1) => {
                                resolve(null);
                            }))
                        })
                    }
                }
            }
            console.log("threee1",CCLoadItem.bundlePathMap[this.bundleName],CCLoadItem.bundleHashMap[this.bundleName],this.bundleName,CCLoadItem.bundleHashMap,CCLoadItem.bundlePathMap);
            cc.assetManager.loadBundle(CCLoadItem.bundlePathMap[this.bundleName] || this.bundleName, {version: CCLoadItem.bundleHashMap[this.bundleName]},(err, bundle : cc.AssetManager.Bundle)=> {
                if (err) {
                    cc.error(err);

                    this.loadStateProcess(LoadState.FAIL);
                    return;
                }
                bundleMap[this.bundleName] = bundle!;
                this._loadItem(bundle);
            });
        })()

    }


    private _loadItem(bundle : cc.AssetManager.Bundle){
        {//加载目录
            // @ts-ignore
            console.log("load " + this._loadKey);
            // @ts-ignore
            if (this.isDir) {
                let progress = 0;
                // @ts-ignore
                bundle[this.isDownLoad ? "preloadDir" : "loadDir"](this.resUrl,this.type, (finish: number, total: number) => {
                    if(total !== 0){
                        progress = Math.max(progress,finish / total);
                        this.progressProcess(progress);
                    }
                }, (err : any, objects : any) => {
                    if (err) {
                        cc.error(err);
                        this.loadStateProcess(LoadState.FAIL);
                        return;
                    }
                    downLoadResMap[this._loadKey] = true;
                    if (!this.isDownLoad) {
                        resMap[this._loadKey] = objects!;
                        this.asset = objects!;
                        for(let i = 0;i < objects!.length;++i){
                            let asset = objects![i] as cc.Asset;
                            let key = CCLoadItem.getKey(this.bundleName,
                                // @ts-ignore
                                bundle.getAssetInfo(asset._uuid)!.path,asset.__classname__);
                            downLoadResMap[key] = true;
                            if (!this.isDownLoad) {
                                resMap[key] = asset;
                            }

                        }
                    }
                    this.loadStateProcess(LoadState.SUCCESS);
                });
                return;
            }
        }
        {
            // @ts-ignore
            bundle[this.isDownLoad ? "preload" : "load"](this.resUrl, this.type, (finish: number, total: number) => {
                if(total != 0){
                    let progress = finish / total;
                    this.progressProcess(progress);
                }
            }, (err : any, prefab : cc.Asset) => {
                console.log("loaded " + this._loadKey);
                if (err) {
                    cc.error(err);
                    this.loadStateProcess(LoadState.FAIL);
                    return;
                }
                downLoadResMap[this._loadKey] = true;
                if (!this.isDownLoad) {
                    resMap[this._loadKey] = prefab;
                    // @ts-ignore
                    this.asset = prefab!;
                }
                this.loadStateProcess(LoadState.SUCCESS);
            });
        }
    }

    static getKey(bundleName : string,resUrl : string,type : string){
        return bundleName + "_" + resUrl + "_" + type;
    }

    /**
     * 是否已经缓存
     * @param bundleName
     * @param resUrl
     * @param type
     */
    static isCache(bundleName : string,resUrl : string,type : new()=>cc.Asset){
        let key = this.getKey(bundleName,resUrl,type.prototype.__classname__);
        return !!(resMap[key] || downLoadResMap[key])
    }

    /**
     * 是否已经缓存
     * @param bundleName
     * @param resUrl
     * @param type
     */
    static isCacheByUrl(resUrl : string,type : new()=>cc.Asset){
        let index = resUrl.indexOf(":");
        let resPath = resUrl.substr(index + 1,resUrl.length);
        let bundleName = index == -1 ? "resources" : resUrl.substr(0,index);
        let key = this.getKey(bundleName,resPath,type.prototype.__classname__);
        return !!(resMap[key] || downLoadResMap[key])
    }

    async loadStateProcess(loadState : LoadState){
        super.loadStateProcess(loadState);
        if(loadState==LoadState.FAIL){
            EventManager.emit(LoadItemEvent.LOAD_FAIL,this.type)
        }
    }

    /**
     * 设置加载优先依赖
     * @param list
     */
    static setBundleList(list : string[]){
        this.bundleList = list;
    }

}