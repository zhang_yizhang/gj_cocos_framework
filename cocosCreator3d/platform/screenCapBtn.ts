
import { _decorator, Component, Node, UIOpacity } from 'cc';
import config, { Platform } from '../../../../config/config';
import { PlatformManager } from '../../../../Script/Manager/PlatformManager';
import { screenCapEnum } from './PlatformManagerBase';

const { ccclass, property } = _decorator;



@ccclass('screenCapBtn')
export class screenCapBtn extends Component {

    start() {
        // [3]
    }

    onLoad() {
        if (config.platform != Platform.bytedance) {
            this.node.destroy();
            return
        }
        this.getComponent(UIOpacity)!.opacity = 0;
        this.node.on(Node.EventType.TOUCH_END, this.share, this)
    }

    onEnable() {
        this.getComponent(UIOpacity)!.opacity = 0
        PlatformManager.getInstance().screenCap(screenCapEnum.STOP, (time: number) => {
            this.getComponent(UIOpacity)!.opacity = (PlatformManager.getInstance().screenCapPath != '' && time >= 5) ? 255 : 0;
        })
    }
    share() {
        if ( this.getComponent(UIOpacity)!.opacity==0)return        
        PlatformManager.getInstance().screenCap(screenCapEnum.SHARE, (success: any) => {
            if (success) {
                this.getComponent(UIOpacity)!.opacity = 0
            }
        }, { title: '字谜模拟器', describe: '快看看语文是体育老师教的吗?!', topics: '字谜模拟器#抖音小游戏#猜字谜' })
    }

}


