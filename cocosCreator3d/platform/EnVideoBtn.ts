/**
@class EnVideoBtn
@author YI ZHANG
@date 2022/4/26
@desc
**/

import * as cc from "cc";
import config, {LangType} from "../../../../config/config";

const {ccclass, property} = cc._decorator;
export interface EnVideoBtnData {
}
@ccclass("EnVideoBtn")
export default class EnVideoBtn extends cc.Component {
    protected onLoad() {
        if(config.langType == LangType.en){
            let opacityComp = this.getComponent(cc.UIOpacity) || this.addComponent(cc.UIOpacity)!;
            opacityComp.opacity = 60;
        }
    }
}

