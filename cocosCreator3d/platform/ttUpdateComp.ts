/** 头条更新
@class ttUpdateComp
@author YI ZHANG
@date 2020/8/17
@desc
**/

export default class ttUpdateComp  {

    static check () {
        // @ts-ignore
        let tt = globalThis.tt || globalThis.wx;
        // @ts-ignore
        if(!tt || !tt.getUpdateManager){
            return;
        }
        const updateManager = tt.getUpdateManager();
        updateManager.onUpdateReady(() => {
            tt.showModal({
                title: "更新提示",
                content: "新版本已经准备好，是否立即使用？",
                success: function (res : any) {
                    if (res.confirm) {
                        // 调用 applyUpdate 应用新版本并重启
                        updateManager.applyUpdate();
                    } else {
                        tt.showToast({
                            icon: "none",
                            title: "小程序下一次「冷启动」时会使用新版本",
                        });
                    }
                },
            });
        });
    }

    // update (dt) {}
}
