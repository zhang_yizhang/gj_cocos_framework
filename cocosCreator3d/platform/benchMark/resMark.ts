
import { _decorator, screen, sys } from 'cc';

export class resMark {

    static getDeviceBench(): number {
        if (!sys.isMobile) return 2
        /* 获取屏幕尺寸 */
        let size = screen.windowSize;
        console.log("view size width:" + size.width + " height:" + size.height);
        if (sys.platform == sys.Platform.IOS) {
            if (size.height >= 1280) {
                /* 基本是pro系列 */
                return 4;
            }
            else if (size.height >= 1160) {
                /* iphone全面屏系列，除了x都还可以 */
                return 3;
            }
            else if (size.height >= 890) {
                return 2;
            }
            else if (size.height >= 800) {
                return 1;
            }
            else  {
                return 0;
            }
            
        } else {
            if (size.height >= 1400 && size.width > 3200) {
                /* 好屏幕配置一般不会太差 */
                return 4;
            }
            else if (size.height >= 1070 && size.width > 2300) {
                /* 比较新的全平面手机，GPU应该还可以 */
                return 3;
            }
            else if (size.height >= 1070 && size.width > 2260 && size.width <= 2300) {
                /* 是全面屏，但是屏幕比较老，配置应该一般 */
                return 2;
            }
            else if (size.height >= 1070) {
                /* 1080p的老机器 */
                return 1;
            }
            else {
                /* 1080以下的默认是渣渣 */
                return 0
            }

        }

    }


}
