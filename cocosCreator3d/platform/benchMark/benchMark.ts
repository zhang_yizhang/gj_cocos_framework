import { cleanRenderer } from './internal/cleanRenderer';
import { deviceInfo } from './internal/deviceInfo';
import { getLevenshteinDistance } from './internal/getLevenshteinDistance';
import { getGPUVersion } from './internal/getGPUVersion';
import { OutdatedBenchmarksError } from './internal/error';
import { director } from 'cc';
import { deobfuscateAppleGPU } from './internal/deobfuscateAppleGPU';
import { LoadDataFromRemote } from "../../../../../Script/Util/loadDataFromRemote";
import * as cc from "cc";
import config, { LangType } from '../../../../../config/config';
async function LoadJson(url: string) {
  return await new Promise(resolve => {
    cc.assetManager.loadRemote(url, { reload: true, cacheAsset: false, cacheEnabled: false, maxRetryCount: 1 }, (err, data) => {
      if (err) {
        // @ts-ignore
        resolve(null);
      } else {
        // @ts-ignore
        resolve(data.json);
      }
    })
  })
}
export const isSSR = typeof window === 'undefined';

// Types
export interface GetGPUTier {
  /**
   * URL of directory where benchmark data is hosted.
   *
   * @default https://unpkg.com/detect-gpu@4.0.14/dist/benchmarks
   */
  benchmarksURL?: string;
  /**
   * Optionally pass in a WebGL context to avoid creating a temporary one
   * internally.
   */
  glContext?: WebGLRenderingContext | WebGL2RenderingContext;
  /**
   * Whether to fail if the system performance is low or if no hardware GPU is
   * available.
   *
   * @default false
   */
  failIfMajorPerformanceCaveat?: boolean;
  /**
   * Framerate per tier for mobile devices.
   *
   * @defaultValue [0, 15, 30, 60]
   */
  mobileTiers?: number[];
  /**
   * Framerate per tier for desktop devices.
   *
   * @defaultValue [0, 15, 30, 60]
   */
  desktopTiers?: number[];
  /**
   * Optionally override specific parameters. Used mainly for testing.
   */
  override?: {
    renderer?: string;
    /**
     * Override whether device is an iPad.
     */
    isIpad?: boolean;
    /**
     * Override whether device is a mobile device.
     */
    isMobile?: boolean;
    /**
     * Override device screen size.
     */
    screenSize?: { width: number; height: number };
    /**
     * Override how benchmark data is loaded
     */
    loadBenchmarks?: (file: string) => Promise<ModelEntry[]>;
  };
}


export type TierResult = {
  tier: number;
  isMobile?: boolean;
  fps?: number;
  gpu?: string;
  device?: string;
};

export type ModelEntryScreen = [number, number, number, string | undefined];

export type ModelEntry = [string, string, 0 | 1, ModelEntryScreen[]];

const debug = true ? console.log : undefined;

export const getGPUTier = async ({
  /* 手机适配的等级，里面是benchmark跑出来的FPS信息 */
  mobileTiers = [15, 18, 24, 36, 50],
  /* 电脑适配的等级，电脑的CPU处理能力要好一点，一般60FPS以上，这个DEMO可以跑慢 */
  desktopTiers = [20, 40, 45, 50, 60],
  override = {},
  /* 配置的json链接，这里这个是外网的，可以把json下载到自己cdn或者保存到本地解析，后面加/可以预览 */
  benchmarksURL = `https://doubleteam.xplaymobile.com/benchmarks`,
}: GetGPUTier = {}): Promise<TierResult> => {
  if (isSSR) {
    return;
  }
  const queryCache: { [k: string]: Promise<ModelEntry[]> } = {};
  const {
    isIpad = !!deviceInfo?.isIpad,
    isMobile = !!deviceInfo?.isMobile,
    screenSize = window.screen,
    loadBenchmarks = async (file: string) => {
      let data: ModelEntry[] = []
      if (config.langType == LangType.cn) {
        data = await LoadJson(`${benchmarksURL}/${file}`);
      } else {
        console.log('loadBenchmarks:' + `gpuInfo/${file.slice(0, -5)}`)
        await new Promise((resolve) => {
          cc.resources.load(`gpuInfo/${file.slice(0, -5)}`, cc.JsonAsset, (err, data) => {
            if (err) {
              resolve(null);
            } else {
              resolve(data.json);
            }
          })
        }).then((res) => { data = res }).catch((e => { console.log('loadBenchmarks-err', JSON.stringify(e)) }))

      }

      // const data: ModelEntry[] = await LoadJson(`${benchmarksURL}/${file}`);
      // console.log(data,'benchMark')
      // Remove version tag and check version is supported
      const version = parseInt(
        (data.shift() as unknown as string).split('.')[0],
        10
      );
      if (version < 4) {
        throw new OutdatedBenchmarksError(
          'Detect GPU benchmark data is out of date. Please update to version 4x'
        );
      }
      return data;
    },
  } = override;
  let { renderer } = override;

  const getGpuType = (renderer: string) => {
    /* 判断手机还是电脑，走不同的json组 */
    console.log("test", renderer);
    const types = isMobile
      ? (['adreno', 'apple', 'mali-t', 'mali', 'nvidia', 'powervr'] as const)
      : (['intel', 'apple', 'amd', 'radeon', 'nvidia', 'geforce'] as const);
    for (const type of types) {
      if (renderer.includes(type)) {
        return type;
      }
    }
  };
  function isDefined<T>(val: T | undefined | null | void): val is T {
    return val !== undefined && val !== null;
  }

  const queryBenchmarks = async (renderer: string) => {
    const type = getGpuType(renderer);
    if (!type) {
      return;
    }

    console.log('queryBenchmarks - found type:', { type });
    /* 从组里面招对应信号的json */
    const benchmarkFile = `${isMobile ? 'm' : 'd'}-${type}${isIpad ? '-ipad' : ''
      }.json`;

    const benchmark = (queryCache[benchmarkFile] =
      queryCache[benchmarkFile] ?? loadBenchmarks(benchmarkFile));
    let benchmarks: ModelEntry[];
    try {
      benchmarks = await benchmark;
    } catch (error) {
      if (error instanceof OutdatedBenchmarksError) {
        throw error;
      }
      debug?.("queryBenchmarks - couldn't load benchmark:", { error });
      return null;
    }
    /* 获取GPU型号 */
    const version = getGPUVersion(renderer);

    let matched = benchmarks.filter(
      ([, modelVersion]) => modelVersion === version
    );

    console.log(
      `found ${matched.length} matching entries using version '${version}':`,

      matched.map(([model]) => model)
    );
    /* 型号没招到，找一下GPU名字 */
    if (!matched.length) {
      matched = benchmarks.filter(([model]) => model.includes(renderer));
      debug?.(
        `found ${matched.length} matching entries comparing model names`,
        {
          matched,
        }
      );
    }

    const matchCount = matched.length;

    if (matchCount === 0) {
      return null;
    }
    /* 通过屏幕长*宽的结果，定位到最合适的机型 */

    // eslint-disable-next-line prefer-const
    let [gpu, , , fpsesByPixelCount] =
      matchCount > 1
        ? matched
          .map(
            (match) =>
              [match, getLevenshteinDistance(renderer, match[0])] as const
          )
          .sort(([, a], [, b]) => a - b)[0][0]
        : matched[0];

    debug?.(
      `${renderer} matched closest to ${gpu} with the following screen sizes`,
      JSON.stringify(fpsesByPixelCount)
    );

    let minDistance = Number.MAX_VALUE;
    let closest: ModelEntryScreen | undefined;
    const { devicePixelRatio } = window;
    const pixelCount =
      screenSize.width *
      devicePixelRatio *
      screenSize.height *
      devicePixelRatio;

    for (const match of fpsesByPixelCount) {
      const [width, height] = match;
      const entryPixelCount = width * height;
      const distance = Math.abs(pixelCount - entryPixelCount);

      if (distance < minDistance) {
        minDistance = distance;
        closest = match;
      }
    }

    if (!closest) {
      return;
    }

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const [, , fps, device] = closest!;
    debug?.(minDistance, fps, gpu, device);

    return [minDistance, fps, gpu, device] as const;
  };

  const toResult = (
    tier: number,
    gpu?: string,
    fps?: number,
    device?: string
  ) => ({
    device,
    fps,
    gpu,
    isMobile,
    tier,
  });

  let renderers: string[];
  let rawRenderer = '';

  if (!renderer) {

    const SceneData: any = director.root!.pipeline.pipelineSceneData
    //通过Cocos pipelineSceneData 拿到gpu型号
    if(SceneData._device){
      renderer = SceneData._device._renderer
    }else{
      renderer=director.root!.pipeline.device.renderer
    }
    

    if (!renderer) {
      debug?.('FALLBACK');
      return null;
    }

    rawRenderer = renderer;
    renderer = cleanRenderer(renderer);
    /* safari12这里有做特殊处理 */
    if (renderer == 'apple gpu') {
      const webgl = SceneData._device._context
      renderers = deobfuscateAppleGPU(webgl, renderer, isMobile)
    }
    else {
      renderers = [renderer]
    }

  } else {
    renderer = cleanRenderer(renderer);
    renderers = [renderer];
  }
  const results = (await Promise.all(renderers.map(queryBenchmarks))).
    filter(isDefined).
    sort(([aDis = Number.MAX_VALUE, aFps], [bDis = Number.MAX_VALUE, bFps]) =>
      aDis === bDis ? aFps - bFps : aDis - bDis
    );
  /* 没有数据返回 */
  if (!results.length) {
    debug?.('NOTFOUND' + renderer);
    return toResult(null, `${renderer} (${rawRenderer})`)
  }


  const [, fps, model, device] = results[0];
  /* 处理下异常数据 */
  if (fps === -1) {
    debug?.('BLOCKLISTED');
    return null;
  }
  /* 根据之前定义的配置数组，返回对应数组 */
  const tiers = isMobile ? mobileTiers : desktopTiers;
  let tier = 0;
  console.log("test", tiers, fps)
  for (let i = 0; i < tiers.length; i++) {
    if (fps >= tiers[i]) {
      tier = i;
    }
  }

  return toResult(tier, model, fps, device);
};
