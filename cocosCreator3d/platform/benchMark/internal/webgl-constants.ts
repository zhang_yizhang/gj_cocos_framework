/**
 * The following defined constants and descriptions are directly ported from https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Constants
 *
 * Any copyright is dedicated to the Public Domain. http://creativecommons.org/publicdomain/zero/1.0/
 *

// Clearing buffers
// Constants passed to WebGLRenderingContext.clear() to clear buffer masks
/**
 * Passed to createShader to define a vertex shader
 * @constant {number}
 */
 export const GL_VERTEX_SHADER: number = 0x8b31;
/**
 * @constant {number}
 */
 export const GL_UNSIGNED_BYTE: number = 0x1401;
/**
 * Passed to drawElements or drawArrays to draw triangles. Each set of three vertices creates a separate triangle
 * @constant {number}
 */
 export const GL_TRIANGLES: number = 0x0004;
/**
 * Passed to bufferData as a hint about whether the contents of the buffer are likely to be used often and not change often
 * @constant {number}
 */
 export const GL_STATIC_DRAW: number = 0x88e4;
/**
 * @constant {number}
 */
 export const GL_RGBA: number = 0x1908;
/**
 * Passed to createShader to define a fragment shader
 * @constant {number}
 */
 export const GL_FRAGMENT_SHADER: number = 0x8b30;
/**
 * @constant {number}
 */
 export const GL_FLOAT: number = 0x1406;
/**
 * Passed to bindBuffer or bufferData to specify the type of buffer being used
 * @constant {number}
 */
 export const GL_ARRAY_BUFFER: number = 0x8892;
/**
 * Passed to clear to clear the current color buffer
 * @constant {number}
 */
export const GL_COLOR_BUFFER_BIT: number = 0x00004000;

// Rendering primitives
// Constants passed to WebGLRenderingContext.drawElements() or WebGLRenderingContext.drawArrays() to specify what kind of primitive to render

/**
 * Passed to drawElements or drawArrays to draw single points
 * @constant {number}
 */
export const GL_POINTS: number = 0x0000;
