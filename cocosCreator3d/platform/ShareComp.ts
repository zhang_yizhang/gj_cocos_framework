import {CCCSingletonPatternBase} from "../../mvvm/SingletonPatternBase";
import * as cc from "cc";
import {CCString} from "cc";
import config, {Platform} from "../../../../config/config";

/** 分享组件
@class ShareComp
@author YI ZHANG
@date 2020/9/3
@desc
**/
const {ccclass, property} = cc._decorator;

@ccclass
export default class ShareComp extends CCCSingletonPatternBase {
    @property(CCString)
    id : string = "";

    protected onLoad(): void {
        super.onLoad();
        if(config.platform != Platform.bytedance){
            if(this.node.isValid){
                this.node.destroy();
            }         
            return;
        }
        this.node.on(cc.Node.EventType.TOUCH_END,this.share,this);
    }

    setId(id : string){
        this.id = id;
    }

    share(){
        if(config.platform == Platform.bytedance && globalThis.tt){
            // @ts-ignore
            cc.game.pause();
            tt.shareAppMessage({
                templateId: this.id,
                query: "",
                success() {
                    console.log("分享成功");
                    cc.game.resume();
                },
                fail(e : any) {
                    console.log("分享失败");
                    cc.game.resume();
                },
            });
        }else {
            cc.game.pause();
            setTimeout(()=>{
                cc.game.resume();
            },2000)
        }
    }
}
