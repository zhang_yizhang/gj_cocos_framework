/** 录屏组件
 @class ShareComp
 @author YI ZHANG
 @date 2020/9/3
 @desc
 **/
import config, {Platform} from "../../../../config/config";
import PanelBase from "../mvvm/ui/PanelBase";
import * as cc from "cc";
import {CCInteger} from "cc";
import ASCAd from "../../../AdSDK/ASCAd";
import {PREVIEW} from "cc/env";

export enum RecordState {
    READY= 1,
    IN_RECORD= 2,
    STOP = 3,
}

const {ccclass, property} = cc._decorator;

@ccclass
export default abstract  class RecordComp extends PanelBase {
    @property(CCInteger)
    time : number = 180;
    private state!: RecordState ;
    private cb!: (isSuccess: boolean) => void;
    static recordTime: number = 0;
    static isAuto ?: boolean = false;

    static instanceList : RecordComp[] = [];
    private static interval: number = -1;
    static isShowPanel: boolean = true;

    protected onLoad(): void {
        // @ts-ignore
        if(config.platform != Platform.bytedance && config.platform != Platform.kuaishou){
            if(this.node.isValid){
                this.node.destroy();
            }            
            return;
        }
        RecordComp.instanceList.push(this);
        this._setState(RecordComp._recordState);
    }

    _setState(state : RecordState){
        this.state = state;
        switch (state) {
            case RecordState.READY:
                RecordComp.recordTime = 0;
                break;
            case RecordState.IN_RECORD:
                RecordComp.recordTime = 0;
                this.updateRecordTime(0);
                break;
            case RecordState.STOP:
                RecordComp.recordTime = 0;
                break;
        }
        this.setState(state);
    }
    /**
     * 设置录制状态
     * @param state
     */
    abstract setState(state : RecordState) : void;


    op(){
        switch (this.state) {
            case RecordState.READY:
                RecordComp.recordTime = 0;
                RecordComp.startRecord(this.time);
                break;
            case RecordState.IN_RECORD:
                if(RecordComp.recordTime < (config.platform == Platform.kuaishou ? 10 : 10)){
                    this.recordTimeTip();
                    return;
                }
                RecordComp.stopRecord();
                break;
            case RecordState.STOP:
                RecordComp.recordTime = 0;
                RecordComp.resetRecord();
                if(RecordComp.recordUrl){
                    this.openSharePanel(RecordComp.recordUrl);
                }
                break;
        }
    }

    abstract recordTimeTip() : void;

    protected update(dt: number): void {
        if(this.state == RecordState.IN_RECORD){
            this.updateRecordTime(RecordComp.recordTime);
        }

    }

    abstract updateRecordTime(time : number) : void;

    abstract openSharePanel(path : string) : void;

    /**
     * 设置分享回调
     * @param cb
     */
    setShareRecordCb(cb : (isSuccess : boolean)=>void){
        this.cb = cb;
    }




    static recordTimeOut : number;
    static _recordState = RecordState.READY
    static set recordState(value : RecordState){
        this.setRecordState(value)
    }
    static get recordState(){
        return this._recordState;
    }
    static recordUrl: string = "";

    /**
     * 开始录制
     * @param time
     * @param isAuto
     */
    static startRecord(time: number,isAuto : boolean = false) {
        if (time > 240) {
            cc.warn("record finishTime > 240");
        }
        if (this.recordState != RecordState.READY && this.recordState != RecordState.STOP) {
            return;
        }
        this.isAuto = isAuto;
        ASCAd.getInstance().startGameVideo(time + 60);
        clearTimeout(RecordComp.recordTimeOut);
        RecordComp.recordTimeOut = setTimeout(() => {
            if (this.recordState == RecordState.IN_RECORD) {
                this.stopRecord();
            }
        }, time * 1000);
        this.recordState = RecordState.IN_RECORD;
        this.interval = setInterval(()=>{
            this.recordTime += 0.2; 
        },200)
    }

    /**
     * 停止录制
     */
    static stopRecord(cb ?: (url: string) => void,isShowPanel : boolean = true) {
        // @ts-ignore
        if(RecordComp.recordTime < (config.platform == Platform.kuaishou ? 10 : 10) || config.platform != Platform.bytedance && config.platform != Platform.kuaishou){
            cb && cb("");
            return;
        }
        if (this.recordState != RecordState.IN_RECORD) {
            cb && cb("");
            return;
        }
        this.isShowPanel = isShowPanel;
        clearInterval(this.interval);
        clearTimeout(RecordComp.recordTimeOut);

        if (PREVIEW) {
            this.recordUrl = "test";
            this.recordState = RecordState.STOP;
            cb && cb("test");
        }else {
            ASCAd.getInstance().stopGameVideo((url: string) => {
                if (url) {
                    this.recordUrl = url;
                    this.recordState = RecordState.STOP;
                } else {
                    this.recordState = RecordState.READY;
                }
                cb && cb(url);
            });
        }

    }

    /**
     * 重置录制
     */
    static resetRecord() {
        if (this.recordState == RecordState.IN_RECORD) {
            clearTimeout(RecordComp.recordTimeOut);
            ASCAd.getInstance().stopGameVideo(() => {
            });
        }
        this.recordState = RecordState.READY;
    }

    /**
     * 分享录屏
     * @param title
     * @param desc
     * @param topics
     * @param cb
     */
    static shareRecord(title: string, desc: string, topics: string, cb ?: (isSuccess: boolean) => void) {
        if (!this.recordUrl) {
            this.setRecordState(RecordState.READY);
            cb && cb(false);
            return;
        }
        this.setRecordState(RecordState.READY);
        if (PREVIEW) {
            cb && cb(true);
        }else {
            ASCAd.getInstance().shareVideo(title, desc, topics, this.recordUrl, (isSuccess: boolean) => {
                cb && cb(isSuccess);
            });
        }
    }

    static setRecordState(state : RecordState){
        if(this._recordState !== state){
            this._recordState = state;
            this.instanceList.forEach(value => {
                if(cc.isValid(value) && value.enabledInHierarchy){
                    value._setState(this._recordState);
                }
            })
        }
    }
}
