/** 平台管理
 @class PlatformManagerBase
 @author YI ZHANG
 @date 2020/7/14
 @desc
 **/


import config, { AndroidsPlatform, Platform } from "../../../../config/config";
import SingletonPatternBase from "../../mvvm/SingletonPatternBase";
import EventManager from "../../Event/EventManager";
import * as cc from "cc";
import ASCAd from "../../../AdSDK/ASCAd";
import { AudioEngine } from "../utils/AudioEngine";
import { UiManager } from "../../mvvm/UiManager/UiManager";
import { DEBUG } from "cc/env";
import GetConfig from "../../../AdSDK/utils/GetConfig";
import { AdType } from "../../adUI/AdBase";
import { resMark } from "./benchMark/resMark";
import { getGPUTier } from "./benchMark/benchMark";
import ModelManager from "../../../../Script/MVVM/Model/ModelManager";

export enum VibrateType {
    LONG = "long",
    SHORT = "short"
}

export let PlatformManagerEvent = {
    NO_VIDEO_AD: "NO_VIDEO_AD",
    SHOW_VIDEO_SUCCESS: "SHOW_VIDEO_SUCCESS",
    SHOW_INTERS_SUCCESS: "SHOW_INTERS_SUCCESS",
    WAIT_VIDEO_LOADING: 'WAIT_VIDEO_LOADING',
};

export enum screenCapEnum {
    START, STOP, SHARE
}

export enum AndroidEnum {
    XIAO_MI = 1,
    OPPO = 2,
    VIVO = 3,
    HUA_WEI = 4,
    AD = 5,
    HUA_WEI_EN = 6,
    A233 = 7,
    DOU_YIN = 8,
}

let AndroidMap = {
    "1005": AndroidEnum.XIAO_MI,
    "1007": AndroidEnum.OPPO,
    "1008": AndroidEnum.VIVO,
    "1012": AndroidEnum.HUA_WEI,
    "1238": AndroidEnum.AD,
    "1182": AndroidEnum.HUA_WEI_EN,
    "1208": AndroidEnum.A233,
    "2003": AndroidEnum.DOU_YIN
};

export let CCAndroidEnum = cc.Enum(AndroidEnum);
export let PlatformEnum = cc.Enum(Platform);
let reportErrorMap: any = {};
let MAX_REPORT_NUM = 10;
let curReportNum = 0;
let isFirstFrame = true;
setTimeout(() => {
    isFirstFrame = false;
});
// @ts-ignore
window.__errorHandler = function (file, line, msg, error) {
    let err = JSON.stringify({ file, line, msg, error });
    if (reportErrorMap[err] || (++curReportNum) > MAX_REPORT_NUM) {
        return;
    }
    reportErrorMap[err] = true;
    if (!isFirstFrame) {
        ASCAd.getInstance().reportAnalytics("jsError", { file, line, msg, error });
        return;
    }
    setTimeout(() => {
        ASCAd.getInstance().reportAnalytics("jsError", { file, line, msg, error });
    });
};
// @ts-ignore
window.onerror = window.__errorHandler;
export default class PlatformManagerBase extends SingletonPatternBase {
    private performanceDevice: number = -1;

    static getInstance<T extends {}>(this: (new () => T)): T {
        if (!(<any>this).instance) {
            (<any>this).instance = new this();
            // (<any>this).instance.init();
        }
        return (<any>this).instance;
    }

    constructor() {
        super();

    }
    async getGPUtier(): Promise<number> {
        /* 获取适配信息 */
        const data = await getGPUTier();
        let tier: number
        if (data && data.tier) {
            tier = data.tier
        }
        else {
            /* 适配信息不存在，通过分辨率盘淡定 */
            tier = resMark.getDeviceBench()
        }
        tier = Number(tier)
        return <number>tier;
    }

    async init() {
        ASCAd.getInstance().initAd();
        ASCAd.getInstance().setGroup(15);
        if (config.platform != Platform.kuaishou && config.platform != Platform.bytedance) {
            this.performanceDevice = await this.getGPUtier() + 1;
            console.log("机器性能:", this.performanceDevice, cc.game.frameRate);
        } else {
            this.performanceDevice = 3;
        }

    }

    /**
     * 显示隐私协议
     * @param cb
     * @param type
     */
    showPrivacyAgreement(cb?: () => void, type = "",) {
        let flag = cc.sys.localStorage.getItem("showPrivacyAgreement_flag");
        if (flag || (config.platform != Platform.huawei && config.platform != Platform.oppo && config.platform != Platform.vivo && config.platform != Platform.qq)) {
            cb && cb();
            return;
        }
        // let id = UiManager.getInstance().showBlock({ isNoShowBlock: true, timeOut: Number.MAX_VALUE });
        ASCAd.getInstance().showPrivacyAgreement(type, "", ((suc: boolean) => {
            // UiManager.getInstance().closeBlock(id);
            if (suc) {
                console.log("点击同意,正常进入游戏");
                cc.sys.localStorage.setItem("showPrivacyAgreement_flag", true);
                cb && cb();
            } else {
                if (DEBUG) {
                    console.log("点击取消");
                    // @ts-ignore
                } else if (window.hbs) {
                    // @ts-ignore
                    hbs.showToast({
                        title: '不同意隐私协议无法继续游戏！',
                        icon: 'none',
                        mask: true,
                        duration: 2000
                    });
                    this.showPrivacyAgreement(cb, type);
                    // @ts-ignore
                } else if (window.qg) {
                    console.log("点击取消,无法进入游戏");
                    // @ts-ignore
                    qg.exitApplication({});
                    cb && cb();
                }  // @ts-ignore
                else if (window.qq) {
                    console.log("点击取消,无法进入游戏");
                    // @ts-ignore
                    qq.exitMiniProgram({});
                    cb && cb();
                }
            }
        }))
    }


    /**
     * 视频广告
     * @param callback
     * @param extData
     * @param isPause
     * @param scene
     */
    showVideoAD(callback: (isSuccess: boolean) => void, scene?: string, isPause?: boolean, extData?: any) {
        scene && ASCAd.getInstance().reportVideoClick(scene);
        if (!ASCAd.getInstance().getVideoFlag()) {
            callback(false);
            EventManager.emit(PlatformManagerEvent.NO_VIDEO_AD);
            return;
        }
        AudioEngine.getInstance().pauseAll();
        let id = UiManager.getInstance().showBlock({ isNoShowBlock: true });
        setTimeout(() => {//屏蔽500毫秒
            UiManager.getInstance().closeBlock(id);
        }, 500);
        setTimeout(() => {
            if (isPause && GetConfig.getChannelName() != "test" && ASCAd.getInstance().getVideoFlag()) {
                cc.director.pause();
            }
            ASCAd.getInstance().showVideo((isSuccess: boolean) => {
                if (config.platform == Platform.oppo) {
                    setTimeout(() => {
                        AudioEngine.getInstance().resumeAll();
                    }, 500);
                } else {
                    AudioEngine.getInstance().resumeAll();
                }
                setTimeout(() => {
                    if (isPause && GetConfig.getChannelName() != "test") {
                        cc.director.resume();
                    }
                    setTimeout(() => {
                        callback(isSuccess);
                        if (isSuccess) {
                            EventManager.emit(PlatformManagerEvent.SHOW_VIDEO_SUCCESS);
                        }
                    })
                }, 0.05);


            }, scene);
        }, 50);
        return true;
    };

    getVideoFlag() {
        return ASCAd.getInstance().getVideoFlag()
    }


    isNative() {
        return config.platform == Platform.androids || config.platform == Platform.ios;
    }

    /**
     * 视频广告直接回调版
     * @param callback
     * @param type
     */
    showVideoADEx(callback: () => void, reportScence: string = '') {
        this.showVideoAD((isSuccess: boolean) => {
            isSuccess && callback && callback();
        }, reportScence);
    };

    interBlockImage?: cc.Node;//vivo插屏屏蔽大图
    /** 插屏 ------------------------------------------------------------------------------------------------------------------ */
    getIntersFlag() {
        return ASCAd.getInstance().getIntersFlag()
    }
    /**
     * 插屏
     */
    showIntersAD(callback: () => void, reportScence: string, flagCallback?: (flag: boolean) => void) {
        if (config.platform == Platform.webTest) {
            callback && callback()
            flagCallback && flagCallback(true)
            return false;
        }

        if (ASCAd.getInstance().getIntersFlag(reportScence)) {
            flagCallback && flagCallback(true)
            if (config.platform == Platform.vivo && !this.interBlockImage) {
                this.interBlockImage = new cc.Node();
                (<any>this.interBlockImage.addComponent("BlockAdComp")).adType = AdType.NativeImage;
                cc.director.getScene()!.addChild(this.interBlockImage);
            }
            const isMusicPlaying = AudioEngine.getInstance().isMusicPlaying()
         
            if (isMusicPlaying&&(config.platform == Platform.ios || config.platform === Platform.google)) {
                AudioEngine.getInstance().pauseAll();
            }
            ASCAd.getInstance().showInters(() => {
                callback();
                if (isMusicPlaying&&(config.platform == Platform.ios || config.platform === Platform.google)) {
                    AudioEngine.getInstance().resumeAll();
                }

                this.interBlockImage && this.interBlockImage.destroy();
                this.interBlockImage = undefined;
            }, reportScence);
            return true;
        } else {
            flagCallback && flagCallback(false)
            this.interBlockImage && this.interBlockImage.destroy();
            this.interBlockImage = undefined;
        }
        return false;
    }



    /**
     * banner
     */
    showBannerAD(isShow: boolean) {
        if (config.platform == Platform.webTest) {
            console.log('showBannerAD:'+isShow)
            return;
        }
        if (isShow) {
            ASCAd.getInstance().showBanner(1);
        } else {
            ASCAd.getInstance().hideBanner();
        }
    }

    /**
     * WX
     * banner误触
     * @param callback 回调
     */
    // showErrBanner(callback: (isSuccess: boolean) => void) {
    //     if (config.platform != Platform.wechat) {
    //         callback(false);
    //         return;
    //     }
    //     if (ASCAd.getInstance().getErrBannerFlag()) {
    //         let node = new cc.Node();
    //         (<any>node.addComponent("BlockAdComp")).adType = AdType.Banner;
    //         ASCAd.getInstance().hideBanner();
    //         cc.director.getScene()!.addChild(node);
    //         ASCAd.getInstance().showErrBanner((suc: any) => {
    //             cc.isValid(node) && node.destroy();
    //             callback(suc);
    //         })
    //     } else {
    //         callback(false);
    //     }
    // }

    /**
     * 微信视频误触
     * @param callback
     */
    showErrVideo(callback: (isSuccess: boolean) => void) {
        if (config.platform != Platform.wechat) return false;
        // // if (ASCAd.getInstance().getErrVideoFlag()) {
        //     this.showVideoAD(callback);
        //     return true;
        // }
        return false;
    }


    /**
     * Android & IOS & WX
     * 展示互推列表(OPPO仅竖版可用)--圆球状-上线版本可拖动
     * 游戏主页调用，大厅页和游戏页
     * @param type vertical-竖版 (不支持--horizontal-横版)
     * @param side left-左侧 right-右侧
     * @param size OPPO - 按钮大小
     * @param y OPPO - 按钮的纵坐标,默认0,处在屏幕左侧或者右侧中间
     */
    showNavigateGroup(type: string, side: string, size: number, y: number) {
        if (config.platform != Platform.wechat) return;
        ASCAd.getInstance().getNavigateGroupFlag() && ASCAd.getInstance().showNavigateGroup(type, side, size, y);
    }

    /**
     * 展示收藏
     * @param cb
     */
    showFavoriteGuide(cb: (isSuccess: boolean) => void) {
        //@ts-ignore
        if (config.platform == Platform.bytedance && globalThis.tt) {
            let count = cc.sys.localStorage.getItem("showFavoriteGuide_count");
            count = count ? JSON.parse(count) : 0;
            if (count >= 2) {
                return;
            }
            let time = cc.sys.localStorage.getItem("showFavoriteGuide_time");
            time = time ? JSON.parse(time) : 0;
            if (new Date().getTime() - time < 7 * 24 * 60 * 60 * 1000) {
                return;
            }
            cc.sys.localStorage.setItem("showFavoriteGuide_count", JSON.stringify(count + 1));
            cc.sys.localStorage.setItem("showFavoriteGuide_time", JSON.stringify(new Date().getTime()));
            // @ts-ignore
            tt.showFavoriteGuide({
                type: "bar",
                content: "一键添加到我的小程序",
                position: "bottom",
                success(res: any) {
                    cb && cb(true);
                    console.log("引导组件展示成功");
                },
                fail(res: any) {
                    console.log("引导组件展示失败");
                    cb && cb(false);
                },
            });
        }
    }


    prePhoneVibrate = 0;

    /**
     * 震动
     * @param type
     */
    phoneVibrate(type: VibrateType) {
        let now = new Date().getTime();
        if (now - this.prePhoneVibrate > 300) {
            ASCAd.getInstance().phoneVibrate(type);
            this.prePhoneVibrate = now;
        }

    }

    //弹出用户评论(1-5星)
    showReviewAlert() {
        ASCAd.getInstance().showReviewAlert();
    }

    //网络判断
    getNetworkstatus() {
        return ASCAd.getInstance().getNetworkstatus();
    }

    /**
     * 请求远程配置
     * @param callback 回调
     * @returns 
     */
    reqRemoteConfig(callback: Function) {
        return ASCAd.getInstance().reqRemoteConfig(callback)
    }

    getRemoteConfig(key: string) {
        return ASCAd.getInstance().getRemoteConfig(key);

    }

    /**
     * 是否是高性能
     */
    async getPerformance() {
        // if (DEBUG) {
        //     return 4;
        // }
        console.log('getPerformance:',this.performanceDevice)
        if (this.performanceDevice === -1) {
            if (config.platform == Platform.bytedance) {
                // @ts-ignore
                if (globalThis.tt) {
                    try {
                        // @ts-ignore
                        let info = tt.getSystemInfoSync();
                        let overall = info && info.deviceScore && info.deviceScore.gpu || 6.0;
                        // @ts-ignore
                        console.log(JSON.stringify(tt.getSystemInfoSync().deviceScore));
                        this.performanceDevice = overall > 9 ? 5 : (overall > 8.2 ? 4 : (overall > 7.3 ? 3 : (overall > 5.9 ? 2 : 1)));
                        if (info && info.brand == "OPPO") {
                            this.performanceDevice--;
                        }
                    } catch (e) {
                        this.performanceDevice = 2;
                    }
                    return this.performanceDevice;
                }
            }else{
                if (config.platform != Platform.kuaishou ) {
                    this.performanceDevice = await this.getGPUtier() + 1;
                    console.log("机器性能:", this.performanceDevice, cc.game.frameRate);
                } else {
                    this.performanceDevice = 3;
                }
            }
            // this.performanceDevice = 4;
        }
        return this.performanceDevice;
    }

    static getAndroidChannel(): AndroidEnum {
        if (DEBUG && config.platform == Platform.webTest) {
            switch (config.androidsPlatform) {
                case AndroidsPlatform.huawei:
                    return AndroidEnum.HUA_WEI
                case AndroidsPlatform.xiaomi:
                    return AndroidEnum.XIAO_MI
                case AndroidsPlatform.oppo:
                    return AndroidEnum.OPPO
                case AndroidsPlatform.vivo:
                    return AndroidEnum.VIVO
                case AndroidsPlatform.ad:
                    return AndroidEnum.AD
            }
        }
        let id = AndroidMap[<1007>ASCAd.getInstance().getChannelId()];
        return id;
    }

    static getChannelName(): (keyof typeof Platform) | "androids_XIAO_MI" | "androids_OPPO" | "androids_AD" | "androids_VIVO" | "androids_HUA_WEI" {
        let name = Platform[config.platform];
        if (config.platform == Platform.androids) {
            if (CCAndroidEnum[this.getAndroidChannel()]) {
                name += "_" + CCAndroidEnum[this.getAndroidChannel()];
            } else {
                //直接用渠道号判断
                name += "_" + ASCAd.getInstance().getChannelId();
            }

            console.log('getChannelName', name)
        }
        return <any>name;
    }

    screenCapPath: string = '';
    screenCapTime: number = Date.now();
    screenCap(type: screenCapEnum, callback: Function = () => { }, data?: any) {
        switch (type) {
            case screenCapEnum.START: {
                console.log('screenCap', 'start');
                this.screenCapPath = '';
                this.screenCapTime = Date.now();
                ASCAd.getInstance().startGameVideo(data ? data : 60);
                break;
            }
            case screenCapEnum.STOP: {
                console.log('screenCap', 'stop');
                ASCAd.getInstance().stopGameVideo((videoPath: string) => {
                    this.screenCapPath = videoPath;
                    callback((Date.now() - this.screenCapTime) / 1000);
                })
                break;
            }
            case screenCapEnum.SHARE: {
                console.log('screenCap', 'share');
                ASCAd.getInstance().shareVideo(data.title!, data.describe!, data.topics!, this.screenCapPath, callback);
                break;
            }
        }



    }

    /**
     * 点击隐私协议
     */
    openProtocol() {
        ASCAd.getInstance().openProtocol();
    }
}
