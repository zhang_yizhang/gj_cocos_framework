
   
/**
 *  开放文档API
 *  https://microapp.bytedance.com/docs/zh-CN/mini-game/develop/open-capacity/ranking/challenge-to-ranking
 */
export let _env = window.wx || window.tt ||window.qq
export class openData {
    static login(callback:Function) {
        if (!_env) return
        _env.login({
            force: true,
            success(res: { code: any; anonymousCode: any; }) {
                console.log(`bytedance-login 调用成功${res.code} ${res.anonymousCode}`);
                callback('success')
            },
            fail(res: any) {
                console.log(`bytedance-login 调用失败`);
                callback('fail')
            },
            complete(res: { code: any; anonymousCode: any; }) {
                console.log(`bytedance-login 调用完成${res.code} ${res.anonymousCode}`);
                callback('complete')
            },
        });
    }

    /**
     * 可检查用户当前的 session 状态是否有效
     */
    static checkSession() {
        if (!_env) return
        _env.checkSession({
            success() {
                console.log(`session 未过期`);
            },
            fail() {
                console.log(`session 已过期，需要重新登录`);
                openData.login(()=>{})
            },
        });
    }

    static setUserGroup(groupId: string) {
        if (!_env) return
        console.log('ffffffffffffffffff-setUserGroup')
        _env.setUserGroup({
            groupId: groupId,
        });
    }

    static setUserCloudStorage(key: string, data: any) {
        if (!_env) return
        // const data = {
        //     ttgame: {
        //       score: 16,
        //       update_time: 1513080573,
        //     },
        //     cost_ms: 36500,
        //   };
        console.log('ffffffffffffffffff-setUserCloudStorage')
        _env.setUserCloudStorage({
            KVDataList: [
                // key 需要在开发者后台配置，且配置为排行榜标识后，data 结构必须符合要求，否则会 set 失败
                { key: key, value: JSON.stringify(data) },
            ],
            success: (res) => {
                console.log("setUserCloudStorage-调用成功", res);
                console.log(res.errMsg);
            },
            fail: (res) => {
                console.log("setUserCloudStorage-调用失败", res);
            },
            complete: (res) => {
                console.log("setUserCloudStorage-调用完成", res);
            },
        }
        );
    }

    // static getCloudStorageByRelation(type: RelationType, keyList: string[], sortKey: string, groupId: string) {
    //     if (!tt) return
    //     console.log('ffffffffffffffffff-getCloudStorageByRelation')
    //     tt.getCloudStorageByRelation({
    //         type: type,
    //         keyList: keyList,
    //         extra: {
    //             sortKey: sortKey, // 指定的key需要在后台配置过
    //             groupId: groupId, // 指定要获取的用户所属分组
    //         },
    //         success(res) {
    //             console.log('getCloudStorageByRelation-获取数据成功', res);
    //         },
    //         fail(e) {
    //             console.log("getCloudStorageByRelation-获取数据失败");
    //         },
    //     });
    // }

}

// export enum RelationType {
//     FRIEND = 'friend',
//     RECOMMEND = 'recommend',
//     GROUP = 'group'
// }
