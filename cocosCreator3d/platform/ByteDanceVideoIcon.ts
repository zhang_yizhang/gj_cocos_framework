import {_decorator, Component, Sprite, SpriteFrame} from 'cc';
import config, {Platform} from "../../../../config/config";

const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = ByteDanceVideoIcon
 * DateTime = Tue Mar 15 2022 14:45:21 GMT+0800 (中国标准时间)
 * Author = zhangyizhang
 * FileBasename = ByteDanceVideoIcon.ts
 * FileBasenameNoExtension = ByteDanceVideoIcon
 * URL = db://assets/a_SubModule/lib/cocosCreator3d/platform/ByteDanceVideoIcon.ts
 * ManualUrl = https://docs.cocos.com/creator/3.4/manual/zh/
 *
 */
 
@ccclass('ByteDanceVideoIcon')
export class ByteDanceVideoIcon extends Component {
    @property(SpriteFrame)
    icon : SpriteFrame = null!;
    onLoad () {
        if(config.platform == Platform.bytedance){
            this.getComponent(Sprite)!.spriteFrame = this.icon;
        }
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}

/**
 * [1] Class member could be defined like this.
 * [2] Use `property` decorator if your want the member to be serializable.
 * [3] Your initialization goes here.
 * [4] Your update function goes here.
 *
 * Learn more about scripting: https://docs.cocos.com/creator/3.4/manual/zh/scripting/
 * Learn more about CCClass: https://docs.cocos.com/creator/3.4/manual/zh/scripting/decorator.html
 * Learn more about life-cycle callbacks: https://docs.cocos.com/creator/3.4/manual/zh/scripting/life-cycle-callbacks.html
 */
