/** 视频按钮动画
@class BtnAnim
@author YI ZHANG
@date 2021/8/10
@desc
**/
import { _decorator, Component } from 'cc';
import * as cc from "cc";
import { RemoteConfig } from '../../../../Script/data/RemoteConfig';

const { ccclass, property } = _decorator;

enum BtnAnimEnum {
    REPEAT_SCALE,
}

@ccclass('BtnAnim')
export class BtnAnim extends Component {
    @property({ type: cc.Enum(BtnAnimEnum) })
    animType: BtnAnimEnum = BtnAnimEnum.REPEAT_SCALE;
    //@ts-ignore
    @property({ visible() { return this.animType == BtnAnimEnum.REPEAT_SCALE } })
    remoteFlag: boolean = false;
    start() {
        switch (this.animType) {
            case BtnAnimEnum.REPEAT_SCALE: {
                if (!RemoteConfig.VIDEO_BTN_SCALE && this.remoteFlag) {
                    return;
                }
                let scale = this.node.scale.x;
                cc.tween(this.node).repeat(1000000, cc.tween(this.node).to(0.3, {
                    scale: cc.v3(1.1 * scale, 1.1 * scale, 1.1 * scale)
                }).to(0.3, { scale: cc.v3(scale, scale, scale) }).delay(0.1)).start();
                break;
            }
        }

    }


}
