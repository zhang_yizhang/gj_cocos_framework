/** 渠道精灵图切换
 @class PlatformSpriteFramesSwitch
 @author ChenymCC
 @date 2022/03/21
 @desc
 **/
import * as cc from "cc";
import config, { Platform } from "../../../../config/config";
import { AndroidEnum, CCAndroidEnum, PlatformEnum } from "../platform/PlatformManagerBase";


const { requireComponent } = cc._decorator;


const { ccclass, property } = cc._decorator;

@ccclass("PlatformSprite")
class PlatformSprite {
    @property({ type: cc.Enum(PlatformEnum) })
    platform: Platform = Platform.bytedance;
    @property({ type: cc.SpriteFrame })
    spriteFrame: cc.SpriteFrame = null!;
}

@ccclass
@requireComponent(cc.Sprite)
export default class PlatformSpriteFramesSwitch extends cc.Component {
    // @property({type : [PlatformEnum]})
    // platformEnumList : Platform[] = [Platform.vivo];
    // @property({type : [CCAndroidEnum]})
    // androidEnumList : AndroidEnum[] = [];

    @property({ type: [PlatformSprite], visible(this: cc.Component) { return !!this.getComponent(cc.Sprite) } })
    platformSpriteList: PlatformSprite[] = [];

    // @property({ type: [PlatformSprite], visible(this: cc.Component) { return !!this.getComponent(cc.Sprite) } })
    // androidSpriteList: PlatformSprite[] = [];



    protected __preload() {
        // let spriteFrame = this.getComponent(cc.Sprite)?.spriteFrame;
        for (let i = 0; i < this.platformSpriteList.length; ++i) {
            let item = this.platformSpriteList[i];
            if (item.platform != config.platform) {
                continue;
            }
            let sprite = this.getComponent(cc.Sprite)!;
            if (sprite) {
                sprite.spriteFrame = item.spriteFrame;
            }
        }

    }

}
