/** 节点池工具
@class PoolTool
@author YI ZHANG
@date 2020/7/21
@desc
**/
import * as cc from "cc";
import CCPanelBase from "../mvvm/ui/PanelBase";
import {Component} from "cc";
let nullCb = ()=>{};

/*
 Copyright (c) 2016 Chukong Technologies Inc.
 Copyright (c) 2017-2020 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

/**
 * @packageDocumentation
 * @hidden
 */


type Constructor<T = {}> = new(...args: any[]) => T;

interface IPoolHandlerComponent extends Component {
    unuse (): void;

    reuse (args: any): void;
}

export class NodePool {

    /**
     * @en The pool handler component, it could be the class name or the constructor.
     * @zh 缓冲池处理组件，用于节点的回收和复用逻辑，这个属性可以是组件类名或组件的构造函数。
     */
    public poolHandlerComp?: Constructor<IPoolHandlerComponent> | string;
    private _pool: cc.Node[];
    isCache: boolean | undefined;
    private cachePos: cc.Vec3 | undefined;

    /**
     * @en
     * Constructor for creating a pool for a specific node template (usually a prefab).
     * You can pass a component (type or name) argument for handling event for reusing and recycling node.
     * @zh
     * 使用构造函数来创建一个节点专用的对象池，您可以传递一个组件类型或名称，用于处理节点回收和复用时的事件逻辑。
     * @param poolHandlerComp @en The constructor or the class name of the component to control the unuse/reuse logic. @zh 处理节点回收和复用事件逻辑的组件类型或名称。
     * @example
     * import { NodePool, Prefab } from 'cc';
     *  properties: {
     *      template: Prefab
     *     },
     *     onLoad () {
     *       // MyTemplateHandler is a component with 'unuse' and 'reuse' to handle events when node is reused or recycled.
     *       this.myPool = new NodePool('MyTemplateHandler');
     *     }
     *  }
     */
    constructor (poolHandlerComp?: Constructor<IPoolHandlerComponent> | string,isCache ?: boolean,cachePos ?: cc.Vec3) {
        this.poolHandlerComp = poolHandlerComp;
        this._pool = [];
        this.isCache = isCache;
        if(isCache && !cachePos){
            throw new Error("no define cachePos");
        }
        if(cachePos){
            this.cachePos = cachePos.clone();
        }
    }

    /**
     * @en The current available size in the pool
     * @zh 获取当前缓冲池的可用对象数量
     */
    public size () {
        return this._pool.length;
    }

    /**
     * @en Destroy all cached nodes in the pool
     * @zh 销毁对象池中缓存的所有节点
     */
    public clear () {
        const count = this._pool.length;
        for (let i = 0; i < count; ++i) {
            if( this._pool[i].isValid){
                this._pool[i].destroy();
            }
           
        }
        this._pool.length = 0;
    }

    /**
     * @en Put a new Node into the pool.
     * It will automatically remove the node from its parent without cleanup.
     * It will also invoke unuse method of the poolHandlerComp if exist.
     * @zh 向缓冲池中存入一个不再需要的节点对象。
     * 这个函数会自动将目标节点从父节点上移除，但是不会进行 cleanup 操作。
     * 这个函数会调用 poolHandlerComp 的 unuse 函数，如果组件和函数都存在的话。
     * @example
     * import { instantiate } from 'cc';
     * const myNode = instantiate(this.template);
     * this.myPool.put(myNode);
     */
    public put (obj: cc.Node) {
        if (obj && this._pool.indexOf(obj) === -1) {
            // Remove from parent, but don't cleanup
            if(this.isCache){
               obj.setPosition(this.cachePos!);
               obj._static = true;
            }else {
                obj.removeFromParent();
            }
            // Invoke pool handler
            // @ts-ignore
            const handler = this.poolHandlerComp ? obj.getComponent(this.poolHandlerComp) : null;
            if (handler && handler.unuse) {
                handler.unuse();
            }

            this._pool.push(obj);
        }
    }

    /**
     * @en Get a obj from pool, if no available object in pool, null will be returned.
     * This function will invoke the reuse function of poolHandlerComp if exist.
     * @zh 获取对象池中的对象，如果对象池没有可用对象，则返回空。
     * 这个函数会调用 poolHandlerComp 的 reuse 函数，如果组件和函数都存在的话。
     * @param args - 向 poolHandlerComp 中的 'reuse' 函数传递的参数
     * @example
     *   let newNode = this.myPool.get();
     */
    public get (...args: any[]): cc.Node | null {
        const last = this._pool.length - 1;
        if (last < 0) {
            return null;
        }
        else {
            // Pop the last object in pool
            const obj = this._pool[last];
            obj._static = false;
            this._pool.length = last;

            // Invoke pool handler
            // @ts-ignore
            const handler = this.poolHandlerComp ? obj.getComponent(this.poolHandlerComp) : null;
            if (handler && handler.reuse) {
                handler.reuse(arguments);
            }
            return obj;
        }
    }
}


export class PoolTool {
    private Pool : NodePool;
    readonly template :  cc.Prefab;
    effectPool : {[key : string] : PoolTool} = {};
    private poolHandlerComp: Constructor<IPoolHandlerComponent> | undefined;
    protected constructor(template : cc.Prefab | cc.Node,poolHandlerComp?: Constructor<IPoolHandlerComponent>,isCache ?: boolean,cachePos ?: cc.Vec3) {
        this.Pool = new NodePool(poolHandlerComp,isCache,cachePos);
        this.poolHandlerComp = poolHandlerComp;
        if(template instanceof cc.Node){
            this.template = new cc.Prefab();
            this.template.data = template;
        }else {
            this.template = template;
        }

    }
    static getInstance(template : cc.Prefab | cc.Node,poolHandlerComp?: Constructor<IPoolHandlerComponent>,isCache ?: boolean,cachePos ?: cc.Vec3){
        if(template instanceof cc.Node){
            template.removeFromParent();
        }
        return new PoolTool(template,poolHandlerComp,isCache,cachePos);
    }
    put(item : cc.Node){
        if(!cc.isValid(item)){
            return;
        }
        if(item.getComponent(CCPanelBase)){
            item.getComponent(CCPanelBase)!.watchManager.removeAllWatch();
        }
        // @ts-ignore
        if(this.Pool.poolHandlerComp && !item.getComponent(this.Pool.poolHandlerComp)){
            // @ts-ignore
            item.addComponent(this.Pool.poolHandlerComp);
        }
        cc.Tween.stopAllByTarget(item);
        this.Pool.put(item);
    }

    putAllChildren(parent : cc.Node,excludeNode ?: cc.Node){
        let list = [...parent.children];
        for(let i = 0;i < list.length;++i){
            if(excludeNode == list[i]){
                continue;
            }
            this.put(list[i]);
        }
    }

    get(parent ?: cc.Node,...args : any) : cc.Node{
        let item =  this.Pool.get(...args) || <cc.Node>cc.instantiate(this.template);
        // @ts-ignore
        item._poolTool = this;
        if(parent && (!this.Pool.isCache || !item.parent)){
            parent.addChild(item);
        }
        return item;
    }

    static getPoolCompont(unuse ?: ()=>void,reuse ?: (args: any)=>{}) : Constructor<IPoolHandlerComponent>{
        class pool extends cc.Component{
            unuse!: () => void;
            reuse!: (args: any) => void;
        }
        pool.prototype.unuse = unuse || nullCb;
        pool.prototype.reuse = reuse || nullCb;

        return pool;
    }


    static getTemplateComp<T extends Component>(type: {prototype: T},node : cc.Node): T | undefined{
        // @ts-ignore
        let pool = node._poolTool;
        if(!pool){
            return;
        }
        return pool.template.getComponent(type);
    }

    static getEffectPool(name : string,node : cc.Node,effectNode : cc.Node[]) : PoolTool | undefined{
        // @ts-ignore
        let pool = node._poolTool;
        if(!pool){
            return;
        }
        return pool.getEffectPool(name,node,effectNode);
    }

    getEffectPool(name : string,node : cc.Node,effectNode : cc.Node[]) : PoolTool{
        if(this.effectPool[name]){
            if(effectNode){
                for(let i = 0;i < effectNode.length;++i){
                    this.effectPool[name].put(effectNode[i]);
                }
            }
            return this.effectPool[name];
        }else {
            this.effectPool[name] = PoolTool.getInstance(effectNode[0]);
            for(let i = 1;i < effectNode.length;++i){
                this.effectPool[name].put(effectNode[i]);
            }
        }
        return this.effectPool[name];
    }

    reuse(item : cc.Node){
        this.poolHandlerComp && this.poolHandlerComp.prototype.reuse && this.poolHandlerComp.prototype.reuse.call({node : item});
    }

    recovery(item : cc.Node){
        cc.Tween.stopAllByTarget(item);
        if(item.getComponent(CCPanelBase)){
            item.getComponent(CCPanelBase)!.watchManager.removeAllWatch();
        }
        this.poolHandlerComp && this.poolHandlerComp.prototype.unuse && this.poolHandlerComp.prototype.unuse.call({node : item});
        // // @ts-ignore
        // if(this.Pool.poolHandlerComp && !item.getComponent(this.Pool.poolHandlerComp)){
        //     // @ts-ignore
        //     item.addComponent(this.Pool.poolHandlerComp);
        // }
    }

    destroy(){
        if(this.template && this.template.isValid){
            this.template.destroy();
        }
       
        this.Pool.clear();
    }

    clear(){
        this.Pool.clear();
    }
}