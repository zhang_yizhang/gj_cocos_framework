/** 打字机效果
@class TypeLabEffect
@author YI ZHANG
@date 2020/8/10
@desc
**/
import * as cc from "cc";
import {CCFloat} from "cc";
// import { EventData } from "../../../../Script/data/EnumData";
// import TranslationComp from "../../../../Script/Module/Util/TranslationComp";
import EventManager from "../../Event/EventManager";
import {NumFormat} from "./NumFormat";
const {ccclass,requireComponent,property} = cc._decorator;


@ccclass
@requireComponent(cc.Label)
export class TypeLabEffect extends cc.Component{
    @property(CCFloat)
    speed : number = 2;
    protected typeStr : string = "";
    protected _curString : string = "";
    private typeLength : number = 0;
    private curTypeLength : number = 0;
    private typeDelayTime : number = 0;
    private finish : boolean = true;
    private label ?: cc.Label | null;
    set string(value : string){
        this.updateTypeString(value);
        this.finish = false;
        this._cb = undefined;
    }
    set curString(value : string){
        this._curString = value;
        if(cc.isValid(this.label)){
            this.label!.string = value;
        }
    }

    protected onLoad(): void {       
        //@ts-ignore
        // if(!this.label.checkLetterFlag()){
        //     console.log('checkLetterFlag')
        //     TranslationComp.initFont(this.getComponent(cc.Label)!)
        // }
        // 
        // EventManager.emit(EventData.DIALOG_UPDATELAYOUT);
        this.string = this.getComponent(cc.Label)!.string;
        if(this.typeStr.indexOf('\n')!=-1){
            this.label!.getComponent(cc.UITransform)!.height = 200
        }else{
            this.label!.getComponent(cc.UITransform)!.height = 80
        }
        
        
    }

    protected onEnable(): void {
        this.string = this.typeStr;
    }

    get curString(){
        return this._curString
    }
    _cb?: ()=>void;

    protected __preload() {        
        this.label = this.getComponent(cc.Label)!;
        this.label.overflow = cc.Overflow.SHRINK;
        this.label.getComponent(cc.UITransform)!.width = 1000
       
    }

    setCb(cb : ()=>void){
        this._cb = cb;
    }

    updateTypeString(str : string){
        this.typeStr = str;
        this.curString = "";
        this.typeLength = str.length;
        this.curTypeLength = 0;
        this.typeDelayTime = 0;
    }

    finishType(){
        this.curString = this.typeStr;
        this.curTypeLength = this.typeLength;
    }

    lateUpdate(dt : number){
        if(this.finish){
            return;
        }
        if(this.typeLength == this.curTypeLength){
            this.finish = true;
            this._cb && this._cb();
            return;
        }
        this.typeDelayTime -= dt;
        if(this.typeDelayTime <= 0){
            this.curString += this.typeStr[this.curTypeLength++];
            this.typeDelayTime = this.speed;
            // this.node.parent?.getComponent(cc.Layout)?.updateLayout()
        }
    }

}