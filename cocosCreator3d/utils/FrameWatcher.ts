/**
@class FrameWatcher
@author YI ZHANG
@date 2021/1/25
@desc
**/
import * as cc from "cc";


const {ccclass, property} = cc._decorator;
export interface FrameWatcherData {
}
@ccclass("FrameWatcher")
export default class FrameWatcher extends cc.Component {
    sampleTime : number = 1;
    curSampleTime : number = 0;
    frame : number = 0;
    static fps : number = 60;
    static renderTime : number = 0;



    static init(){
        let node = new cc.Node();
        node.addComponent(FrameWatcher);
        cc.director.getScene()!.addChild(node);
    }


    protected update(dt: number): void {
        this.curSampleTime += dt;
        this.frame++;
        FrameWatcher.fps = this.frame / this.curSampleTime;
    }
}

