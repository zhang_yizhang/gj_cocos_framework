/** 引导遮罩
@class GuideMask
@author YI ZHANG
@date 2020/8/28
@desc
**/

import {NodeTool} from "./NodeTool";
import * as cc from "cc";
const {ccclass, property} = cc._decorator;
@ccclass
export default class GuideMask extends cc.Component {
    private _graphics!: cc.Graphics;
    private pointsList : cc.Vec2[][] = [];
    @property(cc.Node)
    maskRectFrameNode : cc.Node = null!;
    @property(cc.Vec2)
    maskBorder : cc.Vec3 = null!;
    pos: cc.Vec3 = cc.v3();
    size : cc.Size = cc.size(0,0);
    _scale : cc.Vec2 = cc.v2(1,1);
    private round: number = 5;
    private feather: number = 10;
    set scale(scale : cc.Vec2){
        this._scale = scale;
    };
    get scale(){
        return cc.v2(this._scale.x * this.viewSizeScale.x,this._scale.y * this.viewSizeScale.y)
    }
    private viewSize!: cc.Size;
    private viewSizeScale!: cc.Vec2;
    private material!: cc.Material;

    protected onLoad(): void {
        this.viewSize = cc.view.getVisibleSize();
        this.viewSizeScale = cc.v2( 1 / this.viewSize.width,1 / this.viewSize.height);
        this.material = this.getComponent(cc.RenderComponent)!.getMaterial(0)!
    }

    hideMask(){
        this.scale = cc.v2(0,0);
        this.maskRectFrameNode.active = false;
    }

    setBindNodeAndBtn(node : cc.Node,cb : ()=>void,playAnim ?: boolean,round : number = 5,feather : number = 10){
        this.maskRectFrameNode.active = true;
        this.round = round;
        this.feather = feather;
        this.pointsList = [[]];
        let data = NodeTool.getWorldPosAndSize(node);
        this.size = data.size;
        this.pos = data.pos;

        if(playAnim){
            this.playScaleAnim(cb);
        }else {
            this.scale = cc.v2(1,1);
            cb();
        }
    }

    playScaleAnim(cb : ()=>void){
        this.scale = cc.v2(3,3);
        // @ts-ignore
        cc.tween(this).to(0.4,{_scale : cc.v2(1,1)}).call(cb).start();
    }



    updateEffect(){
        let pos = cc.v2(this.pos.x * this.viewSizeScale.x,(this.viewSize.height - this.pos.y) * this.viewSizeScale.y);
        this.material.setProperty("center",pos);
        this.material.setProperty("size",cc.v2(this.size.width * this.scale.x,
            this.size.height  * this.scale.y));
        this.material.setProperty("round",this.viewSizeScale.x * this.round);
        this.material.setProperty("feather",this.viewSizeScale.x * this.feather);
        this.maskRectFrameNode.setPosition(
            this.maskRectFrameNode.parent!.getComponent(cc.UITransform)!.convertToNodeSpaceAR(this.pos));
        this.maskRectFrameNode.getComponent(cc.UITransform)!.height
            = this.size.height  * this._scale.y + this.maskBorder.y;
        this.maskRectFrameNode.getComponent(cc.UITransform)!.width
            = this.size.width  * this._scale.x + this.maskBorder.x;
    }

    protected update(dt: number): void {
        this.updateEffect();
    }


}
