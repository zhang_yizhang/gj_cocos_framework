/** 指定平台展示
@class PlatformShowNode
@author YI ZHANG
@date 2021/6/2
@desc
**/
import * as cc from "cc";
import config, {Platform} from "../../../../config/config";
import PlatformManagerBase, {AndroidEnum, CCAndroidEnum, PlatformEnum} from "../platform/PlatformManagerBase";

const {ccclass, property} = cc._decorator;

@ccclass
export default class PlatformShowNode extends cc.Component {

    @property({type : [PlatformEnum]})
    platformEnumList : Platform[] = [Platform.bytedance];
    @property({type : [CCAndroidEnum]})
    androidEnumList : AndroidEnum[] = [];

    protected onLoad(): void {
       this.node.active = this.platformEnumList.indexOf(config.platform) != -1 &&
           (config.platform != Platform.androids || this.androidEnumList.length == 0 || this.androidEnumList.indexOf(PlatformManagerBase.getAndroidChannel()) != -1);
    }

    // update (dt) {}
}
