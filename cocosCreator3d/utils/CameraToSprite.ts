/**
@class CameraToSprite
@author YI ZHANG
@date 2020/11/25
@desc
**/
import * as cc from 'cc';
import {
    _decorator,
    Component,
    GFXColorAttachment,
    GFXDepthStencilAttachment,
    GFXFormat,
    GFXTextureLayout,
    RenderTexture,
    View
} from 'cc';

const { ccclass, property } = _decorator;

@ccclass('CameraToSprite')
export class CameraToSprite extends Component {
    @property(cc.Sprite)
    private sp:cc.Sprite = null!;
    static renderTexturePool : cc.SpriteFrame[] = [];
    private spriteFrame ?: cc.SpriteFrame;


    protected onEnable(): void {
        let material = this.sp.getMaterial(0);
        this.spriteFrame = CameraToSprite.getSpriteFrame()!;
        this.getComponent(cc.Camera)!.targetTexture = this.spriteFrame.texture as cc.RenderTexture;
        this.sp.spriteFrame = this.spriteFrame;
        this.sp.node.getComponent(cc.UITransform)!.setContentSize(View.instance.getVisibleSize());
        this.sp.setMaterial(material,0);
        // @ts-ignore
        this.sp._uiMaterial = material;
    }

    protected onDisable(): void {
        if(!cc.isValid(this.spriteFrame)){
            return;
        }
        CameraToSprite.putSpriteFrame(this.spriteFrame!);
        this.spriteFrame = undefined;
        this.sp.spriteFrame = null;
    }

    static getSpriteFrame(){
        if(CameraToSprite.renderTexturePool.length){
            return CameraToSprite.renderTexturePool.pop();
        }
        let renderTexture = new RenderTexture();
        let size = View.instance.getVisibleSize();
        renderTexture.reset({
            width: size.width,
            height: size.height,
        });
        const sp = new cc.SpriteFrame();
        sp.reset({
            texture : renderTexture,
            originalSize: size,
            rect: new cc.Rect(0,0,size.width,size.height),
            offset: cc.v2(0,0),
            isRotate: false,
            borderTop: 0,
            borderLeft: 0,
            borderBottom: 0,
            borderRight: 0,
        });
        sp.texture = renderTexture;
        return  sp;
    }

    static putSpriteFrame(spriteFrame : cc.SpriteFrame){
        CameraToSprite.renderTexturePool.push(spriteFrame);
    }

    update (deltaTime: number) {
        // Your update function goes here.
    }
}
