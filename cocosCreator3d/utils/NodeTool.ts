/** 节点工具
@class NodeTool
@author YI ZHANG
@date 2020/8/10
@desc
**/
import SingletonPatternBase from "../../mvvm/SingletonPatternBase";
import * as cc from "cc";
import {geometry, math, primitives, renderer, UITransform} from "cc";
import {EngineInit} from "./engineInit";
import {AudioEngine} from "./AudioEngine";
import {PoolTool} from "./PoolTool";
let oldPosMap : {[key : string] : cc.Vec3} = {};
let zeroPos = cc.v3();
let c1 = cc.v3();
let c2 = cc.v3();
let v1 = cc.v3();
let v2 = cc.v3();
let convertPos = cc.v3();
let dirZero = cc.v2(1,0);
let dirV2 = cc.v2(1,0);
let TO_ANGLE = 180 / Math.PI;
export class NodeTool {

    /**
     * 获取node在target的位置
     * @param node
     * @param target
     * @param pos
     */
    static getPositionWithTarget(node : cc.Node,target : cc.Node,pos : cc.Vec3 = zeroPos,outPos ?: cc.Vec3){
        node.getComponent(UITransform)!.convertToWorldSpaceAR(pos,convertPos );
        return target.getComponent(UITransform)!.convertToNodeSpaceAR(convertPos ,outPos);
    }

    /**
     * 获取节点世界坐标和大小
     * @param node
     */
    static getWorldPosAndSize(node : cc.Node){
        let size = node.getComponent(UITransform)!.contentSize;
        let mat4 = new cc.Mat4();
        node.getWorldMatrix(mat4);
        let scale = new cc.Vec3();
        mat4.getScale(scale);
        let size1 = cc.size(size.width * scale.x,size.height * scale.y);
        let pos = cc.v3();
        mat4.getTranslation(pos);
        pos.x += (0.5 - node.getComponent(UITransform)!.anchorX) * size1.width;
        pos.y += (0.5 - node.getComponent(UITransform)!.anchorY) * size1.height;
        return {pos : pos,size : size1};
    }

    static get3dPositionToUI(node : cc.Node,target : cc.Node,camera : cc.Camera){
        let v = node.getWorldPosition();
        camera.convertToUINode(v,target,v);
        return v;
    }

    static getModels(node : cc.Node) : renderer.scene.Model[]{
        let models = [];
        for(let i = 0;i < node.children.length;++i){
            models.push(...this.getModels(node.children[i]));
        }
        let comp = node.getComponent(cc.MeshRenderer);
        let model = comp && comp.model;
        model && models.push(model);
        return models;
    }


    static isCollide(node1 : cc.Node,node2 : cc.Node,exData : {ignoreZ ?: boolean,scale ?: number} = {}):boolean{
        const aMin = new cc.Vec3();
        const aMax = new cc.Vec3();
        const bMin = new cc.Vec3();
        const bMax = new cc.Vec3();
        this.isCollide = (node1 : cc.Node,node2 : cc.Node,exData : {ignoreZ ?: boolean,scale ?: number})=> {
            let model1List = this.getModels(node1);
            let model2List = this.getModels(node2);
            if (!model1List.length || !model2List.length) {
                return false;
            }
            let aabb1 = model1List[0].worldBounds;
            for (let i = 1; i < model1List.length; ++i) {
                geometry.aabb.merge(aabb1!, aabb1!, model1List[i].worldBounds!);
            }
            let aabb2 = model2List[0].worldBounds;
            for (let i = 1; i < model2List.length; ++i) {
                geometry.aabb.merge(aabb2!, aabb2!, model2List[i].worldBounds!);
            }
            if(exData.scale){
                aabb1!.halfExtents.multiplyScalar(exData.scale);
                aabb2!.halfExtents.multiplyScalar(exData.scale);
            }
            cc.Vec3.subtract(aMin, aabb1!.center, aabb1!.halfExtents);
            cc.Vec3.add(aMax, aabb1!.center, aabb1!.halfExtents);
            cc.Vec3.subtract(bMin, aabb2!.center, aabb2!.halfExtents);
            cc.Vec3.add(bMax, aabb2!.center, aabb2!.halfExtents);
            return (aMin.x <= bMax.x && aMax.x >= bMin.x) &&
                (aMin.y <= bMax.y && aMax.y >= bMin.y) && (exData.ignoreZ ||
                    (aMin.z <= bMax.z && aMax.z >= bMin.z));
        };
        return this.isCollide(node1,node2,exData);
    }


    /**
     * 切换动画状态
     * @param node
     * @param isPlay
     * @param animName
     */
    static toggleAnimState(node : cc.Node,isPlay : boolean,animName ?: string){
        let anim = node.getComponent(cc.Animation);
        anim!.play(animName);
        if(!isPlay){
            anim!.stop();
        }
    }

    static forEachNode(node : cc.Node,fn : (node : cc.Node)=>boolean | void){
        if(fn(node)){
            return true;
        }
        let children = node.children;
        for(let i = 0;i < children.length;++i){
            if(this.forEachNode(children[i],fn)){
                return true;
            }
        }
        return false;
    }

    static setLayer(node : cc.Node,layer : number){
        let children = node.children;
        node.layer = layer;
        for(let i = 0;i < children.length;++i){
            this.setLayer(children[i],layer)
        }
    }

    /**
     * 播放贝塞尔飞行
     * @param endPos
     * @param startNode
     * @param duration
     * @param callback
     * @param isChangeEnd
     */
    static playFlyAnim(endPos : cc.Vec3,startNode : cc.Node, {duration =  0.7,callback,isChangeEnd = false} : {isChangeEnd :  boolean,duration : number,callback ?: ()=>void }){
        let item = startNode;
        let startPos = NodeTool.getPositionWithTarget(startNode,startNode!.parent!,undefined,convertPos);
        c1.set(endPos);
        c1.subtract(startPos);
        c2.set(startPos);
        c2.subtract(endPos);
        let angle = (Math.random() * 25 + 35) * Math.PI / 180;
        let len = 1 / 3 / Math.cos(angle);
        let dir = Math.random() > 0.5 ? 1 : -1;
        cc.Vec3.rotateZ(c1,c1.clone().multiplyScalar(len),zeroPos,(angle * dir));
        cc.Vec3.rotateZ(c2,c2.clone().multiplyScalar(len),zeroPos,(angle * dir));
        let c1Temp = c1.add(startPos).clone();
        let c2Temp = endPos.clone().subtract(c2);
        cc.tween(item).to(duration,{position : isChangeEnd ? endPos : endPos.clone()},{progress : EngineInit.bezierTo(
                c1Temp,c2Temp,isChangeEnd ? endPos : undefined)}).call(()=>{
                callback && callback();
        }).start();
    }

    static getAngleByDirVec(x : number,y : number){
        dirV2.set(x,y);
        return dirV2.signAngle(dirZero) * TO_ANGLE;
    }


    static quat1 = cc.quat();

    /**
     * 根据速度转向
     * @param node
     * @param endRotation
     * @param speed
     * @param dt
     */
    static turnBySpeed(node : cc.Node,endRotation : cc.Quat,speed : number,dt : number){
        let quat1 = node.getWorldRotation(this.quat1);
        let dis = cc.Vec3.distance(endRotation.getEulerAngles(v1),quat1.getEulerAngles(v2));
        quat1.slerp(endRotation,Math.min(speed * dt * 180 / Math.PI / dis,1));
        return quat1;
    }

    static getOpacityComp(node : cc.Node){
        return node.getComponent(cc.UIOpacity) || node.addComponent(cc.UIOpacity);
    }

    static nullCb(){

    }
    static pauseNode(node : cc.Node){
        let spineList = node.getComponentsInChildren(cc.sp.Skeleton);
        spineList.forEach((comp)=>{
            // @ts-ignore
            comp.__updateAnimation__ = comp.updateAnimation;
            comp.updateAnimation = this.nullCb;
        });
        let comps = node.getComponentsInChildren(cc.Component);
        comps.forEach((comp)=>{
            // @ts-ignore
            comp.update = comp.__update__ || comp.update;
            // @ts-ignore
            comp.lateUpdate = comp.__lateUpdate__ || comp.lateUpdate;
        });

        let animList = node.getComponentsInChildren(cc.Animation);
        animList.forEach((comp)=>{
            comp.pause();
        });
    }

    static resumeNode(node : cc.Node){
        let spineList = node.getComponentsInChildren(cc.sp.Skeleton);
        spineList.forEach((comp)=>{
            // @ts-ignore
            comp.updateAnimation =  comp.__updateAnimation__ || comp.updateAnimation;
        });
        let comps = node.getComponentsInChildren(cc.Component);
        comps.forEach((comp)=>{
            // @ts-ignore
            comp.update = comp.__update__ || comp.update;
            // @ts-ignore
            comp.lateUpdate = comp.__lateUpdate__ || comp.lateUpdate;
        });
        let animList = node.getComponentsInChildren(cc.Animation);
        animList.forEach((comp)=>{
            comp.resume();
        });
    }
}