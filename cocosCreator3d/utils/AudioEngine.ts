/** 声音控制器
 @class AudioEngine
 @author YI ZHANG
 @date 2020/8/6
 @desc
 **/
 import SingletonPatternBase from "../../mvvm/SingletonPatternBase";
 import * as cc from "cc";
 import CCLoadItem from "../mvvm/ui/CCLoadItem";
 import config, { Platform } from "../../../../config/config";
 import { QQAuido } from "./qqAudio";
 import { Component } from "cc";
 import EventManager from "../../Event/EventManager";
 type AudioComp = QQAuido | cc.AudioSource;
 export class AudioEngine extends SingletonPatternBase {
  
     isMusic: boolean = true;
     isSound: boolean = true;
     loadMusicId: number = 1;
     music?: AudioComp;
     effect!: AudioComp;
     effectPlayTimeMap: { [key: string]: number } = {};
     private loadEffectId: number = 1;
     private musicVolume!: number;
     private isLoop!: boolean;
     private musicId!: number;
     private node: cc.Node = null!;
     private musicNode!: cc.Node;
     private effectNode!: cc.Node;
     playEffectTime: number = 0;
     // @ts-ignore
     static audioComp: new (...args: any[]) => AudioComp = config.platform == Platform.qq && window.qq ? QQAuido : cc.AudioSource;
     // loopEffects: cc.AudioSource[] = [];
     // loopEftNodes: cc.Node[] = [];
 
     static getInstance<T extends {}>(this: new () => T): T {
         if (!(<any>this).instance) {
             (<any>this).instance = new this();
             let node = new cc.Node("musicManager");
             (<any>this).instance.node = node;
             cc.director.getScene()!.addChild(node);
             let node1 = new cc.Node("music");
             let node2 = new cc.Node("effect");
             node.addChild(node1);
             node.addChild(node2);
             (<any>this).instance.musicNode = node1;
             (<any>this).instance.effectNode = node2;
             (<any>this).instance.effect = node2.addComponent(AudioEngine.audioComp);
             EventManager.on(cc.Game.EVENT_SHOW, () => {
 
                 (<any>this).instance.resumeAll();
 
 
             });
             EventManager.on(cc.Game.EVENT_HIDE, () => {
                 (<any>this).instance.pauseAll();
             });
         }
         return (<any>this).instance;
     }
 
     /**
      * 播放音乐
      * @param bgMusic
      * @param {Number} volume 音量
      * @param isLoop
      * @param cb
      */
     playMusic(bgMusic: cc.AudioClip, volume: number = 1, isLoop: boolean = true, cb?: () => void) {
         if (volume === null) {
             volume = 1;
         }
         console.log('_objFlags-bgMusic:',bgMusic._uuid)
         if(this.music){
            console.log('_objFlags-music:',   this.music.clip?._uuid)
         
         }
         if (this.music && this.music.clip && bgMusic._uuid == this.music.clip!._uuid && this.isLoop == isLoop && volume == this.musicVolume) {
            if(AudioEngine.getInstance().isMusicPlaying()){
                return;
            }
             
         }
        //  console.log('_objFlagssss:',bgMusic._objFlags,this.music&&this.music.clip!._objFlags)
         if ( this.music && cc.isValid(this.music.node) ) {
             this.music.node.destroy();
         }
         this.loadMusicId++;
         this.musicVolume = volume;
         this.isLoop = isLoop;
         let node = new cc.Node();
         let music = node.addComponent(AudioEngine.audioComp);
         music.clip = bgMusic;
         music.playOnAwake = true;
         music.loop = isLoop;
         music.volume = volume;
         this.musicNode.addChild(node);
         this.music = music;
     }

     removeMusic(){
        if ( this.music && cc.isValid(this.music.node) ) {
            this.music.node.destroy();
            this.music=null!
        }
     }

     isMusicPlaying() {
        return this.music && this.music.playing
    }
 
     /**
      * 设置音乐音量
      * @param volume
      */
     setMusicVolume(volume: number = 1) {
         if (this.music) {
             this.music.volume = volume;
         }
     }
 
     /**
      * 根据资源路径播放音乐
      * @param url
      * @param loop
      * @param volume
      */
     playMusicByUrl(url: string, loop: boolean = true, volume: number = 1) {
         let id = ++this.loadMusicId;
         this._playMusicByUrl(url, loop, volume, id);
     }
 
     playMusicByUrlRandom(urls: string[], loop: boolean = true, volume: number = 1){
        console.log('playMusicByUrlRandom:',urls)
         this.playMusicByUrl(urls[Math.floor(Math.random()*urls.length)], loop, volume)
     }
 
     private async _playMusicByUrl(url: string, loop: boolean = true, volume: number = 1, id: number) {
         console.log("playMusic", this.setUrlToName(url));
         let rs = await CCLoadItem.load(url, cc.AudioClip);
         if (this.loadMusicId != id || !rs) {
             return;
         }
         this.playMusic(rs, volume, loop);
         console.log("playMusic1", url);
     }
 
     /**
      * 播放音效
      * @param effect
      * @param {Number} volume 音量
      * @param isLoop
      */
     playEffect(effect: cc.AudioClip, volume: number = 1, isLoop = false) {
         this.playEffectTime = new Date().getTime();
         this.loadEffectId++;
         if (!this.isSound) {
             return;
         }
         if (isLoop) {
             this.playEftLoop(effect, effect.name)
         } else {
             this.effect.playOneShot(effect, volume);
         }
 
     }
 
     setUrlToName(url: string) {
         let tmp = url.split("/");
         let name = "";
         for (let i = 0; i < tmp.length; i++) {
             name += tmp[i];
         }
         return name;
     }
 
     /**
      *
      * @param effect
      * @param volume
      */
     private playEftLoop(effect: cc.AudioClip, url: string, volume: number = 1) {
         let name = url.replace("/", "");
         let node = this.effectNode.getChildByName(name);
         let music: AudioComp;
         if (node) {
             music = node.getComponent(AudioEngine.audioComp)!;
         } else {
             node = new cc.Node();
             node.name = name;
             music = node.addComponent(AudioEngine.audioComp);
             this.effectNode.addChild(node);
             // this.loopEffects.push(music);
             // let id = this.loopEffects.length - 1;
             // this.loopEftNodes[id] = node;
         }
         music.clip = effect;
         music.playOnAwake = true;
         music.loop = true;
         music.volume = volume;
     }
 
     stopEftLoop(url: string | cc.AudioClip) {
         let node
         if (typeof url === 'string') {
             node = this.effectNode.getChildByName(url.replace("/", ""));
         } else {
             node = this.effectNode.getChildByName(url.name)
         }
 
         if (node && cc.isValid(node)) {
             node.getComponent(AudioEngine.audioComp)!.stop();
             node.destroy();
         }
         // if (!rootFlag) {
         //     this.loopEftNodes[id].destroy();
         //     this.loopEffects.splice(id,1);
         // }
         // this.loopEftNodes.splice(id,1);
     }
 
     /**
      * 设置音效音量
      * @param id
      * @param volume
      */
     setEffectVolume(id: number, volume: number = 1) {
         this.effect.volume = volume;
     }
 
     /**
      * 根据资源路径播放音效
      * @param url
      * @param loop
      * @param volume
      */
     playEffectByUrl(url: string, loop: boolean = false, volume: number = 1) {
         this.playEffectTime = new Date().getTime();
         this._playEffectByUrl(url, loop, volume);
     }
     playEffectByUrlRandom(urls: string[], loop: boolean = true, volume: number = 1){
         this.playEffectByUrl(urls[Math.floor(Math.random()*urls.length)], loop, volume)
     }
     /**
      * 根据资源路径播放音效(带间隔)
      * @param url
      * @param interval
      * @param loop
      * @param volume
      */
     playEffectByUrlWithInterval(url: string, interval: number, loop: boolean = false, volume: number = 1) {
         let now = Date.now();
         if (this.effectPlayTimeMap[url] && now - interval * 1000 < this.effectPlayTimeMap[url]) {
             return;
         }
         this.effectPlayTimeMap[url] = now;
         this.playEffectByUrl(url, loop, volume);
     }
 
     private async _playEffectByUrl(url: string, loop: boolean = true, volume: number = 1) {
         let rs = await CCLoadItem.load(url, cc.AudioClip);
         if (!this.isSound || !rs) {
             return;
         }
         if (loop) {
             this.playEftLoop(rs, url, volume);
         } else {
             this.playEffect(rs, volume, loop);
         }
     }
 
     /**
      * 暂停播放
      */
     pauseMusic() {
         if (this.music) {
             this.music.pause();
         }
     }
 
     /**
      * 停止
      */
     stopMusic() {
         if (this.music) {
             this.music.stop();
         }
     }
 
     /**
      * 获取音乐时间
      */
     getMusicCurTime() {
         if (this.music && this.music.clip) {
             return this.music.currentTime;
         }
         return 0;
     }
 
     setMusicCurTime(time: number) {
         if (this.music && this.music.clip) {
             this.music.currentTime = time;
         }
     }
     /**
      * 恢复播放
      */
     resumeMusic() {
         if (!this.isMusic || !this.music || !cc.isValid(this.music.clip)) return;
         this.music.play();
     }
 
     rePlayMusic() {
         if (!this.isMusic || !this.music || !cc.isValid(this.music.clip)) return;
         this.music.play();
     }
 
     /**
      * 停止所有
      */
     stopAll() {
         this.stopMusic();
         this.stopAllEffects();
     }
 
     /**
      * 暂停音效
      */
     pauseAll() {
         this.node.active = false;
     }
 
     /**
      * 恢复音效
      */
     resumeAll() {
         if (this.music && (this.music!.state != cc.AudioSource.AudioState.STOPPED)) {
             this.node.active = true;
         }
     }
 
     /**
      * 停止所有音效
      */
     stopAllEffects() {
         this.effect.stop();
     }
 
     stopEffect(url: string | cc.AudioClip) {
         this.stopEftLoop(url);
     }
 
     /**
      * 开关音乐
      */
     switchMusic(isMusic: boolean) {
         if (isMusic == this.isMusic) {
             return;
         }
         this.isMusic = isMusic;
         this.musicNode.active = isMusic;
     }
 
     /**
      * 开关音效
      */
     switchEffect(isSound: boolean) {
         if (isSound == this.isSound) {
             return;
         }
         this.effectNode.active = isSound;
         this.isSound = isSound;
     }
 
 }
 