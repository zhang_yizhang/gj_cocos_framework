/** 数据处理(可控制随机)
 @class DataUtilBySeed
 @author YI ZHANG
 @date 2020/7/30
 @desc
 **/

export class DataUtilBySeed{
    /**
     * 从min到max的所有值（不包括max）随机一个
     * @param min
     * @param max
     */

    static random(min : number, max :number,randomFunc ?: ()=>number) : number{
        return Math.floor(this.seedRandom() * (max - min)) + min
    }

    public static seed:number = 5;
    /**种子随机数*/
    public static seedRandom(){
        this.seed = (this.seed * 9301 + 49297) % 233280;
        return this.seed / 233280.0;
    }

    /**种子随机数*/
    public static getSeed(){
        this.seed = (this.seed * 9301 + 49297) % 233280;
        return this.seed;
    }

    public static setSeed(seed : number){
        this.seed = seed;
        this.seedRandom();
    }


    /**
     * 随机获取list中的一个item，weight为权重列表
     * @param list
     * @param weight
     */
    static getRandomItem<T>(list : T[],...weight : number[]) : T | undefined{
        if(list.length == 0){
            return ;
        }
        if(weight.length > 0){
            let weightList = [];
            let curWeight = 0;
            for(let i = 0;i < weight.length;++i){
                curWeight += weight[i];
                weightList.push(curWeight);
            }
            let randomWeight = this.random(0,curWeight);
            for(let i = 0;i < weightList.length;++i){
                if(randomWeight < weightList[i]){
                    return list[i];
                }
            }
        }
        let index = this.random(0,list.length);
        let item = list[index];
        return item;
    }

    /**
     * 随机移除list中的一个item并返回
     * @param list
     */
    static removeRandomItem<T>(list : T[]) : T | undefined{
        if(list.length == 0){
            return ;
        }
        let index = this.random(0,list.length);
        let item = list[index];
        list.splice(index,1);
        return item;
    }

    /**
     * 打乱list
     * @param list
     */
    static randomList<T>(list : T[]) : T[]{
        for(let i = 0;i < list.length - 1;++i){
            let index = this.random(i + 1,list.length);
            let temp = list[index];
            list[index] = list[i];
            list[i] = temp;
        }
        return list;
    }

    /**
     * 设置位标识
     * @param oldFlag
     * @param flag
     * @param newFlag
     */
    static setFlag(oldFlag : number,flag : number,newFlag : number){
        return oldFlag & (~flag) | newFlag
    }

    /**
     * 清除位标识
     * @param oldFlag
     * @param clearFlag
     */
    static clearFlag(oldFlag : number,clearFlag : number){
        return oldFlag & (~clearFlag);
    }

    /**
     * 列表转为map
     * @param list
     * @param key
     */
    static listToMap<T>(list : T[],key : string) : {[key : string] : T}{
        let map : {[key : number] : T} = {};
        for(let i = 0;i < list.length;++i){
            // @ts-ignore
            map[list[i][key]] = list[i];
        }
        return map;
    }

    static MapForEach<T>(map : T,fn : (value : any,key : keyof T)=>(boolean | void)){
        let keys = Object.keys(map);
        for(let i = 0;i < keys.length;++i){
            // @ts-ignore
            if(fn(map[keys[i]],keys[i])){
                return;
            }
        }
    }
}