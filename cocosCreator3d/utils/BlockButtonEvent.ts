/**
 @class BlockButtonEvent
 @author YI ZHANG
 @date 2021/5/17
 @desc
 **/
import { _decorator, Component, Node } from 'cc';
import * as cc from "cc";
const { ccclass, property } = _decorator;

@ccclass('BlockButtonEvent')
export class BlockButtonEvent extends Component {
    /* class member could be defined like this */
    // dummy = '';

    /* use `property` decorator if your want the member to be serializable */
    // @property
    // serializableDummy = 0;

    onLoad () {
    }

    // update (deltaTime: number) {
    //     // Your update function goes here.
    // }
}
