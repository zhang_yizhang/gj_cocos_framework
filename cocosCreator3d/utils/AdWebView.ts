import { _decorator, Component, Node, WebView } from 'cc';
import { ReportIntersState } from '../../../../Script/data/ReportData';
import { PlatformManager } from '../../../../Script/Manager/PlatformManager';
import EventManager from '../../Event/EventManager';
import { PlatformManagerEvent, VibrateType } from '../platform/PlatformManagerBase';
const { ccclass, property ,requireComponent} = _decorator;

enum webViewCall {
    showBanner = 'showBanner',
    hideBanner = 'hideBanner',
    showInters = 'showInters',
    showVideo = 'showVideo',
    getVideoFlag = 'getVideoFlag',
    getIntersFlag = 'getIntersFlag',
    getChannelName = 'getChannelName',
    getAndroidChannel = 'getAndroidChannel',
    phoneVibrateShort = 'phoneVibrateShort',
    phoneVibrateLong = 'phoneVibrateLong',
    exitWeb = 'exitWeb',
    showWebView='showWebView',
}

export enum webViewEvent{
    EXIT_WEBGAME='EXIT_WEBGAME',
    LOADED_WEBGAME='LOADED_WEBGAME',
}

@ccclass('AdWebView')
@requireComponent(WebView)
export class AdWebView extends Component {    
    scheme = "adkey";
    webview!:WebView

    protected onLoad() { 
        this.webview = this.node.getComponent(WebView)!
        this.node.on(WebView.EventType.ERROR, this.webviewLoadErr, this);
        this.node.on(WebView.EventType.LOADED, this.webViewLoaded, this);
        this.node.on(WebView.EventType.LOADING, this.webViewLoading, this);
        this.node.on(WebView.EventType.NONE, this.webviewLoadNone, this);
        EventManager.on(PlatformManagerEvent.NO_VIDEO_AD,this.showNoVideoTip,this);
        console.log('AdWebView-onload')

        let jsCallback=(target: any, url: string)=> {
            console.log('webCall', url)
            // 这里的返回值是内部页面的 URL 数值，需要自行解析自己需要的数据。
            let str = url.replace(this.scheme + '://', ''); // str === 'a=1&b=2'
            switch (str) {
                case webViewCall.showBanner: {
                    PlatformManager.getInstance().showBannerAD(true)
                    break
                }
                case webViewCall.hideBanner: {
                    PlatformManager.getInstance().showBannerAD(false)
                    break
                }
                // case webViewCall.showVideo: {
                //     PlatformManager.getInstance().showVideoAD((success: boolean) => {
                //         this.webview.evaluateJS(`webViewVideoCallBack(${success})`);
                //     }, ReportVideoScene.webViewGame)
                //     break
                // }
                // case webViewCall.showInters: {
                //     PlatformManager.getInstance().showInters(() => { }, ReportScene.webView)
                //     break
                // }
                case webViewCall.getIntersFlag: {
                    let flag =  PlatformManager.getInstance().getIntersFlag()
                    this.webview.evaluateJS(`webViewGetIntersFlag(${flag})`);
                    break
                }
                case webViewCall.getVideoFlag: {
                    let flag =  PlatformManager.getInstance().getVideoFlag()
                    this.webview.evaluateJS(`webViewGetVideoFlag(${flag})`);
                    break
                }
                case webViewCall.getChannelName: {
                    let data=  PlatformManager.getChannelName()
                    this.webview.evaluateJS(`webViewGetChannelName(${data})`);
                    break
                }
                case webViewCall.getAndroidChannel: {
                    let data=  PlatformManager.getAndroidChannel()
                    this.webview.evaluateJS(`webViewGetAndroidChannel(${data})`);
                    break
                }
                case webViewCall.phoneVibrateShort: {
                    PlatformManager.getInstance().phoneVibrate(VibrateType.SHORT)
                    break
                }
                case webViewCall.phoneVibrateLong: {
                    PlatformManager.getInstance().phoneVibrate(VibrateType.LONG)
                    break
                }
                // case webViewCall.exitWeb: {
                //     EventManager.emit(EnumEvent.WEBVIEW_CLICK_RETURN_HOME);
                //     console.log('webViewCall.exitWeb')
                //     break
                // }
                default:{
                    EventManager.emit('WEBVIEW_JSCALL',str)
                }    
            }
        }
        this.webview.setJavascriptInterfaceScheme(this.scheme);
        //@ts-ignore
        this.webview.setOnJSCallback(jsCallback.bind(this));
    }
    showNoVideoTip(){
        this.webview.evaluateJS(`webViewNoVideoTip()`);
    }
    webviewLoadErr(data:any){
        console.log('webviewLoadErr',data)
    }
    webviewLoadNone(data:any){
        console.log('webviewLoadNone',data)
    }
    webViewLoaded(data:any): void {
        console.log('webViewLoaded',data)
        EventManager.emit(webViewEvent.LOADED_WEBGAME);
    }
    webViewLoading(data:any): void {
        console.log('webViewLoading',data)
    }
    webViewAudioStopAll(){
        this.webview.evaluateJS(`webViewAudioStopAll()`);
    }
    webViewAudioResumeAll(){
        this.webview.evaluateJS(`webViewAudioResumeAll()`);
    }
    update(deltaTime: number) {

    }
}

