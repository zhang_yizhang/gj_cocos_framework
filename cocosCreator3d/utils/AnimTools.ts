import * as cc from 'cc';
import { _decorator, Component, Node } from 'cc';
import { NodeTool } from "./NodeTool";
import { EngineInit } from "./engineInit";
import { AudioEngine } from "./AudioEngine";
import { PoolTool } from "./PoolTool";
const { ccclass, property } = _decorator;
let zeroPos = cc.v3();
let c1 = cc.v3();
let c2 = cc.v3();
let convertPos = cc.v3();
export class AnimTools {

    random() {
        //取0-1的随机整数
        let randomNum = Math.floor(Math.random() * 2);

    }

    /**
     * 播放贝塞尔飞行
     * @param endPos
     * @param startNode
     * @param duration
     * @param callback
     */
    static playFlyAnim(endPos: cc.Vec3, startNode: cc.Node, { duration = 0.7, callback }: { duration: number, callback?: () => void }) {
        let item = startNode;
        let startPos = NodeTool.getPositionWithTarget(startNode, startNode!.parent!, undefined, convertPos);
        c1.set(endPos);
        c1.subtract(startPos);
        c2.set(startPos);
        c2.subtract(endPos);
        let angle = (Math.random() * 25 + 35) * Math.PI / 180;
        let len = 1 / 3 / Math.cos(angle);
        let dir = Math.random() > 0.5 ? 1 : -1;
        cc.Vec3.rotateZ(c1, c1.clone().multiplyScalar(len), zeroPos, (angle * dir));
        cc.Vec3.rotateZ(c2, c2.clone().multiplyScalar(len), zeroPos, (angle * dir));
        let c1Temp = c1.add(startPos).clone();
        let c2Temp = c2.subtract(startPos).clone();
        cc.tween(item).to(duration, { position: endPos.clone() }, {
            progress: EngineInit.bezierTo(
                c1Temp, c2Temp)
        }).call(() => {
            callback && callback();
        }).start();
    }

    static playCoinFlyAnim(pool: PoolTool, panel: cc.Node, startNode: cc.Node | cc.Vec3, endNode: cc.Node | cc.Vec3, callback: (index: number) => void,
        { isReverse = false, flyNum = 5, duration = 0.7, offset = cc.v3() }) {
        let endTargetPos: cc.Vec3;
        if (endNode instanceof cc.Node) {
            endTargetPos = NodeTool.getPositionWithTarget(endNode, panel).add(offset);
        }
        else {
            endTargetPos = panel.getComponent(cc.UITransform)!.convertToNodeSpaceAR(endNode);
        }
        let startTargetPos: cc.Vec3;
        if (startNode instanceof cc.Node) {
            startTargetPos = NodeTool.getPositionWithTarget(startNode, panel);
        }
        else {
            startTargetPos = panel.getComponent(cc.UITransform)!.convertToNodeSpaceAR(startNode);
        }
        if (isReverse) {
            let temp = endTargetPos;
            endTargetPos = startTargetPos;
            startTargetPos = temp;
        }
        let startPos = startTargetPos.clone();
        let endPos = endTargetPos.clone();
        for (let i = 0; i < flyNum; ++i) {
            setTimeout(() => {
                let item = pool.get(panel);
                startPos.set(startTargetPos);
                endPos.set(endTargetPos);
                let subPos = cc.v3(endPos).subtract(startPos);
                let subPos2 = cc.v3(startPos).subtract(endPos);
                let c1 = cc.v3();
                let c2 = cc.v3();
                let angle = Math.random() * 25 + 20;
                let len = 1 / 3 * Math.cos(angle);
                let dir = Math.random() > 0.5 ? 1 : -1;
                cc.Vec3.rotateZ(c1, subPos.multiplyScalar(len), cc.v3(), angle * dir);
                cc.Vec3.rotateZ(c2, subPos2.multiplyScalar(len), cc.v3(), angle * dir);
                item.setPosition(startPos);
                cc.tween(item).to(duration, { position: endPos }, {
                    progress: EngineInit.bezierTo(
                        c2.add(startPos), c1.add(endPos))
                }).call(() => {
                    pool.put(item);
                    callback(i + 1);
                }).start();
            }, 100 * i);

        }
    }

    static playCoinFlyAnim2(pool: PoolTool, panel: cc.Node, startNode: cc.Node | cc.Vec3, endNode: cc.Node | cc.Vec3, callback: (index: number) => void,
        { isReverse = false, flyNum = 5, duration = 0.6, offset = cc.v3() }) {
        let endTargetPos: cc.Vec3;
        if (endNode instanceof cc.Node) {
            endTargetPos = NodeTool.getPositionWithTarget(endNode, panel).add(offset);
        }
        else {
            endTargetPos = panel.getComponent(cc.UITransform)!.convertToNodeSpaceAR(endNode);
        }
        let startTargetPos: cc.Vec3;
        if (startNode instanceof cc.Node) {
            startTargetPos = NodeTool.getPositionWithTarget(startNode, panel);
        }
        else {
            startTargetPos = panel.getComponent(cc.UITransform)!.convertToNodeSpaceAR(startNode);
        }
        if (isReverse) {
            let temp = endTargetPos;
            endTargetPos = startTargetPos;
            startTargetPos = temp;
        }
        let startPos = startTargetPos.clone();
        let endPos = endTargetPos.clone();
        for (let i = 0; i < flyNum; ++i) {
            setTimeout(() => {
                let item = pool.get(panel);
                startPos.set(startTargetPos);
                let startMovePos = startPos.clone().add(cc.v3(Math.random() * 160/flyNum* i - 80/flyNum* i, Math.random() * -80/flyNum*i , 0))
              
                endPos.set(endTargetPos);
                let dir = startMovePos.x < startPos.x ? -1 : 1;
                let subPos = dir==-1?cc.v3(endPos).subtract(startMovePos):cc.v3(endPos).add(startMovePos);
                let subPos2 =dir==-1? cc.v3(startMovePos).subtract(endPos):cc.v3(startMovePos).add(endPos);
                let c1 = cc.v3();
                let c2 = cc.v3();
                // let angle = Math.random() * 25 + 20;
                let angle =Math.abs( startMovePos.x-startPos.x)/200*45
                let len =  Math.cos(angle);

                cc.Vec3.rotateZ(c1, subPos.multiplyScalar(len*0.4), cc.v3(),0.3* angle * dir);
                cc.Vec3.rotateZ(c2, subPos2.multiplyScalar(len*0.4), cc.v3(), 0.3*angle * dir);
                item.setPosition(startMovePos);
                let movePos = (startMovePos.clone()).subtract(startPos).multiplyScalar(2)
                // let scale = item.getScale()
                // item.setScale(cc.v3())              
                cc.tween(item)
                    .by(0.25, { position: movePos},{easing:cc.easing.quadOut})
                    // .delay(0.05)
                    // .to(0.15,{scale:scale})
                    .to(duration, { position: endPos }, {
                        progress: EngineInit.bezierTo(
                            c2.add(startMovePos), c1.add(endPos)),
                        // easing: cc.easing.backIn

                    })
                    .delay(0.1)
                    .call(() => {
                        pool.put(item);
                        callback(i + 1);
                    }).start();
            }, 10* i);

        }

    }

    static async openAnim(node: cc.Node, duration = 0.5) {
        await new Promise(resolve => {
            cc.tween(node).set({ scale: cc.v3() })
                .to(duration, { scale: cc.v3(1, 1, 1) }, { easing: cc.easing.backOut })
                .call(() => {
                    resolve(true);
                })
                .start();
        });
    }

    static async closeAnim(node: cc.Node, duration = 0.5) {
        await new Promise(resolve => {
            cc.tween(node).to(duration, { scale: cc.v3(0, 0, 0) }, { easing: cc.easing.backIn })
                .call(() => {
                    resolve(true);
                })
                .start();
        });
    }

    static async openAnimFakeWin10(contentNode: cc.Node, starWorldPos: cc.Vec3, endWorldPos: cc.Vec3, BgOpactiy?: cc.UIOpacity,duration:number=0.2) {
        await new Promise(resolve => {
            if (BgOpactiy) {
               cc.tween(BgOpactiy).set({ opacity: 1}).to(duration, { opacity: 255 }).start()
            }
            if (contentNode.getComponent(cc.UIOpacity)) {
                cc.tween(contentNode.getComponent(cc.UIOpacity)!).set({ opacity: 1 }).to(duration, { opacity: 255 }, { easing: cc.easing.quadIn }).start()
            }
            let contentScale = contentNode.getScale()
            cc.tween(contentNode).set({ scale: cc.v3(0, 0, 1), worldPosition: starWorldPos })
                .to(duration, { scale: contentScale, worldPosition: endWorldPos }, { easing: cc.easing.backOut })
                .call(() => {
                    resolve(true);
                })
                .start();
        });
    }


    static async closeAnimFakeWin10(contentNode: cc.Node, worldPos: cc.Vec3, BgOpactiy?: cc.UIOpacity,duration:number=0.2) {
        await new Promise(resolve => {
            if (BgOpactiy) {
                
               cc.tween(BgOpactiy).to(duration+0.1, { opacity: 1 }).start()
            }
            if (contentNode.getComponent(cc.UIOpacity)) {
                cc.tween(contentNode.getComponent(cc.UIOpacity)!).to(duration+0.1, { opacity: 1 }, { easing: cc.easing.quadOut }).start()
            }

            cc.tween(contentNode)
                .to(duration, { scale: cc.v3(0, 0, 1), worldPosition: worldPos }, { easing: cc.easing.backIn })
                .call(() => {
                    resolve(true);
                })
                .start();
        });
    }

    
    static async openAnimFakeWin10Quad(contentNode: cc.Node, starWorldPos: cc.Vec3, endWorldPos: cc.Vec3, BgOpactiy?: cc.UIOpacity,duration:number=0.2) {
        await new Promise(resolve => {
            if (BgOpactiy) {                               
               cc.tween(BgOpactiy).set({ opacity: 1 }).to(duration, { opacity: 255 }).start()
            }
            if (contentNode.getComponent(cc.UIOpacity)) {
                cc.tween(contentNode.getComponent(cc.UIOpacity)!).set({ opacity: 1 }).to(duration, { opacity: 255 }, { easing: cc.easing.quadIn }).start()
            }
            let contentScale = contentNode.getScale()
            cc.tween(contentNode).set({ scale: cc.v3(0, 0, 1), worldPosition: starWorldPos })
                .to(duration, { scale: contentScale, worldPosition: endWorldPos }, { easing: cc.easing.quadOut })
                .call(() => {
                    resolve(true);
                })
                .start();
        });
    }

}
