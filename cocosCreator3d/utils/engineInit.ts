/** 初始化自定义
 @class engineInit
 @author YI ZHANG
 @date 2020/8/7
 @desc
 **/

import * as cc from "cc";
import { bezier } from "cc";
import EventManager from "../../Event/EventManager";
import config, { Platform } from "../../../../config/config";
import { BlockButtonEvent } from "./BlockButtonEvent";

export enum CustomEngineEvent {
    MOVE_CONTENT = "c_moveContent",
    EVENT_BEFORE_DRAW1 = "EVENT_BEFORE_DRAW1",
    EVENT_BEFORE_DRAW2 = "EVENT_BEFORE_DRAW2",
    EVENT_BEFORE_DRAW3 = "EVENT_BEFORE_DRAW3",
    CLICK_BUTTON = "CLICK_BUTTON",
}
const tweenKeyList = ["x", "y", "z", "w"];
export class EngineInit {
    static isFristOp: boolean = true;
    static init() {
        let instance = new EngineInit();
        instance.initSDK();
        instance.setUseBakedAnimation();
        cc.director.on(cc.Director.EVENT_BEFORE_DRAW, () => {//发出渲染前事件供自定义组件更新脏视图
            EventManager.emit(CustomEngineEvent.EVENT_BEFORE_DRAW1);
            EventManager.emit(CustomEngineEvent.EVENT_BEFORE_DRAW2);
            EventManager.emit(CustomEngineEvent.EVENT_BEFORE_DRAW3);
        });
        cc.game.on(cc.Game.EVENT_SHOW, () => {
            EventManager.emit(cc.Game.EVENT_SHOW);
        });
        cc.game.on(cc.Game.EVENT_HIDE, () => {
            EventManager.emit(cc.Game.EVENT_HIDE);
        });
        let list: cc.SafeArea[] = [];
        let oldFunc = cc.SafeArea.prototype.onEnable;
        cc.SafeArea.prototype.onEnable = function () {
            oldFunc.call(this);

            if (list.indexOf(this) == -1) {
                list.push(this);
            }
        };
        // @ts-ignore
        cc.SafeArea.updateAll = function () {
            for (let i = 0; i < list.length; ++i) {
                if (!cc.isValid(list[i])) {
                    list.splice(i--, 1);
                    continue;
                }
                list[i].updateArea();
            }
        };


        // @ts-ignore
        let onLoad = cc.Button.prototype.onLoad;
        let eventCall = () => {
            EventManager.emit(CustomEngineEvent.CLICK_BUTTON);
        };
        // @ts-ignore
        cc.Button.prototype.onLoad = function () {
            onLoad && onLoad.call(this);
            if (!this.node.getComponent(BlockButtonEvent)) {
                this.node.on(cc.Button.EventType.CLICK, eventCall);
            }

        };

        let oldFunc3 = cc.sp.Skeleton.prototype.setAnimation;
        cc.sp.Skeleton.prototype.setAnimation = function (trackIndex: number, name: string, loop: boolean) {
            if (name === null || name === undefined) {
                return;
            }
            return oldFunc3.call(this, trackIndex, name.toString(), loop);
        };

        let oldFunc4 = cc.sp.Skeleton.prototype.setMix;
        cc.sp.Skeleton.prototype.setMix = function (fromAnimation: string, toAnimation: string, duration: number) {
            if (fromAnimation === null || fromAnimation === undefined || toAnimation === null || toAnimation === undefined) {
                return;
            }
            return oldFunc4.call(this, fromAnimation.toString(), toAnimation.toString(), duration);
        };

        let oldFunc5 = cc.sp.Skeleton.prototype.addAnimation;
        cc.sp.Skeleton.prototype.addAnimation = function (trackIndex: number, name: string, loop: boolean, delay?: number) {
            if (name === null || name === undefined) {
                return;
            }
            return oldFunc5.call(this, trackIndex, name.toString(), loop, delay);
        };
    }

    static downloaderMaxConcurrency(num: number) {
        //加载包上限
        cc.assetManager.downloader.maxConcurrency = num;
    }

    /**
     * sdk 变更修复
     */
    initSDK() {
        // @ts-ignore
        cc.assetManager.loadImage = cc.assetManager.loadRemote;
        // @ts-ignore
        cc.AssetManager.loadImage = cc.assetManager.loadImage.bind(cc.assetManager);
        // @ts-ignore
        cc.Event.prototype.stopPropagation = function () {
            this.propagationStopped = true;
        }
    }

    setUseBakedAnimation() {
        if (!cc.SkeletalAnimation) {
            return;
        }
        let oldFunc = cc.SkeletalAnimation.prototype.onLoad;
        cc.SkeletalAnimation.prototype.onLoad = function () {
            oldFunc && oldFunc.call(this);
            if (config.platform == Platform.oppo || config.platform == Platform.vivo) {
                this.useBakedAnimation = false;
            }
        }
    }

    static bezierTo(c1: cc.Vec3, c2: cc.Vec3, endPos?: cc.Vec3) {
        let index = 0;
        return function (start: number, end: number, current: number, t: number) {
            let key = tweenKeyList[(index++) % 3];
            return bezier(start, (<any>c1)[key], (<any>c2)[key], endPos ? (<any>endPos)[key] : end, t);
        }
    }

    static bezierBy(c1: cc.Vec3, c2: cc.Vec3) {
        let index = 0;
        return function (start: number, end: number, current: number, t: number) {
            let sx = start;
            let key = tweenKeyList[(index++) % 3];
            return bezier(sx, (<any>c1)[key] + sx, (<any>c2)[key] + sx, end, t);
        };
    }

    static slerpTo(q1: cc.Quat, q2: cc.Quat) {
        let index = 0;
        let temp = cc.quat();
        return function (start: number, end: number, current: number, t: number) {
            let key = tweenKeyList[(index++) % 4];
            let value: any = temp.set(q1).slerp(q2, t);
            return value[key];
        };
    }

    static jumpTo(height = 15, endPos?: cc.Vec3) {
        let index = 0;
        return (start: number, end: number, current: number, ratio: number) => {
            if (endPos) {
                end = index % 3 == 0 ? endPos.x : (index % 3 == 1 ? endPos.y : endPos.z);
            }
            if ((index) % 3 == 1) {
                index++;
                let y = (height + Math.abs(end - start)) * 3 * ratio * (1 - ratio);
                return start + (end - start) * ratio + y;
            } else {
                index++;
                return start + (end - start) * ratio
            }

        }
    }

    static initSafeArea(dataEx: { top: number, bottom: number }) {
        let cb = cc.sys.getSafeAreaRect;
        // @ts-ignore
        cc.sys.getSafeAreaRect = function () {
            let data: any;
            try {
                data = cb();
                if (data.height * 2 < cc.view.getVisibleSize().width) {
                    throw new Error("size error");
                }
            }
            catch (e) {
                let size = cc.view.getVisibleSize();
                data.height = size.height;
                data.width = size.width;
                data.x = 0;
                data.y = 0;
            }
            if (dataEx.bottom > 80) {
                data.height += data.y;
                data.y = 0;
            }
            data.y += dataEx.bottom;
            data.height -= dataEx.bottom + dataEx.top;
            return data;
        }
    }
}

let map = new Map<string, number>();
let oldFunc = cc.Node.prototype._onPreDestroy;
let oldFunc2 = cc.Node.prototype._init;
let list: cc.Node[] = [];
cc.Node.prototype._init = function (...args) {
    oldFunc2.call(this, ...args);
    if (this.inited) {
        return;
    }
    this.inited = true;
    if (!map.has(this.name)) {
        map.set(this.name, 0);
    }
    map.set(this.name, map.get(this.name)! + 1);
    // if(this.name == "New Node"){
    list.push(this);
    // }
}
cc.Node.prototype._onPreDestroy = function (...args) {
    oldFunc.call(this, ...args);
    if (!map.has(this.name)) {
        map.set(this.name, 0);
    }
    map.set(this.name, map.get(this.name)! - 1);
    let index = list.indexOf(this);
    if (index != -1) {
        list.slice(list.indexOf(this), 1);
    }
}
window.list = list;
