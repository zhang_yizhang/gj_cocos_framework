/** 平台对应适配
@class PlatformWidget
@author YI ZHANG
@date 2021/9/22
@desc
**/
import * as cc from "cc";
import PlatformManagerBase, {AndroidEnum, CCAndroidEnum, PlatformEnum} from "../platform/PlatformManagerBase";
import config, {Platform} from "../../../../config/config";
import {_decorator} from "cc";
const { ccclass, property,requireComponent} = _decorator;

@ccclass("PlatformWidgetData")
class PlatformWidgetData{

    @property({type : [PlatformEnum]})
    platformEnumList : Platform[] = [Platform.vivo];
    @property({type : [CCAndroidEnum]})
    androidEnumList : AndroidEnum[] = [];
    @property(cc.CCFloat)
    top : number = 0;
    @property(cc.CCFloat)
    bottom : number = 0;
    @property(cc.CCFloat)
    left : number = 0;
    @property(cc.CCFloat)
    right : number = 0;
}

@ccclass("PlatformWidget")
@requireComponent(cc.Widget)
export class  PlatformWidget extends cc.Component{
    @property([PlatformWidgetData])
    dataList : PlatformWidgetData[] = [];

    protected __preload(): void {
        for(let i = 0;i < this.dataList.length;++i){
            let data = this.dataList[i];
            let platformEnumList = data.platformEnumList;
            let androidEnumList = data.androidEnumList;
            let isChoose = platformEnumList.indexOf(config.platform) != -1 &&
                (config.platform != Platform.androids || androidEnumList.length == 0
                    || androidEnumList.indexOf(PlatformManagerBase.getAndroidChannel()) != -1);
            if(isChoose){
                let comp = this.getComponent(cc.Widget)!;
                comp.top = data.top;
                comp.bottom = data.bottom;
                comp.left = data.left;
                comp.right = data.right;
                break;
            }
        }
    }
}

