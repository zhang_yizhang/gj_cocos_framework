/** 时间相关
@class DateUtil
@author YI ZHANG
@date 2020/7/27
@desc
**/
import SingletonPatternBase from "../../mvvm/SingletonPatternBase";

export namespace Time{
    export const SECOND = 1000;
    export const MINUTE = SECOND * 60;
    export const HOUR = MINUTE * 60;
    export const DAY = HOUR * 24;
    export const WEEK = DAY * 7;
}
export class DateUtil extends SingletonPatternBase{
    private date!:Date;
    private fmt!:string;
    static now(){
        return new Date().getTime();
    }
    static format(date:Date | number, fmt:string,toCnTime ?: boolean){
        if(typeof date == "number"){
            date = new Date(date);
        }
        let instance = this.getInstance();
        instance.setDate(date);
        instance.setFmt(fmt);
        return instance.format();
    }

    /**
     * 获取今天0点时间
     * @param time
     */
    public getTodayZeroTime(time = DateUtil.now()){
        let day = 60 * 60 * 24 * 1000;
        return time - (time + Time.DAY / 3) % day;
    }

    /**
     * 是否是今天的时间
     * @param time
     */
    public isToday(time : number){
        return this.getTodayZeroTime(time) == this.getTodayZeroTime();
    }
    public getDate():Date {
        return this.date;
    }
    public setDate(date:Date):void {
        this.date = date
    }
    public getFmt():string {
        return this.fmt;
    }
    public setFmt(fmt:string):void {
        this.fmt = fmt
    }
    public format = ():string => {
        let _date = this.getDate()
        let fmt = this.getFmt();
        let time = _date.getTime();
        let o:any = {
            "d+" : Math.floor(time / Time.DAY),                    //日
            "h+" : Math.floor(time % Time.DAY / Time.HOUR),                   //小时
            "m+" : Math.floor(time % Time.HOUR / Time.MINUTE),                 //分
            "s+" : Math.floor(time % Time.MINUTE / Time.SECOND),                  //秒
            "S"  : Math.floor(time % Time.SECOND),                   //毫秒
        };

        if(/(y+)/.test(fmt)) {
            fmt=fmt.replace(RegExp.$1, (_date.getFullYear()+"").substr(4 - RegExp.$1.length));
        }
        for(let k in o) {
            if(new RegExp("("+ k +")").test(fmt)){
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
            }
        }
        return fmt;
    }
    static formatBySec(time: number, fmt:string,toCnTime ?: boolean){   
        time*=Time.SECOND;   
        let o:any = {
            "d+" : Math.floor(time / Time.DAY),                    //日
            "h+" : Math.floor(time % Time.DAY / Time.HOUR),                   //小时
            "m+" : Math.floor(time % Time.HOUR / Time.MINUTE),                 //分
            "s+" : Math.floor(time % Time.MINUTE / Time.SECOND),                  //秒
            "S"  : Math.floor(time % Time.SECOND),                   //毫秒
        };

        for(let k in o) {
            
            if(new RegExp("("+ k +")").test(fmt)){
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
            }
        }
        return fmt;
    }    
}
