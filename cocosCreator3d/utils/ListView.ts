/** ListView复用组件
@class ListView
@author YI ZHANG
@date 2020/12/2
@desc
**/
import * as cc from "cc";
import { PoolTool } from "./PoolTool";
import { DateUtil } from "./DateUtil";
import EventManager from "../../Event/EventManager";
import { CustomEngineEvent } from "./engineInit";

const { ccclass, property } = cc._decorator;
class PosInfo {
    top !: number;
    size !: number;
    offset !: number;
    col !: number;
    node?: cc.Node;
    boundingSize!: number;

}
interface ListItem extends cc.Component {
    init(data: any, index: number): any;
}
@ccclass("ListView")
export default class ListView extends cc.Component {
    dataList: any[] = [];
    @property(cc.ScrollView)
    scrollView: cc.ScrollView = null!;
    @property(cc.Node)
    contentTmp: cc.Node = null!;
    spacing!: number; // space between each item
    private content!: cc.Node;
    private pool!: PoolTool;
    private size!: cc.Size;
    private isHorizontal!: boolean;
    public itemSize!: number;
    public itemBoundingSize!: number;
    private totalNum!: number;
    private posList!: PosInfo[];
    private itemOffset!: number;
    @property(cc.Node)
    mask: cc.Node = null!;
    private topBorder!: number;
    private bottomBorder!: number;
    private comp!: (new (...args: any[]) => ListItem);
    private _isDirty!: boolean;
    private _removeList: { item: cc.Node, recoveryTime: number }[] = [];
    private _colNum!: number;
    private _colSpace!: number;
    private startColPos!: number;
    private colSize!: number;
    private startPos!: number;
    private _isSort!: boolean;
    private endSpace!: number;
    onLoad() {
        this.content = this.contentTmp || this.scrollView.content!;
        this.scrollView.content!.on(cc.Node.EventType.TRANSFORM_CHANGED, this.refreshView, this);
        this.mask.on(cc.Node.EventType.SIZE_CHANGED, this.updateMaskView, this);
        this.updateMaskView();
    }

    updateMaskView() {
        this.size = this.mask.getComponent(cc.UITransform)!.contentSize;
        if (this.isHorizontal) {
            this.topBorder = -this.size.width * this.mask.getComponent(cc.UITransform)!.anchorX;
            this.bottomBorder = this.size.width * (1 - this.mask.getComponent(cc.UITransform)!.anchorX);
        } else {
            this.topBorder = -this.size.height * this.mask.getComponent(cc.UITransform)!.anchorY;
            this.bottomBorder = this.size.height * (1 - this.mask.getComponent(cc.UITransform)!.anchorY);
        }

        this._isDirty = true;
        this._isSort = true;
    }

    protected onEnable(): void {
        this.scrollView.content!.on(cc.Node.EventType.TRANSFORM_CHANGED, this.refreshView, this);
        EventManager.on(CustomEngineEvent.EVENT_BEFORE_DRAW1, this._lateUpdate, this);
    }
    protected onDisable(): void {
        this.scrollView.content!.off(cc.Node.EventType.TRANSFORM_CHANGED, this.refreshView, this);
        EventManager.off(CustomEngineEvent.EVENT_BEFORE_DRAW1, this._lateUpdate, this);
    }

    /**
     * 初始化
     * @param dataList
     * @param pool
     * @param comp
     * @param space
     * @param extData
     */
    initialize<T extends ListItem>(dataList: any[], pool: PoolTool, comp: (new (...args: any[]) => ListItem), space: number,
        extData: {
            isNoRefresh?: boolean, col?: number, colSpace?: number,
            start?: number, endSpace?: number, content?: cc.Node
        } = {}) {
        this.clearAll();
        this.pool = pool;
        this.content = extData.content || this.scrollView.content!;
        let col = extData.col || 1;
        let colSpace = extData.colSpace || 0;
        this._colNum = col;
        this._colSpace = colSpace;
        this.startPos = extData.start || 0;
        this.isHorizontal = this.scrollView.horizontal;
        this.endSpace = extData.endSpace || 0;
        this.updateMaskView();
        let pos = this.content.getPosition();
        pos[this.isHorizontal ? "x" : "y"] = this.isHorizontal ? this.topBorder : this.bottomBorder;
        this.scrollView.content!.setPosition(pos);
        this.comp = comp;
        this.dataList = dataList || [];
        this.pool = pool;
        this.spacing = space;
        this.itemOffset = this.isHorizontal ?
            this.pool.template.data.getComponent(cc.UITransform).anchorX * this.pool.template.data.getComponent(cc.UITransform).width :
            (1 - this.pool.template.data.getComponent(cc.UITransform).anchorY) * this.pool.template.data.getComponent(cc.UITransform).height;
        this.itemSize = this.pool.template.data.getComponent(cc.UITransform)[this.isHorizontal ? "width" : "height"];
        this.itemBoundingSize = this.pool.template.data.getComponent(cc.UITransform).getBoundingBoxTo(new cc.math.Mat4())[this.isHorizontal ? "width" : "height"];
        if (this._colNum > 1) {
            if (this.isHorizontal) {
                this.colSize = this.pool.template.data.height;
                this.startColPos = this.mask.getComponent(cc.UITransform)!.height * (1 - this.mask.getComponent(cc.UITransform)!.anchorY)
                    - this.colSize * (1 - this.pool.template.data.getComponent(cc.UITransform).anchorY);
            } else {
                this.colSize = this.pool.template.data.getComponent(cc.UITransform).width;
                this.startColPos = -this.mask.getComponent(cc.UITransform)!.width * this.mask.getComponent(cc.UITransform)!.anchorX + this.colSize * this.pool.template.data.getComponent(cc.UITransform).anchorX;
            }
            this.colSize += this._colSpace
        }
        for (let i = 0; i < dataList.length; ++i) {
            this.addItem(i, true);
        }
        this.updateContentSize();
        if (!extData.isNoRefresh) {
            this.refreshView();
        }
    }

    /**
     * 添加一个item
     * @param index
     * @param isInit
     * @param data
     */
    addItem(index: number, isInit?: boolean, data?: any) {
        if (data !== undefined) {
            this.dataList.splice(index, 0, data);
        }
        this.totalNum++;
        let prePos = this.posList[index - this._colNum];
        let dir = this.isHorizontal ? 1 : -1;
        let top = prePos ? prePos.top + prePos.size * dir : this.startPos * dir;
        let cellInfo = this.getItemCellInfo(index);
        let space = prePos ? this.spacing : 0;
        top += space * dir;
        let posInfo: PosInfo = {
            top: top,
            size: cellInfo.size,
            offset: cellInfo.offset * dir,
            boundingSize: cellInfo.boundingSize,
            col: index % this._colNum
        };
        let offset = cellInfo.size + space;
        this.posList.splice(index, 0, posInfo);
        for (let i = index + 1; i < this.totalNum; ++i) {
            let posInfo = this.posList[i];
            let col = posInfo.col + 1;
            let isAdd = col >= this._colNum;
            posInfo.col = col % this._colNum;
            if (isAdd) {
                posInfo.top += offset * dir;
            }
            this.updateItemPos(i);
        }
        if (!isInit) {
            this.refresh();
        }
    }

    pushItem(data: any, isInit?: boolean,) {
        this.addItem(this.dataList.length, isInit, data);
    }

    /**
     * 移除一个item
     * @param index
     * @param isInit
     */
    removeItem(index: number, isInit?: boolean) {
        this.totalNum--;
        let dir = this.isHorizontal ? 1 : -1;
        this._recoveryItem(this.posList[index]);
        let posInfo = this.posList[index];
        let offset = posInfo.size + this.spacing;
        let data = this.dataList.splice(index, 1)[0];
        this.posList.splice(index, 1);
        for (let i = index; i < this.totalNum; ++i) {
            let posInfo = this.posList[i];
            let col = posInfo.col - 1;
            let isAdd = col < 0;
            posInfo.col = (col + this._colNum) % this._colNum;
            if (isAdd) {
                posInfo.top -= offset * dir;
            }
            this.updateItemPos(i);
        }
        if (!isInit) {
            this.refresh();
        }
        return data;
    }

    clearAll() {
        this.scrollView.stopAutoScroll();
        this.totalNum = 0;
        this.posList = [];
        this._isDirty = false;
        this._removeList = [];
        this.pool && this.pool.putAllChildren(this.content);
    }

    /**
     * 更新content大小
     */
    updateContentSize() {
        let posInfo = this.posList[this.posList.length - 1];
        if (this.isHorizontal) {
            this.content.getComponent(cc.UITransform)!.width = (posInfo ? posInfo.top + posInfo.size : 0) + this.endSpace + this.startPos;
        } else {
            this.content.getComponent(cc.UITransform)!.height = (-(posInfo ? posInfo.top - posInfo.size : 0)) + this.endSpace + this.startPos;
        }
    }

    /**
     * 根据索引获取item
     * @param index
     */
    getItemByIndex(index: number) {
        return this.posList[index];
    }

    /**
     * 更新item位置
     * @param index
     */
    updateItemPos(index: number) {
        let posInfo = this.posList[index];
        if (cc.isValid(posInfo.node)) {
            let pos = posInfo.node!.getPosition();
            pos[this.isHorizontal ? "x" : "y"] = posInfo.top + posInfo.offset;
            if (this._colNum > 1) {
                let dir = this.isHorizontal ? -1 : 1;
                pos[this.isHorizontal ? "y" : "x"] = this.startColPos + dir * posInfo.col * this.colSize;
            }
            posInfo.node!.setPosition(pos);
            return true;
        }
        return false;
    }



    refresh() {
        this.updateContentSize();
        this.refreshView();
    }

    sortList() {
        this._isDirty = true;
        this._isSort = true;
        this._lateUpdate(0.1);
    }

    /**
     * 刷新视图  todo yizhang 待优化 不需要全遍历 可以记录上一次刷新边界
     */
    refreshView() {
        this._isDirty = true;
    }

    _showItem(index: number) {
        let posInfo = this.posList[index];
        let item = this._getPoolItem();
        item.getComponent(this.comp)!.init(this.dataList[index], index);
        posInfo.node = item;
        this.updateItemPos(index);
        // console.log("showItem");
    }

    _refreshItemData(index: number) {
        let posInfo = this.posList[index];
        let item = posInfo.node!;
        if (cc.isValid(item)) {
            item.getComponent(this.comp)!.init(this.dataList[index], index);
        }
    }

    _getPoolItem() {
        // console.log("getPoolItem");
        let item = this._removeList.length && this._removeList.shift()!.item;
        if (item && cc.isValid(item)) {
            this.pool.reuse(item);
            return item;
        }
        return this.pool.get(this.content);
    }

    _recoveryItem(posInfo: PosInfo) {
        if (cc.isValid(posInfo.node)) {
            this._delayRecoveryItem(posInfo.node!);
            posInfo.node = undefined;
        }
    }

    _delayRecoveryItem(node: cc.Node) {
        let pos = node.getPosition();
        pos[this.isHorizontal ? "x" : "y"] = 40000;
        node.setPosition(pos);
        this.pool.recovery(node);
        this._removeList.push({ item: node, recoveryTime: DateUtil.now() + 10000 });
    }


    /**
     * 获取item大小信息 可以自定义大小
     * @param index
     */
    getItemCellInfo(index: number) {
        let transform = this.pool.template.data.getComponent(cc.UITransform) as cc.UITransform
        return {
            size: transform[this.isHorizontal ? "width" : "height"],
            boundingSize: transform.getBoundingBoxTo(new cc.math.Mat4())[this.isHorizontal ? "width" : "height"],
            offset: this.isHorizontal ?
                transform.anchorX * transform.width :
                (1 - transform.anchorY) * transform.height
        };
    }


    /**
     * 获取位置是否在视图里
     * @param top
     * @param size
     */
    getPositionInView(top: number, size: number) { // get item position in scrollview's node space
        let worldPos = this.content.getComponent(cc.UITransform)!.convertToWorldSpaceAR(cc.v3(top, top, 0));
        let viewPos = this.mask.getComponent(cc.UITransform)!.convertToNodeSpaceAR(worldPos);
        let topPos = this.isHorizontal ? viewPos.x : viewPos.y;
        if (this.topBorder <= topPos && topPos <= this.bottomBorder) {
            return true;
        }
        let bottomPos = topPos + (this.isHorizontal ? size : -size);
        return this.topBorder <= bottomPos && bottomPos <= this.bottomBorder;
    }

    protected _lateUpdate(dt: number): void {
        if (!this.posList) {
            return;
        }
        if (this._isDirty) {
            let showList = [];
            for (let i = 0; i < this.posList.length; ++i) {
                let posInfo = this.posList[i];
                let isIn = this.getPositionInView(posInfo.top, posInfo.boundingSize);
                if (!isIn) {
                    this._recoveryItem(posInfo);
                } else if (!posInfo.node) {
                    showList.push(i);
                } else if (this._isSort) {
                    this._refreshItemData(i);
                }
            }
            for (let i = 0; i < showList.length; ++i) {
                this._showItem(showList[i]);
            }
            this._isDirty = false;
            this._isSort = false;
        }
        if (this._removeList.length) {
            let now = DateUtil.now();
            for (let i = this._removeList.length - 1; i >= 0; --i) {
                if (now > this._removeList[i].recoveryTime) {
                    this.pool.put(this._removeList[i].item);
                    this._removeList.splice(i, 1);
                }
            }
        }
    }


}
