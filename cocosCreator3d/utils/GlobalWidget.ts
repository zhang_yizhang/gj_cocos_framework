
import * as cc from "cc";
let requireComponent = cc._decorator.requireComponent;

const {ccclass, property} = cc._decorator;

@ccclass("GlobalWidget")
@requireComponent(cc.Widget)
export default class GlobalWidget extends cc.Component {
    protected __preload() {
        let node = this.getCanvas(this.node);
        if(node){
            this.getComponent(cc.Widget)!.target = node;
        }

    }

    getCanvas(node : cc.Node) : cc.Node | undefined{
        if(node.getComponent(cc.Canvas)){
            return node;
        }
        if(node.parent){
            return  this.getCanvas(node.parent);
        }
    }
}
