/** 数字格式化工具
 @class NumFormat
 @author YI ZHANG
 @date 2020/7/20
 @desc
 **/
import * as cc from "cc";
import {_decorator, CCFloat, CCString} from "cc";
const {ccclass, property,requireComponent} = cc._decorator;
let list = [1,10,100,1000];
// @ts-ignore
list[-1] = [0.1];
// @ts-ignore
list[-2] = [0.01]
@ccclass
@requireComponent(cc.Label)
export class NumFormat extends cc.Component{
    @property(CCString)
    formatString : string = "$num$";
    private label!: cc.Label;
    @property(CCFloat)
    _num : number = 0;
    @property(CCFloat)
    set string(value : number){
        this._num = value;
        this.label.string = this.formatString.replace(/\$num\$/g,NumFormat.getFormat(value,this.isFixed));
    }
    get string(){
        return this._num;
    }
    @property(cc.CCBoolean)
    isFixed : boolean = false;


    protected __preload() {
        let label = this.getComponent(cc.Label)!;
        this.label = label;
    }

    static getFormat(num : number,isFixed : boolean){
        if(num > 100000000){
            return (num/100000000).toFixed(2) + "亿";
        }
        if(num > 10000){
            return (num/10000).toFixed(2) + "万";
        }
        if(isFixed){
            return num.toFixed(2);
        }
        return num.toString();
    }
}