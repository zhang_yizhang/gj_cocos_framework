/**
 * 使用渲染纹理操作图片
 */
import { _decorator, Component, Node, UITransform, RenderTexture, Sprite, SpriteFrame, Texture2D, game, v3, Camera, ImageAsset, Layers, Color, color, v2, math, isValid } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('RenderUtil')
export class RenderUtil extends Component {
    static cameraNode: Node
    static renderTexture: RenderTexture
    /**
     * 读取目标节点像素数据
     * @param node 目标节点
     * @param justNode 是否只读取节点
     * @param flipY 是否延 Y 轴方向, 翻转 UV
     * @returns 像素数据
     */
    public static async getPixelsData(node: Node, layers: number, clearColor: Color, flipY: boolean = true) {
        return await new Promise((resolve: (pixelsData: Uint8Array) => void, reject) => {
            const uiTransform = node.getComponent(UITransform)!;
            //节点宽高
            const width = uiTransform.width,
                height = uiTransform.height;
            //只读取节点，从原始层级分离
            const initLayer = node.layer;
            node.layer = layers;
            //创建临时相机用于渲染目标节点
            if(!isValid(this.cameraNode)){
                this.cameraNode= new Node('RenderUtil-camera');
            }else{

            }
            this.cameraNode.parent = node;
            this.cameraNode.position = v3(0, 0, 1000);
            this.cameraNode.active=true
            const camera = this.cameraNode.getComponent(Camera)||this.cameraNode.addComponent(Camera);
            camera.visibility = layers;
            camera.clearColor = clearColor;
            camera.projection = Camera.ProjectionType.ORTHO;
            camera.orthoHeight = height / 2;
            //将节点渲染到 RenderTexture 中
            let renderTexture = new RenderTexture();
            renderTexture.reset({
                width: width,
                height: height,
            });
            camera.targetTexture = renderTexture;
            this.renderTexture = renderTexture
            setTimeout(() => {
                //获取像素数据
                let pixelsData = renderTexture.readPixels()!;
                // 销毁临时对象并返回数据
                camera.targetTexture=null
                renderTexture.destroy();                
                this.cameraNode.active=false
                camera.visibility=Layers.Enum.NONE
                // cameraNode.getComponent(Camera)?.destroy()
                // cameraNode.destroy();
                console.log('renderUtil-setTimeout-destroy')
                node.layer = initLayer;
                //垂直翻转数据
                if (flipY && pixelsData) {
                    pixelsData = this.flipY(pixelsData, width * 4);
                }
                resolve(pixelsData);
            }, game.frameTime)
        }) 
    }

    static destroyData() {
        console.log('renderUtil-destroyData',this.cameraNode)
        if (isValid(this.renderTexture)) {
            console.log('renderUtil-destroyData-renderTexture')
            this.renderTexture.destroy()
            this.renderTexture = null!
        }
        if (isValid(this.cameraNode)) {
            console.log('renderUtil-destroyData-cameraNode')
            this.cameraNode.active=false
            // this.cameraNode.getComponent(Camera)?.destroy()
            // this.cameraNode.destroy()
            // this.cameraNode = null!
        }
        
    }

    onDisable() {
        RenderUtil.destroyData()
    }

    /**
     * 垂直翻转图像数据
     * @param array 数据
     * @param width 行宽
     */
    public static flipY(array: Uint8Array, width: number) {
        const length = array.length,
            flipped = new Uint8Array(length);
        for (let i = 0, j = length - width; i < length; i += width, j -= width) {
            for (let k = 0; k < width; k++) {
                flipped[i + k] = array[j + k];
            }
        }
        return flipped;
    }

    /**
     * 输出新图片(截图或染色)
     * @param buffer 像素数据
     * @param width 
     * @param height 
     * @param outputSprite 输出目标
     * @param flipY 是否延 Y 轴方向, 翻转 UV
     */
    public static printScreen(buffer: ArrayBufferView, width: number, height: number, outputSprite: Sprite, flipY: boolean = true) {
        let img = new ImageAsset();
        img.reset({
            _data: buffer,
            width: width,
            height: height,
            format: Texture2D.PixelFormat.RGBA8888,
            _compressed: false
        });
        let texture = new Texture2D();
        texture.image = img;
        const sp = new SpriteFrame();
        sp.texture = texture;
        sp.packable = false;
        outputSprite.spriteFrame = sp;
        outputSprite.spriteFrame.flipUVY = flipY;
    }

    /**
     * 广度搜索替换相同像素
     * @param buffer 
     * @param width 
     * @param x 
     * @param y 
     * @param targetColor 
     * @param excludeColor 
     * @returns 
     */
    public static replaceBufferBySamePixel(buffer: Uint8Array, width: number, x: number, y: number, targetColor: Color, excludeColor?: Color) {
        const index = this.getIndex(width, x, y),
            initColor = this.parseColorByUint8Array(buffer, index),
            targetBuffer = [targetColor.r, targetColor.g, targetColor.b, targetColor.a],
            height = buffer.length / width / 4;
        if (excludeColor && initColor.equals(excludeColor)) {
            return;
        }
        let findList = [v2(x, y)];
        let replaceList: math.Vec2[] = [];
        const findRoundPixel = () => {
            if (findList.length === 0) {
                return;
            }
            const newFindList = [];
            for (let pos of findList) {
                if ((pos.x >= width || pos.x < 0) || (pos.y >= height || pos.y < 0)) {
                    continue;
                }
                const curIndex = this.getIndex(width, pos.x, pos.y),
                    curColor = this.parseColorByUint8Array(buffer, curIndex);
                if (excludeColor && curColor.equals(excludeColor) || curColor.equals(targetColor)) {
                    continue;
                }
                //替换像素值
                for (let i = curIndex; i < curIndex + 4; i++) {
                    buffer[i] = targetBuffer[i - curIndex];
                }
                newFindList.push(v2(pos.x + 1, pos.y), v2(pos.x, pos.y + 1), v2(pos.x - 1, pos.y), v2(pos.x - 1, pos.y - 1));
                replaceList.push(v2(Math.round(pos.x), Math.round(pos.y)));
            }
            findList = newFindList;
            findRoundPixel();
        }
        findRoundPixel();
        return { buffer: buffer, replaceList: replaceList };
    }

    /**
     * 获取像素下标
     * @param width 
     * @param x 
     * @param y 
     * @returns 
     */
    public static getIndex(width: number, x: number, y: number) {
        return Math.floor(width) * 4 * Math.floor(y) + (4 * Math.floor(x));
    }

    /**
     * 解析成color
     * @param buffer 
     * @param index 
     * @returns 
     */
    public static parseColorByUint8Array(buffer: Uint8Array, index: number) {
        const colors = buffer.slice(index, index + 4);
        return color(colors[0], colors[1], colors[2], colors[3]);
    }

    /**
     * 输出一个Color类型的RGBA值
     * @param color 
     */
    public static printRGBA(color: Color) {
        console.log(color.r, color.g, color.b, color.a);
    }
}
