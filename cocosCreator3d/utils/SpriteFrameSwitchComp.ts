/** 精灵图切换
 @class SpriteFrameSwitchComp
 @author YI ZHANG
 @date 2020/8/18
 @desc
 **/
import * as cc from "cc";

const {ccclass, property,requireComponent,executeInEditMode} = cc._decorator;

@ccclass
@requireComponent(cc.Sprite)
@executeInEditMode(true)
export default class SpriteFrameSwitchComp extends cc.Component {
    @property(cc.CCBoolean)
    isRandom: boolean = false;
    @property([cc.SpriteFrame])
    spriteFrameList: cc.SpriteFrame[] = [];
    @property(cc.CCInteger)
    _startIndex : number = 0;
    @property(cc.CCInteger)
    set startIndex(index : number){
        this._startIndex = index;
        this.getComponent(cc.Sprite)!.spriteFrame = this.spriteFrameList[index];
    }
    get startIndex(){
        return this._startIndex;
    }
    curIndex: number = -1;

    setIndex(index: number) {
        if(index == -1){
            return;
        }
        this.curIndex = index;
        let comp = this.getComponent(cc.Sprite);
        comp && (comp.spriteFrame = this.spriteFrameList[index % this.spriteFrameList.length]);
    }

    protected onEnable(): void {
        if(this.isRandom){
            this.setIndex(Math.floor(Math.random() * this.spriteFrameList.length));
        }
    }
}
