/** icon适配
@class IconWidgetComp
@author YI ZHANG
@date 2021/5/17
@desc
**/
import { _decorator, Component, Node } from 'cc';
import * as cc from "cc";
import {EDITOR} from "cc/env";
const { ccclass, property,executeInEditMode} = _decorator;

@ccclass('IconWidgetComp')
@executeInEditMode
export class IconWidgetComp extends Component {
    @property(cc.CCInteger)
    size : number = 100;
    @property(cc.CCInteger)
    isMax : boolean = false;
    private blockUpdateSize: boolean = false;


    onLoad () {
        this.node.on(cc.Node.EventType.SIZE_CHANGED,()=>{
            if(this.blockUpdateSize){
                return;
            }
            this.blockUpdateSize = true;
            this.updateSize();
            this.blockUpdateSize = false;
        },this);
        this.updateSize();
    }

    updateSize(){
        let comp = this.node.getComponent(cc.UITransform);
        if(!comp){
            return;
        }
        let size = comp.contentSize;
        let viewSize = cc.size(this.size,this.size);
        if(viewSize.height / viewSize.width  > size.height / size.width){
            let scale = this.isMax ? viewSize.height / size.height : viewSize.width / size.width;
            this.node.scale = cc.v3(scale,scale,scale)
        }else {
            let scale = this.isMax ? viewSize.width / size.width : viewSize.height / size.height;
            this.node.scale = cc.v3(scale,scale,scale)
        }
    }

    protected update(dt: number): void {
        if(EDITOR){
            this.updateSize();
        }
    }
}
