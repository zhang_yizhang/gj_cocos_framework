/**
@class QQAudio
@author YI ZHANG
@date 2021/9/26
@desc
**/
import * as cc from 'cc';
import {clamp} from "cc";
const { ccclass, property } = cc._decorator;
@ccclass('QQAudio')
export class QQAuido extends cc.Component {
    static get maxAudioChannel () {
        return 2;
    }
    public  audioState = cc.AudioSource.AudioState.INIT;

    protected _clip: cc.AudioClip | null = null;
    protected _player: any | null = null;
    protected _loop = false;
    protected _playOnAwake = true;
    protected _volume = 1;

    private _cachedCurrentTime = 0;

    // An operation queue to store the operations before loading the AudioPlayer.
    private _operationsBeforeLoading: string[] = [];
    private _isLoaded = false;

    private _lastSetClip?: cc.AudioClip;


    set clip (val) {
        if (val === this._clip) {
            return;
        }
        this._clip = val;
        this._syncPlayer();
    }

    get clip () {
        return this._clip;
    }
    private _syncPlayer () {
        if(this._player){
            this._player.destroy();
            this._player = null;
        }
        if(!this._clip){
            return;
        }
        this._isLoaded = false;
        // @ts-ignore
        const innerAudioContext = qq.createInnerAudioContext();
        innerAudioContext.autoplay = false;
        innerAudioContext.src = this._clip.nativeUrl;
        innerAudioContext.volume = this.volume;
        innerAudioContext.loop = this.loop;
        innerAudioContext.onCanplay(()=>{
            this._isLoaded = true;
            if(this.audioState == cc.AudioSource.AudioState.PLAYING){
                innerAudioContext.play();
            }
        });
        innerAudioContext.onPlay(() => {
        });
        innerAudioContext.onError((res : any) => {
            this.audioState = cc.AudioSource.AudioState.INTERRUPTED;
        });
        this._player = innerAudioContext;
    }

    set loop (val) {
        this._loop = val;
        this._player && (this._player.loop = val);
    }
    get loop () {
        return this._loop;
    }

    set playOnAwake (val) {
        this._playOnAwake = val;
    }
    get playOnAwake () {
        return this._playOnAwake;
    }

    set volume (val) {
        if (Number.isNaN(val)) { console.warn('illegal audio volume!'); return; }
        val = cc.clamp(val, 0, 1);
        if (this._player) {
            this._player.volume = val;
            this._volume = this._player.volume;
        } else {
            this._volume = val;
        }
    }
    get volume () {
        return this._volume;
    }

    public onLoad () {
        this._syncPlayer();
    }

    public onEnable () {
        // audio source component may be played before
        if (this._playOnAwake && !this.playing) {
            this.play();
        }
    }

    public onDisable () {
        this.pause();
    }

    public onDestroy () {
        this.stop();
        if(this._player){
            this._player.destroy();
        }
    }

    /**
     * @en
     * Play the clip.<br>
     * Restart if already playing.<br>
     * Resume if paused.
     * @zh
     * 开始播放。<br>
     * 如果音频处于正在播放状态，将会重新开始播放音频。<br>
     * 如果音频处于暂停状态，则会继续播放音频。
     */
    public play () {
        this.audioState = cc.AudioSource.AudioState.PLAYING;
        if (!this._isLoaded) {
            return;
        }
        if(this._player){
            if (this.state ===  cc.AudioSource.AudioState.PLAYING) {
                this._player.stop();
            }
            this._player.play();
        }
    }

    /**
     * @en
     * Pause the clip.
     * @zh
     * 暂停播放。
     */
    public pause () {
        this.audioState = cc.AudioSource.AudioState.PAUSED;
        if(this._player){
            this._player.pause();
        }
    }

    /**
     * @en
     * Stop the clip.
     * @zh
     * 停止播放。
     */
    public stop () {
        this.audioState = cc.AudioSource.AudioState.STOPPED;
        if(this._player){
            this._player.stop();
        }
    }

    /**
     * @en
     * Plays an AudioClip, and scales volume by volumeScale. The result volume is `audioSource.volume * volumeScale`. <br>
     * @zh
     * 以指定音量倍数播放一个音频一次。最终播放的音量为 `audioSource.volume * volumeScale`。 <br>
     * @param clip The audio clip to be played.
     * @param volumeScale volume scaling factor wrt. current value.
     */
    public playOneShot (clip: cc.AudioClip, volumeScale = 1) {
        if (!clip._nativeAsset) {
            console.error('Invalid audio clip');
            return;
        }
        // @ts-ignore
        const innerAudioContext = qq.createInnerAudioContext();
        innerAudioContext.volume = this.volume;
        innerAudioContext.autoplay = true;
        innerAudioContext.src = clip.nativeUrl;
        innerAudioContext.onCanplay(()=>{
            innerAudioContext.play();
        });
    }

    /**
     * @en
     * Set current playback time, in seconds.
     * @zh
     * 以秒为单位设置当前播放时间。
     * @param num playback time to jump to.
     */
    set currentTime (num: number) {
        if (Number.isNaN(num)) { console.warn('illegal audio time!'); return; }
        num = clamp(num, 0, this.duration);
        this._cachedCurrentTime = num;
        if(this._player){
            this._player.seek(this._cachedCurrentTime);
        }
    }

    /**
     * @en
     * Get the current playback time, in seconds.
     * @zh
     * 以秒为单位获取当前播放时间。
     */
    get currentTime () {
        return this._player ? this._player.currentTime : this._cachedCurrentTime;
    }

    /**
     * @en
     * Get the audio duration, in seconds.
     * @zh
     * 获取以秒为单位的音频总时长。
     */
    get duration () {
        if(this._clip){
            return this._clip.getDuration();
        }
        return this._player ? this._player.currentTime : 0;
    }


    /**
     * @en
     * Get current audio state.
     * @zh
     * 获取当前音频状态。
     */
    // @ts-ignore
    get state (): cc.AudioSource.AudioState {
        return this.audioState;
    }

    /**
     * @en
     * Is the audio currently playing?
     * @zh
     * 当前音频是否正在播放？
     */
    get playing () {
        return this.state === cc.AudioSource.AudioState.PLAYING;
    }
}


