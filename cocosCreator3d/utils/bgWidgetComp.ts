/** 背景适配
@class bgWidgetComp
@author YI ZHANG
@date 2021/5/17
@desc
**/
import { _decorator, Component, Node } from 'cc';
import * as cc from "cc";
const { ccclass, property } = _decorator;

@ccclass('BgWidgetComp')
export class BgWidgetComp extends Component {
    /* class member could be defined like this */
    // dummy = '';

    /* use `property` decorator if your want the member to be serializable */
    // @property
    // serializableDummy = 0;

    onLoad () {
        let comp = this.node.getComponent(cc.UITransform);
        if(!comp){
            return;
        }
        let size = comp.contentSize;
        let viewSize = cc.view.getVisibleSize();
        if(viewSize.height / viewSize.width  > size.height / size.width){
            let scale = viewSize.height / size.height;
            this.node.scale = cc.v3(scale,scale,scale)
        }else {
            let scale = viewSize.width / size.width;
            this.node.scale = cc.v3(scale,scale,scale)
        }
    }

    // update (deltaTime: number) {
    //     // Your update function goes here.
    // }
}
