/** 事件处理 不建议视图层使用，用于模型层信息交互，避免信息强耦合
@class EventManager
@author YI ZHANG
@date 2020/7/21
@desc
**/
import * as cc from "cc";

interface IEventData {
    func:Function;
    target:any;
    isCCClass : boolean;
}
interface IEvent{
    [eventName:string]:IEventData[];
}

export default class EventManager {
    public static handle:IEvent = {};

    public static on(eventName:string,cb:Function,target?:Object){
        if(!this.handle[eventName]){
            this.handle[eventName] = [];
        }
        const data:IEventData = {func:cb,target,isCCClass : ((target instanceof cc.Component) || (target instanceof cc.Node))};
        let eventList = this.handle[eventName]
        for(let i = 0;i < eventList.length;++i){
            if(eventList[i].func == cb && eventList[i].target == target){
                return;
            }
        }
        this.handle[eventName].push(data);
    }

    public static off(eventName:string,cb:Function,target?:any){
        const list = this.handle[eventName];
        if(!list || list.length<=0){
            return;
        }

        for (let i = 0; i < list.length; i++) {
            const event = list[i];
            if(event.func === cb && (!target || target === event.target)){
                list.splice(i,1);
                break;
            }            
        }
    }

    public static offByTarget(target ?: any){
        for(let key in this.handle){
            let list : IEventData[] = this.handle[key];
            for(let i = 0;i < list.length;++i){
                if(list[i].target == target){
                    list.splice(i--,1)
                }
            }
        }
    }

    public static emit(eventName:string,...args:any){
        if(!this.handle[eventName])return;
        let list = new Array(...this.handle[eventName]);
        if(!list || list.length<=0){
            return;
        }
        for (let i = 0; i < list.length; i++) {
            const event = list[i];
            if(event.isCCClass && !cc.isValid(event.target)){
                this.offByTarget(event.target);
                continue;
            }
            event.func.apply(event.target,args);         
        }
    }
  
}
